require("./../config")
const { fetchUrl, fetchBuffer } = require("../lib/Function")
const { formPerkenalkanDiri, catatanGIF } = require("../lib/template")
const { getGuildID, cariTraveler, gifDBEngine } = require("../lib/gif")
const fs = require("fs")

module.exports = async (paimon, groupUpdate) => {
    try {
        let groupID = groupUpdate.id
        let action = groupUpdate.action
        let partisipan = groupUpdate.participants[0]
        let metadata = await paimon.groupMetadata(groupID)
        let gif_guilds = global.gif_guilds
        let isGIFGuild = gif_guilds.includes(groupID)

        if(!isGIFGuild) return; // untuk sekarang hanya meyambut calon/mantan member GIF

        let ppimg = await paimon.profilePictureUrl(`${partisipan.split('@')[0]}@c.us`, 'image')
        let imgChars = await fetchBuffer(ppimg)

        if(action=="remove") {
            console.log("Terdapat member GIF yang keluar group")
            let caption = `Sayonara @${partisipan.split('@')[0]}, paimon MISS YOU :")`
            let resPesan = {
                image: imgChars,
                caption: `${caption}`
            }
            return await paimon.sendMessage(m.from, resPesan, {})
        } else
        if(action=="add") {
            console.log("Terdapat seseorang yang masuk ke GIF")
            let teks_welcome = `Halo @${partisipan.split('@')[0]}, saya Paimon\n`
            teks_welcome += `Selamat datang di *${metadata.subject}*\n`
            // Cek ke database GIF
            let guildID = await getGuildID(metadata.subject)
            let cari = await cariTraveler(partisipan)
            if(cari==null || !cari || cari.length<1) {
                console.log("Terdapat calon member GIF baru")
                teks_welcome += formPerkenalkanDiri()
                teks_welcome += `───────────────\n`
                teks_welcome += catatanGIF()
                // Tambah ke database GIF
                try {
                    let ponsel = partisipan.split('@')[0]
                    let nomorHP = "'+" + ponsel.slice(0, 2) + " " + ponsel.slice(2, 5) + "-" + ponsel.slice(5, 9) + "-" + ponsel.slice(9)
                    let namaUser = "Traveler Baru"
                    let data_baru = {
                        "data": [{
                            "Nama": namaUser,
                            "Nomor Hp": nomorHP,
                            "Status": "Blm. Registrasi",
                            "Signature": "-",
                            "Teguran": "0",
                            "WhatsApp_ID": partisipan
                        }]
                    }
                    let put_data = await gifDBEngine("post", guildID.name, false, data_baru)
                    console.log(put_data)
                } catch (e) {
                    console.log(e)
                }
                // End Tambah ke database GIF
            }
            // End Cek ke database GIF

            let resWelcome = {
                image: imgChars,
                caption: `${teks_welcome}`,
                mentions: [partisipan],
            }
            return await paimon.sendMessage(m.from, resWelcome, {})
        }
    } catch (e) {
        console.log(e)
    }
}