const {
    getFireData, 
    getAllFireData, 
    getLocalFireData, 
    getFireDataRealtime,
    updateFireData,
    getRealtimeDB
} = require("./../controller/firebase/fireController")

const {
	realtimePushNotifHandler
} = require("./database/realtime_notif")

const {
    realtimeUpdateProfile
} = require("./database/profile_config")


exports.getListenerRealtimeDB = async(paimon) => {
	console.log(`Melakukan sinkronisasi realtime ke db...`);
    const masterDB = await getRealtimeDB()
    const observer = await masterDB.onSnapshot(async(docSnapshot) => {
        console.log(`Realtime database handling...`);
        try {
        	const documents = await fetchDB(docSnapshot)
        	const handling = documentHandling(paimon, documents)
        	return handling
        } catch(e) {
        	console.log(e)
        }
    }, err => {
        console.log(`Encountered error: ${err}`);
    });
    return observer
}

const documentHandling = async(paimon, fireDatabase) => {
	global.gif_guilds = await getLocalFireData(fireDatabase, 'gif_guilds')
    global.registered_groups = await getLocalFireData(fireDatabase, 'registered_groups')
    global.badwords = await getLocalFireData(fireDatabase, 'badwords')
    global._pelanggar = await getLocalFireData(fireDatabase, 'pinalty')
    global.db_limits = await getLocalFireData(fireDatabase, 'limits')
    global._party_helper = await getLocalFireData(fireDatabase, 'party_helper')
    global.users_banned = await getLocalFireData(fireDatabase, 'users_banned')
    global.db = await getLocalFireData(fireDatabase, 'db_custom')
    global.afk_list = await getLocalFireData(fireDatabase, 'afk_list')
    global.contacts = await getLocalFireData(fireDatabase, 'realtime_contact')
    // global.paimon_live_chat = await getLocalFireData(fireDatabase, 'paimon_live_chat')
    // Profile Configs
    const profile_config = await getLocalFireData(fireDatabase, 'profile_config')
    const handlingProfile = await realtimeUpdateProfile(paimon, profile_config)
    // Notif Push
    const list_notif = await getLocalFireData(fireDatabase, 'realtime_notif')
    const pushNotif = await realtimePushNotifHandler(paimon, list_notif)
}

const fetchDB = async(docSnapshot) => {
	let data = []
    if (docSnapshot.docs.length > 0) {
        for (const item of docSnapshot.docs) {
            let id = item.id
            let dt = item.data()
            data.push({
                id: id,
                data: dt.data
            })
        }
    }
    return data
}