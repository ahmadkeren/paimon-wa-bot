const {
    updateFireData,
} = require("./../../controller/firebase/fireController")

const { fetchBuffer, htmlToWaText } = require("./../../lib/Function")

//  === Sistem realtime notifikasi - pusher dari web atau API ===

const collection_name = "realtime_notif"

exports.realtimePushNotifHandler = async(paimon, dataMaster) => {
    // Filter data
    let dataFilter = filterUnsendedMessage(dataMaster)
    
    if(dataFilter.length == 0) return;

    // Kirim pesan method
    for(let dt of dataFilter) {
        // Templating message
        let text = ``
        text += `[ *${dt.title}* ]\n`
        text += `${htmlToWaText(dt.message)}`
        let resPesan = {}
        let thumbIMG = null;

        // Thumb filter
        let haveThumb = false
        if(dt.thumb!=null && dt.thumb!="") {
            try {
                thumbIMG = await fetchBuffer(dt.thumb, {}, "")
                haveThumb = true
            } catch(e) {
                console.log(e)
            }
        }

        // Kirim pesan
        for(let target of dt.target) {
            try {
                console.log(`Mengirim pesan "${dt.title}" ke nomor ${target}`)

                // Smart filter
                let text_filter = text.split("%nama").join("%name")
                if(text_filter.toLowerCase().indexOf("%name%") !== -1) {
                    text_filter = text_filter.split("%name%").join(await getContactName(target))
                }

                // Inisialisasi
                if(haveThumb) {
                    resPesan = {
                        image: thumbIMG,
                        caption: `${text_filter}`
                    }
                } else {
                    resPesan = {
                        text: text_filter
                    }
                }
                resPesan.footer = dt.footer

                // Kirim pesan
                await paimon.sendMessage(target, resPesan, {})
            } catch(e) {
                try {
                    await paimon.sendMessage(target, {text: text}, {})
                } catch(e) {
                    console.log(`Pesan gagal dikirim ke ${target} karena ${e}`)
                }
            }
        }
    }

    // Toggle status
    let dataStatus = changeMessageStatus(dataMaster)
    let updateStatus = updateFireData(collection_name, dataStatus)
}

const filterUnsendedMessage = function(dataMaster) {
    let dataFilter = []
    for(let dt of dataMaster) {
        if(dt.isSended) continue
        dataFilter.push(dt)
    }
    return dataFilter
}

const changeMessageStatus = function(dataMaster) {
    let dataFilter = []
    for(let dt of dataMaster) {
        if(dt.isSended)
            dataFilter.push(dt)
        else {
            let tmp = dt
            tmp.isSended = true
            dataFilter.push(tmp)
        }
    }
    return dataFilter
}

const getContactName = async (target) => { // Tambahkan kontak ke db
    for(let contact of global.contacts) {
        if(contact.id == target) {
            if(
                contact.custom_name != null &&
                contact.custom_name != ""
            )
                return contact.custom_name
            if(
                contact.name != null &&
                contact.name != ""
            )
                return contact.name
            return contact.id.split("@s.whatsapp.net").join("")
        }
    }
}