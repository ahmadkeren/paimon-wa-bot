const {
    updateFireData,
} = require("./../../controller/firebase/fireController")

const { fetchBuffer } = require("./../../lib/Function")

//  === Sistem realtime notifikasi - pusher dari web atau API ===

const collection_name = "profile_config"

exports.realtimeUpdateProfile = async(paimon, dataMaster) => {
    // Filter data
    let dataFilter = filterUnAppliedConfig(dataMaster)
    if(dataFilter.length == 0) return;
    // Apply config
    for(let config of dataFilter) {
        switch(config.config_name) {
            case "profile_name":
                try {
                    console.log("Mencoba mengganti nama profile...")
                    $update = paimon.updateProfileName(config.value)
                } catch(e) {
                    console.log(e)
                    console.log("Gagal mengganti nama profile...")
                }
                break
            case "profile_picture":
                try {
                    console.log("Mencoba mengganti foto profile...")
                    $update = paimon.updateProfilePicture(paimon.user.jid, {
                        url: config.value,
                    })
                } catch(e) {
                    console.log("Gagal mengganti foto profile...")
                }
                break
            case "profile_status":
                // Update status
                try {
                    console.log("Mencoba mengganti status profile...")
                    $update = paimon.updateProfileStatus(config.value)
                } catch(e) {
                    console.log("Gagal mengganti status profile...")
                }
                break;
            default:
                break;
        }
    }
    let dataConfig = toggleStatusApply(dataMaster)
    let updateStatus = updateFireData(collection_name, dataConfig)
}

const filterUnAppliedConfig = function(dataMaster) {
    let dataFilter = []
    for(let config of dataMaster) {
        if(config.applied) continue;
        dataFilter.push(config)
    }
    return dataFilter
}

const toggleStatusApply = function(dataMaster) {
    let dataFilter = []
    for(let config of dataMaster) {
        let tmp = config
        tmp.applied = true
        dataFilter.push(config)
    }
    return dataFilter
}