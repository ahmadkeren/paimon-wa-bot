const wiki = require('wikipedia');
const changedLang = wiki.setLang('id');

module.exports = {
    name: "Wikipedia Knowledge",
    alias: ["ai_wikipedia","ai_wiki"],
    desc: "Logic paimon: pencarian wikipedia",
    type: "ai",
    example: `!ai_wikipedia indonesia`,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        try {
            const page = await wiki.page(text);
            const summary = await page.summary();
            let jawaban = summary.extract
            return m.reply(jawaban)
        } catch(e) {
            console.log(e)
            return false
        }
    }
}