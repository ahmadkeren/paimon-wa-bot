const google = require('google-it')

module.exports = {
    name: "Google Knowledge",
    alias: ["ai_google"],
    desc: "Logic paimon: pencarian google",
    type: "ai",
    example: `!ai_google indonesia`,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        try {
            let jawaban = "..."
            // Google AI Logic
            await google({
                'query': text+" site:.id",
            }).then(results => {
                results = results
                // return res.json(results)
                for(let result of results) {
                    title = result.title.toLowerCase()
                    result = result.snippet.toLowerCase()
                    // console.log(result)
                    // rejection
                    if(result.includes("berikut") || result.includes("dibawah")) {
                        if(!result.includes("...")) {
                            continue
                        }
                    }
                    // jika terdapat data dari wikipedia
                    if(title.includes("wikipedia")) {
                        // console.log("ketemu: "+result)
                        jawaban = result.split("wikipedia").join("")
                        break
                    }
                    // jika terdapat di kbbi
                    if(title.includes("kbbi")) {
                        jawaban = cleanGoogleSnippet(result)
                        if(jawaban.includes(": "))
                            jawaban = (jawaban.split(": "))[0]
                        break
                    }
                    // jika terdapat definisi
                    if(result.includes("adalah")) {
                        if(!result.includes("...")) {
                            jawaban = cleanGoogleSnippet(result)
                            break
                        }
                    }
                    // jika terdapat didalam pdf
                    if(title.includes("[pdf]")) {
                        if(!result.includes("...")) {
                            jawaban = cleanGoogleSnippet(result)
                            break
                        }
                    }
                    // jika terdapat penjelasan
                    if(result.includes("karena") || result.includes("merupakan") || result.includes("sebab")) {
                        if(!result.includes("...")) {
                            jawaban = cleanGoogleSnippet(result)
                            break
                        }
                    }
                }
                return m.reply(jawaban)
            }).catch(e => {
                console.log(e)
                return false
            })
            // End of Google AI Logic
        } catch(e) {
            console.log(e)
            return false
        }
    }
}

cleanGoogleSnippet = (teks) => {
    if(teks.includes(", · ,")) {
        teks = teks.split(", · ,")
        teks = teks[1]
    }
    if(teks.includes("..."))
        teks = teks.replace("...","")
    if(teks.includes(";")) {
        teks = teks.split(";")
        teks = teks[0]
    }
    return teks
}