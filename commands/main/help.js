module.exports = {
    name: "menu",
    alias: ["help","?","menu"],
    desc: "Lihat daftar menu",
    type: "main",
    exec: async(paimon, m, { commands, args, prefix, text, toUpper, isGIFGuild }) => {
        if (args[0]) {
            let data = []
            let name = args[0].toLowerCase()
            let cmd = commands.get(name) || Array.from(commands.values()).find((v) => v.alias.includes(name))
            if (!cmd || cmd.type == "hide") return m.reply("Command tidak ditemukan!")
            else data.push(`*Nama Menu:* ${cmd.name.replace(/^\w/, c => c.toUpperCase())}`)
            if (cmd.alias) data.push(`*Perintah:* ${cmd.alias.join(", ")}`)
            if (cmd.desc) data.push(`*Deskripsi:* ${cmd.desc}`)
            if (cmd.example) data.push(`*Contoh:* ${cmd.example.replace(/%prefix/gi, prefix).replace(/%command/gi, cmd.name).replace(/%text/gi, text)}`)

            return m.reply(`*Info Menu ${cmd.name.replace(/^\w/, c => c.toUpperCase())}*\n\n${data.join("\n")}`)
        } else {
            try {
                let publicMenu = [];
                for (let type of commands.type) {
                    let folder_name = type.toLowerCase()
                    if(
                        folder_name == "ai" ||
                        folder_name == "admin" ||
                        folder_name == "database" ||
                        folder_name == "hide" ||
                        folder_name == "hidden" ||
                        folder_name == "owner" ||
                        folder_name == "group" ||
                        folder_name == "main"
                    ) continue
                    if(!isGIFGuild && folder_name.includes('gif')) continue;
                    let rowsMenu = []
                    for(let menu of commands.list[type.toLowerCase()]) {
                        if(menu.type == "hide" || menu.type == "hidden"  || menu.type =="ai") continue
                        let tmpMenu = {
                            title: `${menu.name}`,
                            description: `${menu.desc}`,
                            rowId: `${menu.alias[0]}`
                        }
                        rowsMenu.push(tmpMenu)
                    }
                    let menuTypes = {
                        title: type.toUpperCase(),
                        rows: rowsMenu
                    }
                    publicMenu.push(menuTypes)
                }
                const listMessage = {
                    buttonText: 'Daftar Menu',
                    text: `*[PAIMON MENU]*\nOhayo traveler!!! sudah genshinan hari ini? Mau tau apa aja yang paimon bisa? silahkan lihat pada daftar menu dibawah ^_^`,
                    sections: publicMenu,
                    listType: 1
                }
                const sendMsg = await paimon.sendMessage(m.from, listMessage, { quoted: m })
            } catch(e) {
                console.log(e)
            }
        }
    }
}
