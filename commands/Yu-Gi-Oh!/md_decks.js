const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { generateListMenu } = require("../../lib/menu_maker")

module.exports = {
    name: "Kumpulan Deck Master Duel",
    alias: ["md_deck","deck_md","deck"],
    desc: "Lihat referensi deck Yu-Gi-Oh! Master Duel!",
    type: "ygo",
    example: `!md_deck dark magician`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("check", m)
        if(args[0]) {
            // Tampilkan deck ygo
            target_pencarian = text
            try {
                let deckData = await fetchUrl(global.api("gif_handler", "", { action: "md_check_deck", _id: target_pencarian }, "apikey"))
                if (!deckData.success)
                    return m.reply("Gomen.. server master duelnya lagi gangguan");
                let cntKartu = 0;
                deckData = deckData.data
                // console.log(deckData)
                // Filter data
                let res_reply = `╭─「 *REFERENSI DECK* 」\n`
                res_reply    += `│──────────────────\n`
                res_reply    += `│ *Nama Deck*: ${deckData.deckType.name}\n`
                res_reply    += `│ *SR Price*: ${deckData.srPrice} Pts\n`
                res_reply    += `│ *UR Price*: ${deckData.urPrice} Pts\n`
                res_reply    += `│──────────────────\n`
                //  ============== Main Deck ==============
                cntKartu = 0
                res_reply    += `│ *Main Deck:*\n`
                for(let kartu of deckData.main) {
                    res_reply    += `│  - [${kartu.amount}x] ${kartu.card.name}\n`
                    cntKartu+=kartu.amount
                }
                if(deckData.main.length ==0)
                    res_reply    += `│  -\n`
                res_reply    += `│  *Total*: ${cntKartu} Kartu\n`
                res_reply    += `│──────────────────\n`

                //  ============== Extra Deck ==============
                cntKartu = 0
                res_reply    += `│ *Extra Deck:*\n`
                for(let kartu of deckData.extra) {
                    res_reply    += `│  - [${kartu.amount}x] ${kartu.card.name}\n`
                    cntKartu+=kartu.amount
                }
                if(deckData.extra.length ==0)
                    res_reply    += `│  -\n`
                res_reply    += `│  *Total*: ${cntKartu} Kartu\n`
                res_reply    += `│──────────────────\n`

                //  ============== Side Deck ==============
                cntKartu = 0
                res_reply    += `│ *Side Deck:*\n`
                for(let kartu of deckData.side) {
                    res_reply    += `│  - [${kartu.amount}x] ${kartu.card.name}\n`
                    cntKartu+=kartu.amount
                }
                if(deckData.side.length ==0)
                    res_reply    += `│  -\n`
                res_reply    += `│  *Total*: ${cntKartu} Kartu\n`
                res_reply    += `│──────────────────\n`
                res_reply    += `│ Let's Make Your Own Deck ^_^\n`
                res_reply    += `╰──────────────────`
                // console.log(mdMeta)
                let thumbIMG = await fetchBuffer(global.api("ygo_canvas", `/deck`, { q: deckData.deckType.name }, "apikey"))
                let resPesan = {
                    image: thumbIMG,
                    caption: `${res_reply}`
                }
                return await paimon.sendMessage(m.from, resPesan, { quoted: m })
            } catch (e) {
                console.log(e)
                return m.reply("Gomen.. terjadi kesalahan.. silahkan coba lagi nanti!")
            }
            // End Tampilkan deck ygo
        } else {
            // Tampilkan seluruh deck yang tersedia
            let mdMeta = await fetchUrl(global.api("gif_handler", "", { action: "md_deck" }, "apikey"))
            if (!mdMeta.success)
                return m.reply("Gomen.. server master duelnya lagi gangguan");
            mdMeta = mdMeta.data
            // Filter data
            tmpDecks = []
            for(let deck of mdMeta) {
                tmpDecks.push(deck.name)
            }
            // Return data
            let res_reply = `Uhuy!! Paimon punya *${mdMeta.length} data deck* Master Duel. Silahkan pilih salah satunya dibawah traveler.. ^_^`
            let txt_balasan = res_reply
            title = "Daftar Deck"
            cmd = "md_deck"
            desc = "Lihat referensi deck"
            list = [
                {
                    title: title,
                    rows: tmpDecks,
                }
            ]
            balasan = await generateListMenu(title,txt_balasan,cmd,desc,list)
            return await paimon.sendMessage(m.from, balasan, {quoted: m})
        }
    }
}