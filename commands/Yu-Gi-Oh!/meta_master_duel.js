const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Meta Master Duel",
    alias: ["md_meta","meta_md"],
    desc: "Lihat meta Yu-Gi-Oh! Master Duel!",
    type: "ygo",
    example: `!md_meta`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("check", m)
        try {
            let mdMeta = await fetchUrl(global.api("gif_handler", "", { action: "md_meta" }, ""))
            if (!mdMeta.success)
                return m.reply("Gomen.. server meta master duelnya lagi gangguan");
            mdMeta = mdMeta.data
            // TMP data untuk list menu
            let tmp_decks = []
            // Filter data
            let res_reply = `╭─「 *TIER LIST MASTER DUEL* 」\n`
            res_reply    += `│──────────────────\n`
            for(let i=0; i<mdMeta.length; i++) {
                let dt_deck = mdMeta[i];
                if(i==0 && dt_deck.length==0) continue; // Skip jika tidak ada tier 0
                if(i+1 == mdMeta.length)
                    res_reply    += `│ *Potensial Deck(s)*: \n`
                else
                    res_reply    += `│ *Tier ${i} Deck(s)*: \n`
                if(dt_deck.length==0)
                    res_reply    += `│ - _Tidak ada_\n`
                for(let deck of dt_deck) {
                    res_reply    += `│ - ${deck.name} (Power: ${deck.power} pts)\n`
                    tmp_decks.push(deck.name) // masukan data deck ke tmp deck
                }
                res_reply    += `│──────────────────\n`
            }
            res_reply    += `│ Untuk melihat referensi deck secara lengkap silahkan akses *!md_deck* ^_^\n`
            res_reply    += `╰──────────────────`
            // console.log(mdMeta)
            let thumbIMG = await fetchBuffer(global.api("ygo_canvas", `/meta/md`, {}, ""))
            let resPesan = {
                image: thumbIMG,
                caption: `${res_reply}`
            }
            return await paimon.sendMessage(m.from, resPesan, { quoted: m })
        } catch (e) {
            console.log(e)
            return m.reply("Gomen.. terjadi kesalahan.. silahkan coba lagi nanti!")
        }
    }
}