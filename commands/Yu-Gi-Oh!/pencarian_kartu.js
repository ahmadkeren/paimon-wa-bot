const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { templateMD } = require("../../lib/ygo")
const { generateListMenu } = require("../../lib/menu_maker")

module.exports = {
    name: "Pencarian Kartu YGO",
    alias: ["ygo","md"],
    desc: "Melakukan pencarian kartu Yu-Gi-Oh!",
    type: "ygo",
    example: `!ygo dark magician`,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("search", m)
        // Sistem Pencarian Kartu
        try {
            target_pencarian = text
            target_pencarian = target_pencarian.split(" (?)").join("")
            target_pencarian = target_pencarian.split(" (UR)").join("")
            target_pencarian = target_pencarian.split(" (SR)").join("")
            target_pencarian = target_pencarian.split(" (R)").join("")
            target_pencarian = target_pencarian.split(" (N)").join("")
            // console.log(target_pencarian)
            // popRank || name
            let n = await fetchUrl(global.api("md_meta", "/api/v1/cards", { search: target_pencarian, cardSort: "name", page: 1, limit: 100}, "apikey"))
            // Optimalisasi hasil gambar
            if (n.length == 0) return m.reply('Ehe! Paimon gak pernah tau ada kartu itu!')
            if (n.length > 100)
                return m.reply('Kartunya kebanyakan traveler! Paimon pucing T.T')
            if (n.length <= 3) {
                // let isPrinted = false
                for (let no = 0; no < n.length; no++) {
                    kartu_terpilih = n[no]
                    //  === Patch: Kartu di MDM masih link ke duel links, jadi deskripsi tidak valid
                    try {
                        let detail_kartu = await fetchUrl(global.api("ygo_pro_deck", "/api/v7/cardinfo.php", { id: kartu_terpilih.konamiID}, "apikey"))
                        kartu_terpilih.description = detail_kartu.data[0].desc
                    } catch(e) {
                        console.log(e)
                    }
                    //  === Patch: Kartu di MDM masih link ke duel links, jadi deskripsi tidak valid
                    if(!kartu_terpilih.rarity) continue;
                    // isPrinted = true;
                    try {
                        jawaban = templateMD(kartu_terpilih)
                        try {
                            pok = await fetchBuffer(global.api("storage_google", `/ygoprodeck.com/pics/${kartu_terpilih.konamiID}.jpg`, { query: text }, "apikey"))
                            resPesan = {
                                image: pok,
                                caption: `${jawaban}`
                            }
                            await paimon.sendMessage(m.from, resPesan, { quoted: m })
                        } catch (e) {
                            m.reply(jawaban)
                        }
                    } catch (e) {}
                }
                // if(!isPrinted)
                    // return m.reply('Ehe! Paimon tau ada kartu itu, tapi kartu tersebut _belum rilis (atau tidak bisa didapatkan)_ di Yu-Gi-Oh! Master Duel!')
            } else {
                // Kasih menu pilihan
                let exaclySame = false
                tmpKartu = []
                for(let kartu of n) {
                    if(kartu.name.toLowerCase() == target_pencarian.toLowerCase() && !exaclySame) {
                        // Kartu exacly same name
                        exaclySame = true
                        try {
                            jawaban = templateMD(kartu)
                            try {
                                pok = await fetchBuffer(global.api("storage_google", `/ygoprodeck.com/pics/${kartu.konamiID}.jpg`, { query: text }, "apikey"))
                                resPesan = {
                                    image: pok,
                                    caption: `${jawaban}`
                                }
                                await paimon.sendMessage(m.from, resPesan, { quoted: m })
                            } catch (e) {
                                m.reply(jawaban)
                            }
                        } catch (e) {}
                    } else {
                        if(!kartu.rarity || kartu.rarity=="" || kartu.rarity==null)
                            kartu.rarity = "?"
                        tmpKartu.push(`${kartu.name} (${kartu.rarity})`)
                    }
                }
                // Remove duplikat
                tmpKartu = Array.from(new Set(tmpKartu))
                // Return data
                let res_reply = ``
                if(exaclySame)
                    res_reply = `Selain dari kartu diatas, paimon mempunyai *${tmpKartu.length} kartu* Yu-Gi-Oh! yang berkaitan dengan *${target_pencarian}*. Silahkan pilih salah satunya dibawah traveler.. ^_^`
                else
                    res_reply = `Uhuy!! Paimon menemukan *${tmpKartu.length} kartu* Yu-Gi-Oh! yang berkaitan dengan *${target_pencarian}*. Silahkan pilih salah satunya dibawah traveler.. ^_^`
                let txt_balasan = res_reply
                title = `Pencarian Kartu: ${target_pencarian}`
                cmd = "ygo"
                desc = "Lihat detail kartu"
                list = [
                    {
                        title: title,
                        rows: tmpKartu,
                    }
                ]
                balasan = await generateListMenu('Lihat Hasil Pencarian',txt_balasan,cmd,desc,list)
                return await paimon.sendMessage(m.from, balasan, {quoted: m})
            }
        } catch(e) {
            console.log(e)
            return m.reply('Ehe! terjadi masalah pada server kartu Yu-Gi-Oh!')
        }
        // End Sistem Pencarian Kartu
    }
}