const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Random Member Picker",
    alias: ["random_picker","random_pick"],
    desc: "Pilih dan tag secara acak member yang ada didalam group",
    type: "group",
    example: `!random_pick <jumlah> (tanpa tanda <>)`,
    isGroup: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper, participants }) => {
        try {

            // Inisialisasi
            let lucky_num = 0;
            try {
                lucky_num = parseInt(text)
            } catch (e) {
                console.log(e)
            }

            // Validasi
            if (lucky_num == NaN || lucky_num == undefined || !Number.isInteger(lucky_num))
                return m.reply("Hanya masukkan bilangan bulat traveler!")
            if (lucky_num < 1)
                return m.reply('Variabel tidak boleh dibawah 1 traveler :")')
            if (lucky_num > 10)
                return m.reply("Value maksimal lucky member adalah 10 orang")
            if (lucky_num >= participants.length)
                return m.reply("Value tidak boleh melebihi atau sama dengan jumlah member!")

            // Handling
            let members_id = []
            for (let i = 0; i < lucky_num; i++) {
                let randIndex = Math.floor(Math.random() * participants.length)
                let luckyOne = participants[randIndex]
                members_id.push(luckyOne.id)
            }

            // Return message
            // console.log(members_id)
            let teks = `╔═  *Random Picker System* ══\n`
            for (let mem of members_id) {
                teks += `╠➥ @${mem.split('@')[0]}\n`
            }
            teks  += `╚═ Lucky ${lucky_num}/${participants.length} Members ══`
            await paimon.sendMessage(m.from, { text: teks, mentions: members_id }, { quoted: m })
        } catch(e) {
            console.log(e)
            return m.reply(`Gagal melakukan random pick`)
        }
    }
}