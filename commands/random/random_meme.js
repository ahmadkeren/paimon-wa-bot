const { fetchUrl, fetchBuffer, pickArray } = require("../../lib/Function")

module.exports = {
    name: "Random Meme",
    alias: ["meme"],
    desc: "Lihat random meme",
    type: "random",
    example: `!meme`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("search", m)
        try {
            let search = await fetchUrl(global.api("some_random_api", `/meme`, {}, ""))
            let imgChars = await fetchBuffer(`${search.image}`)
            let resPesan = {
                image: imgChars,
                caption: `${search.caption}\n#${search.category}`
            }
            await paimon.sendMessage(m.from, resPesan, { quoted: m })
        } catch (e) {
            console.log(e)
            return m.reply("Waduh!! gambarnya dibawa lari natsu...")
        }
    }
}