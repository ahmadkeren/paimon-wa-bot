const { fetchUrl, fetchBuffer, numberWithCommas } = require("../../lib/Function")
const { cariTraveler } = require("../../lib/gif")
const { formPerkenalkanDiri, travelerIdentity} = require("../../lib/template")
const { generateListMenu } = require("../../lib/menu_maker")

module.exports = {
    name: "Karakter Publik Saya",
    alias: ["profile","profil","public","publik"],
    desc: "Lihat karakter publik Genshin Impact saya",
    type: "gif",
    example: `!publik <tag> atau !publik`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("check", m)
        // Initial Parameter
        let selfCheck = true
        let target_pencarian = m.sender
        if(m.mentions.length > 0) {
            selfCheck = false
            target_pencarian = m.mentions[0]
        }
        try {
            // Cari anggota
            let cari = await cariTraveler(target_pencarian)
            if (cari == null || !cari)
                return m.reply(`Gomen... UIDnya tidak ditemukan traveler!`)
            // Cetak data
            for (let lmt of cari) {
                // belum registrasi
                if (lmt["WhatsApp_ID"] == target_pencarian && lmt["Status"] == "Blm. Registrasi") {
                    if (selfCheck) {
                        teks = `Oops! kamu belum melakukan registrasi!\n`
                        teks += formPerkenalkanDiri()
                    } else
                        teks = `Oops! Member yang bersangkutan belum melakukan registrasi!`
                    return m.reply(teks)
                } else
                if (lmt["WhatsApp_ID"] == target_pencarian) {
                    const api = global.api("gif_canvas", "/api/profile/"+lmt["UID"])
                    const public_chars = await fetchUrl(api.split("?").join(""))
                    let data = []
                    if(!public_chars) return m.reply(`Tidak ditemukan traveler dengan UID *${lmt["UID"]}*`)
                    // console.log(public_chars)
                    // Prepare balasan
                    let balasan = `╭─「 *PROFIL PUBLIK TRAVELER* 」\n`
                    balasan = await travelerIdentity(balasan, lmt)
                    balasan += `│──────────────────\n`
                    balasan += `│ *Daftar Karakter Publik*\n`
                    if(public_chars.data.length==0) {
                        balasan += `│ _Tidak ada karakter milik ${lmt["IGN"]} yang dipublikasi_\n`
                    } else {
                        for(let char of public_chars.data) {
                            balasan += `│ - ${char.name} (Lvl. ${char.level}/${char.max_lvl})\n`
                            data.push(char.name)
                        }
                    }
                    if(data.length>0) {
                        balasan += `│──────────────────\n`
                        balasan += `│ _*Untuk melihat detail publik karakter silahkan akses melalui menu dibawah*_\n`
                    }
                    balasan += `╰──────────────────`
                    if(data.length==0)
                        return m.reply(balasan)
                    // Generate list
                    title = `Daftar Karakter`
                    let txt_balasan = balasan
                    cmd = `public_chars_show ${lmt["UID"]}/${lmt["IGN"]}/`
                    desc = "Lihat rincian karakter"
                    list = [
                        {
                            title: `Karakter Publik Milik ${lmt["IGN"]}`,
                            rows: data
                        },
                    ]
                    balasan = await generateListMenu(title,txt_balasan,cmd,desc,list)
                    return await paimon.sendMessage(m.from, balasan, {quoted: m})
                break
                }
            }
        } catch(e) {
            console.log(e)
            return m.reply(`Server sedang gangguan, coba lagi nanti!`)
        }
    }
}