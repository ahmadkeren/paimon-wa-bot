const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { cariTraveler, getTravelerCookies, getDataFromCookie } = require("../../lib/gif")
const { formPerkenalkanDiri, benefitCookies } = require("../../lib/template")

module.exports = {
    name: "Submit Cookie",
    alias: ["submit_cookie"],
    desc: "Lihat cara submit cookie kamu ke paimon",
    type: "gif",
    example: `!submit_cookie <kode_cookie> (tanpa tanda < >)`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        try {
            if(m.isGroup) {
                if(args[0]) {
                    // Didalam Group + memasukan parameter || Cekal dia
                    balasan  = `Traveler!! tolong kirim chat ini via PM (Private Message)..\n`
                    balasan += `Untuk menjaga keamanan, silahkan hapus chat ini...`
                    return m.reply(`${balasan}`)
                } else {
                    // Didalam Group dan tidak memasukan parameter
                    balasan = `Untuk mensubmit cookie silahkan chat pribadi Paimon!\n`
                    balasan = benefitCookies(balasan)
                    return m.reply(balasan)
                }
            } else {
                if(args[0]) {
                    // Tampilkan submit cookie
                    let pesan_cookie = text
                    if (pesan_cookie.length < 10)
                        return m.reply('Maaf COOKIE terlalu singkat, minimal terdiri dari 10 karakter!')
                    if (!pesan_cookie.includes("ltoken") || !pesan_cookie.includes("ltuid"))
                        return m.reply(`Oops! Cookie yang kamu submit tidak valid!`)
                    global.mess("check", m)
                    // Cek keanggotaan GIF
                    target_pencarian = m.sender
                    let cari = await cariTraveler(target_pencarian)
                    if (cari == null || !cari)
                        return m.reply(`Gomen... UIDnya tidak ditemukan traveler!`)
                    // Inisialisasi cookie
                    dataSaya = cari[0];
                    let dataCookie = await getDataFromCookie(pesan_cookie);
                    let myLtuid = dataCookie["ltuid"];
                    let myLtoken = dataCookie["ltoken"];
                    // Cek validitas cookie
                    let cekCookies = await fetchUrl(global.api("gif_api_stats", "/note", { uid: dataSaya["UID"], ltuid: myLtuid, ltoken: myLtoken}, ""))
                    if (cekCookies.error) {
                        if (cekCookies.error.includes("incorrect"))
                            return m.reply("Cookie yang kamu submit invalid atau sudah kadaluarsa!")
                    }
                    // Check apakah sudah pernah submit + cookies reminder
                    submitedCookie = await getTravelerCookies(dataSaya["UID"]);
                    // console.log(submitedCookie)
                    let resinReminder = "off"
                    if(submitedCookie != null) {
                        resinReminder = submitedCookie.reminder
                    }
                    // Inisialisasi data baru
                    let data_baru = {
                        "data": [{
                            "uid": dataSaya["UID"],
                            "owner_data": dataSaya["Nama"],
                            "reminder": resinReminder,
                            // "data_cookie": `ltuid=${myLtuid}; ltoken=${myLtoken}`,
                            "data_cookie": `${pesan_cookie}`,
                        }]
                    };
                    // Handle submit |OR| update data
                    methodReq = "put"
                    if(submitedCookie.ltuid==null)
                        methodReq = "tambah";
                    let anu = await cookieDBEngine(methodReq, dataSaya["UID"], data_baru)
                    console.log(anu)
                    try {
                        if (anu.error) {
                            console.log(anu.error)
                            return m.reply(`Gomen... Cookie gagal disubmit! Silahkan coba lagi nanti!`)
                        }
                        return m.reply(`Yey! COOKIE berhasil disubmit :)`)
                    } catch (e) {
                        console.log(e)
                        return m.reply("Gomen.. Cookie gagal disubmit!")
                    }
                    // End Tampilkan submit cookie
                } else {
                    // Tampilkan tutorial submit_cookie
                    balasan = `Masukkan COOKIE kamu traveler!\n`
                    balasan = benefitCookies(balasan)
                    return m.reply(balasan)
                }
            }
        } catch(e) {
            console.log("Cookie sistem bermasalah, alasan:")
            console.log(e)
            return m.reply("Oops! sedang ada masalah teknis, coba lagi nanti!")
        }
    }
}