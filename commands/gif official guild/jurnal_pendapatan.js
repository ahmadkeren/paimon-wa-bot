const { fetchUrl, fetchBuffer, numberWithCommas, getNamaBulan } = require("../../lib/Function")
const { cariTraveler, getTravelerCookies } = require("../../lib/gif")
const { formPerkenalkanDiri, travelerIdentity } = require("../../lib/template")

module.exports = {
    name: "Jurnal Pendapatan",
    alias: ["jurnal","income","incomes"],
    desc: "Lihat perolehan primogem dan mora dalam bulan dan hari ini",
    type: "gif",
    example: `!jurnal`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("check", m)
        // Initial Parameter
        let selfCheck = true
        let target_pencarian = m.sender
        if(m.mentions.length > 0)
            return m.reply(`Gomen... Fitur ini hanya dapat digunakan untuk pengecekan diri sendiri`)
        try {
            // Cari anggota
            let cari = await cariTraveler(target_pencarian)
            if (cari == null || !cari)
                return m.reply(`Gomen... UIDnya tidak ditemukan traveler!`)
            // Cetak data
            for (let lmt of cari) {
                // belum registrasi
                if (lmt["WhatsApp_ID"] == target_pencarian && lmt["Status"] == "Blm. Registrasi") {
                    if (selfCheck) {
                        teks = `Oops! kamu belum melakukan registrasi!\n`
                        teks += formPerkenalkanDiri()
                    } else
                        teks = `Oops! Member yang bersangkutan belum melakukan registrasi!`
                    return m.reply(teks)
                } else
                if (lmt["WhatsApp_ID"] == target_pencarian) {
                    let balasan = `╭─「 *JURNAL PENDAPATAN* 」\n`
                    balasan = await travelerIdentity(balasan, lmt)
                    showTodoList = false;
                    needSubmitCookie = false;
                    try {
                        dataCookie = await getTravelerCookies(lmt['UID'])
                        if (dataCookie == null) {
                            balasan += `│──────────────────\n`
                            balasan += `│ Jurnal pendapatan tidak dapat ditampilkan karena ${lmt["IGN"]} *belum pernah* mensubmit cookie!\n`
                            needSubmitCookie = true;
                        } else {
                            // Proses ambil data catatan harian
                            let hyb = await fetchUrl(global.api("gif_official", "/jurnal", { uid: lmt["UID"], ltuid: dataCookie.ltuid, ltoken: dataCookie.ltoken }, "apikey"))
                            if (hyb.error) {
                                errorMSG = hyb.error
                                // Filter error
                                if (hyb.error.includes("incorrect"))
                                    errorMSG = "Cookie yang kamu submit invalid atau sudah kadaluarsa!"
                                if (hyb.error.includes("herokuapp"))
                                    errorMSG = "Server API sedang sibuk!"
                                if (hyb.error.includes("genshinstats"))
                                    errorMSG = "Server API sedang maintenance!"
                                // Return error
                                balasan += `│──────────────────\n`
                                balasan += `│ *Oops!* server sedang mengalami gangguan, silahkan coba lagi nanti!\n`
                                console.log(errorMSG)
                            } else {
                                jurnal = hyb
                                balasan += `│──────────────────\n`
                                balasan += `│ *Overview Pendapatan Bulan ${getNamaBulan(jurnal.month)}*:\n`
                                balasan += `│ *• 💎 Total Primogem*: ${numberWithCommas(jurnal.data.current_primogems)} Primo\n`
                                balasan += `│ *• 💰 Total Mora*: ${numberWithCommas(jurnal.data.current_mora)} Mora\n`
                                if(jurnal.data.primogems_rate>0)
                                    balasan += `│ *• 📈 Rate Primo*: +${jurnal.data.primogems_rate}% dari bulan lalu\n`
                                else
                                    balasan += `│ *• 📉 Rate Primo*: ${jurnal.data.primogems_rate}% dari bulan lalu\n`
                                if(jurnal.data.mora_rate>0)
                                    balasan += `│ *• 📈  Rate Mora*: +${jurnal.data.mora_rate}% dari bulan lalu\n`
                                else
                                    balasan += `│ *• 📉 Rate Mora*: ${jurnal.data.mora_rate}% dari bulan lalu\n`
                                balasan += `│──────────────────\n`
                                balasan += `│ *Detail Perolehan Bulan Ini*:\n`
                                for(let jenis of jurnal.data.categories) {
                                    balasan += `│ - *${jenis.name}*: ${numberWithCommas(jenis.amount)} Primo\n`
                                }
                                balasan += `│──────────────────\n`
                                balasan += `│ *Pendapatan Hari Ini*:\n`
                                balasan += `│ *• 💎 Primogem*: ${numberWithCommas(jurnal.day_data.current_primogems)} Primo\n`
                                balasan += `│ *• 💰 Mora*: ${numberWithCommas(jurnal.day_data.current_mora)} Mora\n`
                                // console.log(jurnal)
                            }
                        }
                    } catch (e) {
                        balasan += `│──────────────────\n`
                        // balasan += `│ *Error*: ${e}\n`
                        balasan += `│ *Error*: Data tidak dapat diambil! pastikan cookie belum expired!\n`
                        console.log(e);
                    }
                    if (needSubmitCookie) {
                        balasan += `│──────────────────\n`
                        balasan += `│ *Info*: _Untuk mensubmit cookie silahkan akses *!submit_cookie* secara pribadi ke Paimon_\n`
                    }
                    showMTServer = false
                    if (showMTServer && !needSubmitCookie) {
                        balasan += `│──────────────────\n`
                        balasan += `│ *Catatan*: _Mihoyo sedang melakukan perbaikan server, mungkin beberapa data tidak akurat._\n`
                    }
                    showInfoTambahan = false
                    if (showInfoTambahan) {
                        balasan += `│──────────────────\n`
                        balasan += `│ *Catatan*: _Pendapatan dari konversi genesis crystal atau blessing of the weelking moon tidak dihitung._\n`
                    }
                    balasan += `╰──────────────────`
                    return m.reply(balasan)
                    break
                }
            }
        } catch(e) {
            console.log(e)
            return m.reply(`Server sedang gangguan, coba lagi nanti!`)
        }
        return m.reply('Are... boom boom bakudan?!')
    }
}