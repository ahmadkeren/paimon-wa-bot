const { fetchUrl, fetchBuffer, numberWithCommas } = require("../../lib/Function")
const { cariTraveler } = require("../../lib/gif")
const { formPerkenalkanDiri, travelerIdentity} = require("../../lib/template")

module.exports = {
    name: "Kartu Member / UID",
    alias: ["uid"],
    desc: "Lihat detail keanggotaan saya di GIF",
    type: "gif",
    example: `!uid <tag> atau !uid`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("check", m)
        // Initial Parameter
        let selfCheck = true
        let target_pencarian = m.sender
        if(m.mentions.length > 0) {
            selfCheck = false
            target_pencarian = m.mentions[0]
        }
        try {
            // Cari anggota
            let cari = await cariTraveler(target_pencarian)
            if (cari == null || !cari)
                return m.reply(`Gomen... UIDnya tidak ditemukan traveler!`)
            // Cetak data
            for (let lmt of cari) {
                // belum registrasi
                if (lmt["WhatsApp_ID"] == target_pencarian && lmt["Status"] == "Blm. Registrasi") {
                    if (selfCheck) {
                        teks = `Oops! kamu belum melakukan registrasi!\n`
                        teks += formPerkenalkanDiri()
                    } else
                        teks = `Oops! Member yang bersangkutan belum melakukan registrasi!`
                    return m.reply(teks)
                } else
                if (lmt["WhatsApp_ID"] == target_pencarian) {
                    let balasan = `╭─「 *DETAIL MEMBER GIF* 」\n`
                    balasan = await travelerIdentity(balasan, lmt)
                    if (lmt["UID"] != "" && lmt["UID"] != null && lmt["UID"] != "805093361" && lmt["UID"] != 805093361)
                        try {
                            let hoyolab = await fetchUrl(global.api("gif_official", "/traveler", { uid: lmt["UID"]}, ""))
                            // return console.log(global.api("gif_official", "/traveler", { uid: lmt["UID"]}, ""))
                            hyb = hoyolab
                            if (!hyb.error && !hyb.pesan) {
                                balasan += `│──────────────────\n`
                                balasan += `│ *• Achievement:* ${hyb.stats.achievements} tercapai\n`
                                balasan += `│ *• Hari Aktif:* ${hyb.stats.days_active} hari\n`
                                balasan += `│ *• Char Dimiliki:* ${hyb.stats.characters} karakter\n`
                                /* Fitur ini dimaintenance
                                try {
                                    total_level = 0
                                    total_best_companion = 0
                                    for (let char of hyb.characters) {
                                        total_level += char.level
                                        if (char.friendship >= 10)
                                            total_best_companion += 1
                                    }
                                    avg_level = total_level / hyb.characters.length
                                    balasan += `│ *• Max Friendship:* ${total_best_companion} karakter\n`
                                    balasan += `│ *• Rerata Char:* Level ${parseInt(avg_level)}\n`
                                } catch (e) {}
                                */
                                try {
                                    explored = 0
                                    for (let eksplorasi of hyb.explorations) {
                                        explored += eksplorasi.explored
                                    }
                                    eksplorasi = explored / hyb.explorations.length
                                    balasan += `│ *• Eksplorasi:* ${Math.round(eksplorasi * 100) / 100}%\n`
                                } catch (e) {}
                                try {
                                    chest_dibuka = hyb.stats.common_chests + hyb.stats.exquisite_chests + hyb.stats.luxurious_chests + hyb.stats.precious_chests + hyb.stats.remarkable_chests
                                    balasan += `│ *• Chest Dibuka:* ${numberWithCommas(chest_dibuka)} peti\n`
                                } catch (e) {}
                                // Optimalisasi data spiral abyss
                                try {
                                    spa = hyb.spiral_abyss[0];
                                    balasan += `│──────────────────\n`
                                    balasan += `│ *• Spiral Abyss:* ${spa.max_floor}\n`
                                    balasan += `│ *• Abyss Stars:* ${spa.total_stars} Stars\n`
                                    // hanya hitung lt 9 - 12
                                    abyss_star = 0
                                    try {
                                        for (let floor of spa.floors) {
                                            if (floor.floor >= 9) {
                                                abyss_star += floor.stars
                                            }
                                        }
                                        balasan += `│ *• End Game Stars:* ${abyss_star} Stars\n`
                                    } catch (e) {
                                        console.log(e)
                                    }
                                    // kalkulasi Effort
                                    try {
                                        win_rate = 0
                                        if (spa.total_battles > 0)
                                            win_rate = spa.total_wins / spa.total_battles * 100
                                        balasan += `│ *• Win Rate:* ${Math.round(win_rate * 100) / 100}%\n`
                                        // Title / Pangkat
                                        pangkat = "Army Student"
                                        if (win_rate >= 95 && abyss_star >= 36) pangkat = "Lord Marshal";
                                        else
                                        if (win_rate >= 90 && abyss_star >= 36) pangkat = "Field Marshal";
                                        else
                                        if (win_rate >= 80 && abyss_star >= 36) pangkat = "General";
                                        else
                                        if (win_rate >= 70 && abyss_star >= 36) pangkat = "Liutenant-General";
                                        else
                                        if (win_rate >= 60 && abyss_star >= 36) pangkat = "Major-General";
                                        else
                                        if (win_rate >= 50 && abyss_star >= 36) pangkat = "Brigadier";
                                        else
                                        if (win_rate >= 40 && abyss_star >= 36) pangkat = "Colonel";
                                        else
                                        if (win_rate >= 30 && abyss_star >= 36) pangkat = "Liutenant-Colonel";
                                        else
                                        if (win_rate >= 20 && abyss_star >= 36) pangkat = "Major";
                                        else
                                        if (win_rate >= 10 && abyss_star >= 36) pangkat = "Captain";
                                        else
                                        if (win_rate >= 0 && abyss_star >= 36) pangkat = "Liutenant";
                                        else
                                        if (abyss_star >= 33) pangkat = "Second Liutenant";
                                        else
                                        if (abyss_star >= 30) pangkat = "Officer Cadet";
                                        else
                                        if (abyss_star >= 27) pangkat = "Warrant Officer";
                                        else
                                        if (abyss_star >= 24) pangkat = "Colour Sergeant";
                                        else
                                        if (abyss_star >= 21) pangkat = "Sergeant";
                                        else
                                        if (abyss_star >= 18) pangkat = "Corporal";
                                        else
                                        if (abyss_star >= 15) pangkat = "Lance Corporal";
                                        else
                                        if (abyss_star >= 12) pangkat = "Private";
                                        balasan += `│ *• Honour:* ${pangkat}\n`
                                        // Effort
                                        pujian = "-"
                                        if (win_rate >= 90) pujian = "Unbelivable";
                                        else
                                        if (win_rate >= 80) pujian = "Very Easy";
                                        else
                                        if (win_rate >= 70) pujian = "Easy";
                                        else
                                        if (win_rate >= 50) pujian = "Intermediate";
                                        else
                                        if (win_rate >= 40) pujian = "Hard";
                                        else
                                        if (win_rate >= 30) pujian = "Very Hard";
                                        else
                                        if (win_rate >= 1) pujian = "Hell";
                                        balasan += `│ *• Attempt:* ${pujian}\n`
                                    } catch (e) {
                                        console.log(e)
                                    }
                                } catch (e) {}
                                try {
                                    if (spa.ranks.strongest_strike.length > 0 && spa.floors.length>0) {
                                        // Tampilkan MVP
                                        mvp = spa.ranks.strongest_strike[0]
                                        balasan += `│ *• MVP:* ${mvp.name} (${numberWithCommas(mvp.value)} DMG)\n`
                                    }
                                } catch (e) {
                                    console.log(e)
                                }
                                try {
                                    // teapot
                                    if (hyb.teapot == null) {
                                        balasan += `│──────────────────\n`
                                        balasan += `│ *• Teapot:* Belum terbuka\n`
                                    } else {
                                        balasan += `│──────────────────\n`
                                        balasan += `│ *• Teapot:* Level ${hyb.teapot.level}\n`
                                        balasan += `│ *• Perabot:* ${numberWithCommas(hyb.teapot.items)} barang\n`
                                        balasan += `│ *• Energi Adeptus:* ${numberWithCommas(hyb.teapot.comfort)}\n`
                                        balasan += `│ *• Pengunjung:* ${numberWithCommas(hyb.visitors)} orang\n`
                                    }
                                } catch (e) {}
                            }
                            console.log(hyb)
                        } catch (e) {
                            console.log(e)
                        }
                    if (selfCheck) {
                        balasan += `│──────────────────\n`
                    }
                    if (selfCheck) {
                        balasan += `│ *!update_uid* <uid>\n`
                        balasan += `│ *!update_ign* <ign>\n`
                        balasan += `│ *!update_signature* <signature>\n`
                    }
                    balasan += `│──────────────────\n`
                    balasan += `│ *Come and Play with Me :)*\n`
                    try {
                        if (selfCheck) {
                            if (hyb.error) {
                                balasan += `│──────────────────\n`
                                if (hyb.error.includes("not public"))
                                    balasan += `│ _*Untuk menampilkan detail perjalanan di tevyat silahkan akses https://paimon.page.link/access_\n`
                                else
                                    balasan += `│ _*Server Mihoyo sedang gangguan, data pengembara tidak dapat ditampilkan*_\n`
                            }
                        }
                    } catch (e) {}
                    balasan += `╰──────────────────`
                    // Kirim beserta kartu id/tidak
                    try {
                        pok = await fetchBuffer(global.api("gif_canvas", `/gif/${lmt["UID"]}`, {}, ""))
                        resPesan = {
                            image: pok,
                            caption: `${balasan}`
                        }
                        return await paimon.sendMessage(m.from, resPesan, { quoted: m })
                    } catch (e) {
                        console.log(e)
                        return m.reply(balasan)
                    }
                break
                }
            }
        } catch(e) {
            console.log(e)
            return m.reply(`Server sedang gangguan, coba lagi nanti!`)
        }
    }
}