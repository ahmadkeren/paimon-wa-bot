const { fetchUrl, fetchBuffer, numberWithCommas } = require("../../lib/Function")
const { cariTraveler } = require("../../lib/gif")
const { formPerkenalkanDiri, travelerIdentity} = require("../../lib/template")
const { generateListMenu } = require("../../lib/menu_maker")

module.exports = {
    name: "Detail Karakter Publik",
    alias: ["public_chars_show"],
    desc: "Lihat detail karakter dari public profile traveler",
    type: "hide",
    example: `!public_chars_show <uid>/<ign>/<query>`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        // global.mess("check", m)
        // Initial Parameter
        let query_params = text.split("/ ").join("/").toString()
        query_params = query_params.split("/")
        let uid = null
        let ign = null
        let query = null
        try {
            uid = query_params[0]
            ign = query_params[1]
            query = query_params[2]
        } catch(e) {
            console.log(e)
            return m.reply(`Metode akses tidak valid!! Silahkan akses melalui menu *!profil*`)
        }
        try {
            const api = global.api("gif_canvas", `/profile/${uid}/${query}`, {}, "")
            let imgRender = await fetchBuffer(api.split("?").join(""))
            let resPesanTwo = {
                image: imgRender,
                caption: `Build Karakter *${query}* by *${ign}* (${uid})`
            }
            return await paimon.sendMessage(m.from, resPesanTwo, { quoted: m })
        } catch(e) {
            console.log(e)
            return m.reply(`Server sedang gangguan, coba lagi nanti!`)
        }
    }
}