const { fetchUrl, fetchBuffer, numberWithCommas, secondsToHms } = require("../../lib/Function")
const { cariTraveler, getTravelerCookies } = require("../../lib/gif")
const { formPerkenalkanDiri, travelerIdentity } = require("../../lib/template")

module.exports = {
    name: "Periksa Resin",
    alias: ["resin","realm_currency","daily","cek_resin"],
    desc: "Periksa jumlah resin kamu saat ini",
    type: "gif",
    example: `!resin`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper, isOwner }) => {
        global.mess("check", m)
        // Initial Parameter
        let selfCheck = true
        let target_pencarian = m.sender
        if(m.mentions.length > 0)
                return m.reply(`Gomen... Fitur ini hanya dapat digunakan untuk pengecekan diri sendiri`)
        try {
            // Cari anggota
            let cari = await cariTraveler(target_pencarian)
            if (cari == null || !cari)
                return m.reply(`Gomen... UIDnya tidak ditemukan traveler!`)
            // Cetak data
            for (let lmt of cari) {
                // belum registrasi
                if (lmt["WhatsApp_ID"] == target_pencarian && lmt["Status"] == "Blm. Registrasi") {
                    if (selfCheck) {
                        teks = `Oops! kamu belum melakukan registrasi!\n`
                        teks += formPerkenalkanDiri()
                    } else
                        teks = `Oops! Member yang bersangkutan belum melakukan registrasi!`
                    return m.reply(teks)
                } else
                if (lmt["WhatsApp_ID"] == target_pencarian) {
                    let balasan = `╭─「 *CATATAN HARIAN TRAVELER* 」\n`
                    balasan = await travelerIdentity(balasan, lmt)
                    showTodoList = false;
                    needSubmitCookie = false;
                    try {
                        dataCookie = await getTravelerCookies(lmt["UID"]);
                        if(dataCookie == null) {
                            balasan += `│──────────────────\n`
                            balasan += `│ Data catatan harian tidak dapat ditampilkan karena kamu *belum pernah* mensubmit cookie!\n`
                            needSubmitCookie = true;
                        } else {
                            // Olah Cookie
                            // Proses ambil data catatan harian
                            // return console.log(global.api("gif_official", "/note", { uid: lmt["UID"], ltuid: dataCookie.ltuid, ltoken: dataCookie.ltoken}, ""))
                            let resin = await fetchUrl(global.api("gif_official", "/note", { uid: lmt["UID"], ltuid: dataCookie.ltuid, ltoken: dataCookie.ltoken}, ""))
                            hyb = resin;
                            // console.log(hyb)
                            //console.log(resin);
                            showTodoList = true;
                            isError = false;
                            if(resin.error)
                                isError = true;
                            if(resin.pesan) {
                                if(!resin.success)
                                    isError = true;
                            }
                            if (isError) {
                                if(resin.error) errorMSG = resin.error
                                if(resin.pesan) errorMSG = resin.pesan
                                // Filter error
                                if (errorMSG.includes("incorrect") || errorMSG.includes("not valid"))
                                    errorMSG = "Cookie yang kamu submit invalid atau sudah kadaluarsa!";
                                else if (errorMSG.includes("not public"))
                                    errorMSG = "Kamu belum mengaktifkan fitur \"Catatan Harian Realtime\" di Hoyolab! Silahkan aktifkan fitur tersebut terlebih dahulu!";
                                else if (errorMSG.includes("herokuapp"))
                                    errorMSG = "Server API sedang sibuk!";
                                else if (errorMSG.includes("genshinstats"))
                                    errorMSG = "Server API sedang maintenance!";
                                else {
                                    console.log(errorMSG)
                                    errorMSG = "Data tidak dapat ditampilkan!";
                                }
                                // Return error
                                balasan += `│──────────────────\n`
                                balasan += `│ *Oops!* ${errorMSG}\n`
                                showTodoList = false;
                            } else {
                                // Hitung ekspedisi Selesai
                                ekspedisiDone = 0;
                                for (let ekspedisi of hyb.expeditions) {
                                    if (ekspedisi.status == "Finished")
                                        ekspedisiDone += 1;
                                }
                                label_daily_claim = "sudah";
                                if (!hyb.claimed_commission_reward)
                                    label_daily_claim = "belum";
                                label_parametic = "telah digunakan";
                                if (hyb.remaining_transformer_recovery_time <= 0)
                                    label_parametic = "belum digunakan";
                                // Return text
                                balasan += `│──────────────────\n`
                                balasan += `│ *• 🌙 Original Resin*: ${hyb.current_resin}/${hyb.max_resin}\n`
                                balasan += `│ *• 🪙 Realm Currency*: ${numberWithCommas(hyb.current_realm_currency)}/${numberWithCommas(hyb.max_realm_currency)}\n`
                                // balasan += `│ *• 🎉 Klaim Misi Harian*: ${label_daily_claim}\n`
                                // balasan += `│ *• 📃 Misi Harian*: ${hyb.completed_commissions}/${hyb.max_commissions} selesai\n`
                                balasan += `│ *• 🛩️ Utus Ekspedisi*: ${hyb.expeditions.length - ekspedisiDone}/${hyb.max_expeditions} diutus\n`
                                balasan += `│ *• 🐲 Diskon Weekly Boss*: ${hyb.max_resin_discounts-hyb.remaining_resin_discounts}/${hyb.max_resin_discounts} terpakai\n`
                                balasan += `│ *• ♻️ Parametic T*: ${label_parametic}\n`
                                // Sistem todo list
                                emergencyResin = false;
                                fullResin = false;
                                dailyKlaim = hyb.claimed_commission_reward;
                                dailyCommision = true;
                                ekspedisiDone = ekspedisiDone; //reinisialisasi biar gak lupa
                                weeklyBossDone = true;
                                emergencyTeapot = false;
                                fullTeapot = false;
                                parameticUsed = true;
                                // Toogle todo list
                                if (hyb.current_resin == hyb.max_resin)
                                    fullResin = true; else
                                if (hyb.max_resin - hyb.current_resin <= 40)
                                    emergencyResin = true;
                                if (!hyb.claimed_commission_reward) dailyKlaim = false; // hide
                                if (hyb.completed_commissions != hyb.max_commissions) dailyCommision = false; // hide
                                if (hyb.remaining_resin_discounts > 0) weeklyBossDone = false;
                                // Teapot
                                if(hyb.max_realm_currency > 0) {
                                    if (hyb.current_realm_currency == hyb.max_realm_currency)
                                        fullTeapot = true; else
                                    if (hyb.max_realm_currency - hyb.current_realm_currency <= 40)
                                        emergencyTeapot = true;
                                }
                                // Parametic
                                if (hyb.remaining_transformer_recovery_time <= 0)
                                    parameticUsed = false;
                                // Karena API mioyo gak jelas pada data misi harian, force datanya
                                dailyKlaim = true; // unhide
                                dailyCommision = true; // unhide
                            }
                            // End Olah Cookie
                        }
                    } catch (e) {
                        balasan += `│──────────────────\n`
                        balasan += `│ *Error*: Sever sedang maintenance!\n`
                        console.log(e);
                    }
                    if (showTodoList) {
                        balasan += `│──────────────────\n`
                        if (!emergencyResin && !fullResin && dailyKlaim && ekspedisiDone == 0 && weeklyBossDone && !emergencyTeapot && !fullTeapot && parameticUsed)
                            balasan += `│ *Todo List*: -\n`
                        else {
                            balasan += `│ *Todo List*:\n`
                            if (fullResin)
                                balasan += `│ - Resin penuh, konversi atau gunakan\n`;
                            else if (emergencyResin)
                                balasan += `│ - Resin hampir penuh, konversi atau gunakan\n`
                            if (fullTeapot)
                                balasan += `│ - Klaim realm currency\n`;
                            else if (emergencyTeapot)
                                balasan += `│ - Klaim realm currency sebelum penuh\n`
                            if (!dailyCommision)
                                balasan += `│ - Selesaikan misi harian\n`
                            if (dailyCommision && !dailyKlaim)
                                balasan += `│ - Klaim reward misi harian\n`
                            if (!weeklyBossDone)
                                balasan += `│ - Gunakan diskon weekly boss\n`
                            if (ekspedisiDone > 0)
                                balasan += `│ - Utus pasukan ekspedisi\n`
                            if (!parameticUsed)
                                balasan += `│ - Gunakan parametic transformer\n`
                        }
                    }
                    if (showTodoList) {
                        try {
                            balasan += `│──────────────────\n`
                            if (fullResin)
                                balasan += `│ ⚠️ *Resin kamu sudah penuh*\n`
                            if(!fullResin)
                                balasan += `│ 🕗 Resin akan penuh dalam: *${secondsToHms(hyb.remaining_resin_recovery_time)}*\n`
                            if (!parameticUsed)
                                balasan += `│ ⚠️ *Parametic transformer sudah dapat digunakan*\n`
                            if(parameticUsed)
                                balasan += `│ 🕒 Parametic transformer cooldown: *${secondsToHms(hyb.remaining_transformer_recovery_time)}*\n`
                        } catch (e) {
                            console.log(e)
                        }
                    }
                    if (needSubmitCookie) {
                        balasan += `│──────────────────\n`
                        balasan += `│ *Info*: _Untuk mensubmit cookie silahkan akses *!submit_cookie* secara pribadi ke Paimon_\n`
                    }
                    showMTServer = false
                    if (showMTServer && !needSubmitCookie) {
                        balasan += `│──────────────────\n`
                        balasan += `│ *Catatan*: _Mungkin beberapa data (terkait misi harian) tidak akurat._\n`
                        // balasan += `│ *Catatan*: _informasi misi harian tidak lagi ditampilkan karena data yang diretrive sering tidak valid._\n`
                    }
                    balasan += `╰──────────────────`
                    return await m.reply(balasan)
                    break
                }
            }
        } catch(e) {
            console.log(e)
            return m.reply(`Server sedang gangguan, coba lagi nanti!`)
        }
    }
}