const fs = require("fs")

const { arrayRemove } = require("../../lib/Function")
const { updateFireData } = require("./../../controller/firebase/fireController")
const id_collection = 'gif_guilds'

module.exports = {
    name: "GIF Management System",
    alias: ["gif_system","gif_sistem","gif_guild","gif_guilds"],
    desc: "Tetapkan group sebagai GIF Official GUILD",
    type: "hide",
    example: `!gif_system [on/off]`,
    isOwner: true,
    isGroup: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { text, isGIFGuild }) => {
    	let toggle = text.toLowerCase()
    	try {
    		switch(toggle) {
    			// Aktifkan gif system
	        	case "on":
                    if(isGIFGuild)
                        return m.reply('Paimon sudah diaktifkan di group ini ^_^')
	        		global.gif_guilds.push(m.from)
                    writeDB(global.gif_guilds)
	        		return m.reply('Yey, Group ini berhasil ditetapkan sebagai GIF Official Guild!')
	        		break;
	        	// Nonaktif gif system
	        	case "off":
                    if(!isGIFGuild)
                        return m.reply('Paimon belum diaktifkan di group ini ^_^')
	        		global.gif_guilds = arrayRemove(global.gif_guilds, m.from)
                    writeDB(global.gif_guilds)
	        		return m.reply('Ehe, Group ini telah ditetapkan bukan sebagai GIF Official Guild!')
	        		break;
	        	default:
	        		return m.reply('Parameter hanya menerima [on/off]!')
	        		break;
	        }
    	} catch(e) {
    		// console.log(e)
    		return m.reply("Terjadi kesalahan: "+e)
    	}
    }
}

const writeDB = async (data_baru) => {
    updateFireData(id_collection, data_baru)
}