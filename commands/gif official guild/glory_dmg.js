const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Glory - Klee Friends",
    alias: ["glory_klee","glory_klee_friends","glory_dmg"],
    desc: "Lihat top DMG player di seluruh season",
    type: "gif",
    example: `!glory_dmg`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("check", m)
        try {
            let imgRender = await fetchBuffer(global.api("gif_canvas", `/glory/klee_friends`, { query: text }, "apikey"))
            let resPesanTwo = {
                image: imgRender,
                caption: `*Glory Klee Friends*\nTop damager desetiap karakter Genshin Impact seluruh season.\n*_Catatan: karakter yang tidak muncul mencerminkan belum adanya traveler yang melakukan showcase menggunakan karakter terkait selama durasi GIF - Abyss Leaderboard berlangsung_`
            }
            return await paimon.sendMessage(m.from, resPesanTwo, { quoted: m })
        } catch (e) {
            console.log(e)
            return m.reply("Waduh!! papan leaderboardnya belum ditulis ulang...")
        }
    }
}