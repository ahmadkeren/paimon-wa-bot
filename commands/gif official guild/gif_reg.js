const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { getGuildID, cariTraveler, gifDBEngine } = require("../../lib/gif")

module.exports = {
    name: "Registrasi ke GIF Guild",
    alias: ["gif_reg","reg_gif","reg"],
    desc: "Daftarkan diri ke GIF Official Guild",
    type: "hide",
    example: `!gif_reg <uid> (tanpa tanda <>)`,
    isGifMenu: true,
    isGroup: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { text, metadata }) => {
        let uid = parseInt(text)
        if(uid.toString() != text)
            return m.reply(`Silahkan daftar sesuai dengan format!\nContoh: !gif_reg 805093361`)
        let guildID = await getGuildID(metadata.subject)
        // Check UID
        global.mess("check", m)
        let data_traveler_baru = await cari_data_traveler(uid)
            if(data_traveler_baru.uid == null)
                return m.reply(`UID yang kamu berikan sepertinya tidak valid! silahkan periksa kembali!\nNote: jika dirasa UID kamu valid tapi error ini muncul, silahkan hubungi Actinium`)
        // Cari traveler
        let cari = await cariTraveler(m.sender)
        if(cari == null || cari.length == 0) {
            // Traveler belum kerekap di database | ACT: push data ke sistem
            // Templating data
            let ponsel = m.sender.split('@')[0]
            let nomorHP = "'+" + ponsel.slice(0, 2) + " " + ponsel.slice(2, 5) + "-" + ponsel.slice(5, 9) + "-" + ponsel.slice(9)
            let data_baru = {
                "data": [{
                    "Nama": "Traveler Baru",
                    "Asal": "-",
                    "Nomor Hp": nomorHP,
                    "IGN": data_traveler_baru.ign,
                    "UID": uid,
                    "Status": "Menunggu Validasi",
                    "Signature": data_traveler_baru.signature,
                    "Teguran": "0",
                    "WhatsApp_ID": m.sender
                }]
            }
            let put_data = await gifDBEngine("post", guildID.name, false, data_baru)
            let jawaban = `Yey! data kamu berhasil Paimon rekap!\nKartu anggotanya nanti Paimon buatin ya..\nKalau ada kesalahan data, jangan lupa melapor traveler 😁`
            return await kirim_laporan(paimon, m, text, jawaban)
        } else {
            // Initial Templating
            let user_found = {
                uid: cari[0]["UID"],
                nama: cari[0]["Nama"],
                ign: cari[0]["IGN"],
                signature: cari[0]["Signature"],
                guild: cari[0]["guild"],
            }
            // Handling
            if(user_found.uid == "" || user_found.uid == undefined || user_found.uid == " ") {
                // put templating
                let data_update = {
                    "data": [{
                        "IGN": data_traveler_baru.ign,
                        "UID": uid,
                        "Asal": "-",
                        "Status": "Menunggu Validasi",
                        "Signature": data_traveler_baru.signature,
                    }]
                };
                try {
                    let anu_update = await gifDBEngine("put", user_found.guild, m.sender, data_update)
                    let jawaban_update = `Yey! UID kamu berhasil didaftarkan!\nKartu anggotanya nanti Paimon update ya..\nKalau ada kesalahan data, jangan lupa melapor traveler 😁`
                    return await kirim_laporan(paimon, m, text, jawaban_update)
                } catch(e) {
                    console.log(e)
                    return m.reply(`Terjadi kesalahan teknis! Coba lagi nanti atau hubungi Actinium`)
                }
            } else
                return m.reply(`Kamu sudah terdaftar sebagai member GIF - Official Guild ^_^\nUntuk mengupdate/mengganti UID silahkan akses menu *!update_uid*`)
        }
    }
}

const cari_data_traveler = async function(uid) {
    let res = {
        uid: null,
        ign: null,
        signature: null,
    }
    const game_data = await fetchUrl(global.api("enka_genshin", `/u/${uid}/__data.json`, {}, ""))
    try {
        if(game_data) {
            res.ign = game_data.playerInfo.nickname
            res.signature = game_data.playerInfo.signature
            res.uid = uid
        }
    } catch(e) {
        console.log(e)
    }
    return res
}

const kirim_laporan = async function(paimon, m, text, pesanKeUser) {
    let jawaban = `${pesanKeUser}`
    await paimon.sendMessage(m.from, {text: jawaban}, { quoted: m }) // kirim ke member baru
    let chat_ke_owner = `*[MEMBER BARU]*\n*Nomor*: @${m.sender.split("@s.whatsapp.net")}\n*Biodata*:\n${text}`
    await paimon.sendMessage('6282282170805@s.whatsapp.net', {text: chat_ke_owner}, { quoted: m }) // kirim ke actinium
    return
}