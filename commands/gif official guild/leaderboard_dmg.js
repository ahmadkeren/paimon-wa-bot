const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Leaderboard - Klee Friends",
    alias: ["klee_friends","klee","kf","top_damage","top_dmg"],
    desc: "Lihat leaderboard Klee best friends",
    type: "gif",
    example: `!klee_friends`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("check", m)
        try {
            let imgRender = await fetchBuffer(global.api("gif_canvas", `/klee_friends`, { query: text }, "apikey"))
            let resPesanTwo = {
                image: imgRender,
                caption: `*Klee Best Friends*\nData yang ditampilkan adalah data top player dari tiap karakter temannya Klee ^_^\nLink partisipasi: https://gif-official.web.app`
            }
            return await paimon.sendMessage(m.from, resPesanTwo, { quoted: m })
        } catch (e) {
            console.log(e)
            return m.reply("Waduh!! papan leaderboardnya lagi dibawa main sama Klee...")
        }
    }
}