const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Glory - Abyss Leaderboard",
    alias: ["glory_leaderboard","glory","glory_abyss"],
    desc: "Lihat top player spyral abyss diseluruh season",
    type: "gif",
    example: `!glory`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("check", m)
        try {
            let imgRender = await fetchBuffer(global.api("gif_canvas", `/glory/leaderboard`, { query: text }, "apikey"))
            let resPesanTwo = {
                image: imgRender,
                caption: `*Glory Abyss Leaderboard*\nTop-10 Player Spiral Abyss dalam GIF - Spiral Abyss seluruh Season`
            }
            return await paimon.sendMessage(m.from, resPesanTwo, { quoted: m })
        } catch (e) {
            console.log(e)
            return m.reply("Waduh!! papan leaderboardnya belum ditulis ulang...")
        }
    }
}