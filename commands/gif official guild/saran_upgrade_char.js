const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { cariTraveler, getTravelerCookies } = require("../../lib/gif")
const { formPerkenalkanDiri } = require("../../lib/template")

module.exports = {
    name: "Saran Upgrade",
    alias: ["saran_upgrade"],
    desc: "Lihat daftar karakter yang disarankan untuk diupgrade",
    type: "gif",
    example: `!saran_upgrade`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("check", m)
        // Initial Parameter
        let selfCheck = true
        let target_pencarian = m.sender
        if(m.mentions.length > 0)
            return m.reply(`Gomen... Fitur ini hanya dapat digunakan untuk pengecekan diri sendiri`)
        try {
            // Cari anggota
            let cari = await cariTraveler(target_pencarian)
            if (cari == null || !cari)
                return m.reply(`Gomen... UIDnya tidak ditemukan traveler!`)
            // Cetak data
            for (let lmt of cari) {
                // belum registrasi
                if (lmt["WhatsApp_ID"] == target_pencarian && lmt["Status"] == "Blm. Registrasi") {
                    if (selfCheck) {
                        teks = `Oops! kamu belum melakukan registrasi!\n`
                        teks += formPerkenalkanDiri()
                    } else
                        teks = `Oops! Member yang bersangkutan belum melakukan registrasi!`
                    return m.reply(teks)
                } else
                if (lmt["WhatsApp_ID"] == target_pencarian) {
                    // Cek Cookie
                    dataCookie = await getTravelerCookies(lmt["UID"]);
                    if(dataCookie==null)
                        return m.reply(`Data daftar karakter tidak dapat ditampilkan karena *${lmt["Nama"]}* belum pernah *mensubmit cookie*!`)
                    // Tampilkan disini
                    let imgRender = await fetchBuffer(global.api("gif_canvas", `/rekomendasi_karakter`, { ign: lmt["IGN"], ltuid: dataCookie.ltuid, ltoken: dataCookie.ltoken, uid: lmt["UID"]}, "apikey"))
                    let resPesan = {
                        image: imgRender,
                        caption: `Rekomendasi upgrade karakter untuk ${lmt["IGN"]}`
                    }
                    return await paimon.sendMessage(m.from, resPesan, { quoted: m })
                    break
                    // End tampilkan disini
                }
            }
        } catch(e) {
            console.log(e)
            return m.reply(`Server sedang gangguan, coba lagi nanti!`)
        }
    }
}