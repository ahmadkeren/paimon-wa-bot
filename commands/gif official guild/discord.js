const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Layanan Discord",
    alias: ["discord"],
    desc: "Lihat link discord GIF untuk keperluan live streaming",
    type: "gif",
    example: `!discord`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        jawaban  = `==================\n`
        jawaban += `== *DISCORD GIF* ==\n`
        jawaban += `==================\n`
        jawaban += `*Nama Sever*: GIF Tralalala\n`
        jawaban += `*Link Invitation*: https://discord.gg/VQsjCeQyYz\n`
        jawaban += `*Deskripsi*: Lets play and have fun together! ^_^\n`
        jawaban += `==================\n`
        jawaban += `*) Catatan:\n`
        jawaban += `- Admin Discord: Ali Alhadi\n`
        jawaban += `- Kontak Admin: https://wa.me/6282110140538\n`
        jawaban += `- Platform discord ini dikhususkan untuk kebutuhan voice line dan live streaming (bukan untuk menggantikan WhatsApp)\n`
        jawaban += `- Jika ada pertanyaan silahkan kontak boss melalui perintah *!owner*\n`
        jawaban += `==================`
        return m.reply(jawaban)
    }
}