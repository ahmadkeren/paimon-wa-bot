const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Badge Member GIF",
    alias: ["badge","medal","badges","medals"],
    desc: "Lihat daftar badge yang resmi dikeluarkan dan diperebutkan member GIF",
    type: "gif",
    example: `!badge`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        jawaban  = `== *DAFTAR BADGE* ==\n`
        jawaban += `Berikut daftar badge yang resmi telah dikeluarkan dan diperbutkan:\n`
        jawaban += `==================\n`
        jawaban += `│ - Title: *Knight of Favonius*\n`
        jawaban += `│ - Badge: ⚜️\n`
        jawaban += `│ - Obtain: Capai TOP#1 Leaderboard S33 dengan mengikuti rule banlist\n`
        jawaban += `==================\n`
        jawaban += `│ - Title: *Maid of Favonius*\n`
        jawaban += `│ - Badge: 💝\n`
        jawaban += `│ - Obtain: Capai TOP#2 Leaderboard S33 dengan mengikuti rule banlist\n`
        jawaban += `==================\n`
        jawaban += `│ - Title: *Feiyun Commerce*\n`
        jawaban += `│ - Badge: ⚛️\n`
        jawaban += `│ - Obtain: Capai TOP#1 Leaderboard S34 dengan mengikuti rule banlist\n`
        jawaban += `==================\n`
        jawaban += `│ - Title: *Tenryou Commerce*\n`
        jawaban += `│ - Badge: 🇯🇵\n`
        jawaban += `│ - Obtain: Capai TOP#1 Leaderboard S37 dengan mengikuti rule banlist\n`
        jawaban += `==================\n`
        jawaban += `│ - Title: *Watatsumi Army*\n`
        jawaban += `│ - Badge: 🌀\n`
        jawaban += `│ - Obtain: Capai TOP#1 Leaderboard S38 dengan mengikuti rule banlist\n`
        jawaban += `==================\n`
        jawaban += `│ - Title: *Kamisato Clan*\n`
        jawaban += `│ - Badge: ❄️\n`
        jawaban += `│ - Obtain: Special exchange badge: peleburan badge "KOF" dan "WA"\n`
        jawaban += `==================\n`
        jawaban += `│ - Title: *Yin/Yang Adventurer*\n`
        jawaban += `│ - Badge: ☯️\n`
        jawaban += `│ - Obtain: GIF - Event: Can You Solve This Dungeon?\n`
        return m.reply(jawaban)
    }
}