const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Leaderboard - Spiral Abyss",
    alias: ["gif_leaderboard","leaderboard"],
    desc: "Lihat leaderboard spyral abyss",
    type: "gif",
    example: `!leaderboard`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("check", m)
        try {
            let imgRender = await fetchBuffer(global.api("gif_canvas", `/leaderboard`, { query: text }, "apikey"))
            let resPesanTwo = {
                image: imgRender,
                caption: `*GIF - Spiral Abyss Leaderboard Terbaru*\nUntuk melihat keseluruhan leaderboard silahkan akses laman https://gif-official.web.app ya traveler... ^_^`
            }
            return await paimon.sendMessage(m.from, resPesanTwo, { quoted: m })
        } catch (e) {
            console.log(e)
            return m.reply("Waduh!! papan leaderboardnya belum ditulis ulang...")
        }
    }
}