const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { cariTraveler, getTravelerCookies } = require("../../lib/gif")
const { formPerkenalkanDiri, travelerIdentity } = require("../../lib/template")

module.exports = {
    name: "Friendship Karakter Saya",
    alias: ["friendship","companion"],
    desc: "Lihat daftar level persahabatan karakter genshin yang belum maksimal",
    type: "gif",
    example: `!friendship`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper, isOwner }) => {
        global.mess("check", m)
        // Initial Parameter
        let selfCheck = true
        let target_pencarian = m.sender
        if(m.mentions.length > 0)
            return m.reply(`Gomen... Fitur ini hanya dapat digunakan untuk pengecekan diri sendiri`)
        try {
            // Cari anggota
            let cari = await cariTraveler(target_pencarian)
            if (cari == null || !cari)
                return m.reply(`Gomen... UIDnya tidak ditemukan traveler!`)
            // Cetak data
            for (let lmt of cari) {
                // belum registrasi
                if (lmt["WhatsApp_ID"] == target_pencarian && lmt["Status"] == "Blm. Registrasi") {
                    if (selfCheck) {
                        teks = `Oops! kamu belum melakukan registrasi!\n`
                        teks += formPerkenalkanDiri()
                    } else
                        teks = `Oops! Member yang bersangkutan belum melakukan registrasi!`
                    return m.reply(teks)
                } else
                if (lmt["WhatsApp_ID"] == target_pencarian) {
                    let balasan = `╭─「 *GENSHIN COMPANION* 」\n`
                    balasan = await travelerIdentity(balasan, lmt)
                    try {
                        needSubmitCookie = false;
                        dataCookie = await getTravelerCookies(lmt["UID"]);
                        if(lmt["UID"].toString().includes("805093361")) {
                            balasan += `│──────────────────\n`
                            balasan += `│ *Oops!* Data milik Actinium tidak dapat ditampilkan :")\n`
                        } else if (dataCookie==null) {
                            balasan += `│──────────────────\n`
                            balasan += `│ Data friendship tidak dapat ditampilkan karena *${lmt["Nama"]}* belum pernah *mensubmit cookie*!\n`
                            needSubmitCookie = true;
                        } else {
                            // Proses ambil data catatan harian
                            let resin = await fetchUrl(global.api("gif_api_stats", "/karakterv2", { uid: lmt["UID"], ltuid: dataCookie.ltuid, ltoken: dataCookie.ltoken }, "apikey"))
                            hyb = resin
                            //console.log(resin);
                            if (!hyb.error) {
                                // Patch api lama
                                tmp = hyb
                                hyb = {
                                    "characters": []
                                }
                                hyb.characters = tmp
                                // Patch api lama
                                total_chars = hyb.characters.length
                                balasan += `│──────────────────\n`
                                balasan += `│ *• Char Dimiliki:* ${total_chars} karakter\n`
                                try {
                                    total_level = 0
                                    total_best_companion = 0
                                    for (let char of hyb.characters) {
                                        total_level += char.level
                                        if (char.friendship >= 10)
                                            total_best_companion += 1
                                    }
                                    total_best_companion = total_best_companion + 1 // ditambah satu karena ada traveler yang levelnya 0
                                    avg_level = total_level / hyb.characters.length
                                    balasan += `│ *• Max Friendship:* ${total_best_companion} karakter\n`
                                    balasan += `│ *• Progression:* ${Math.round(total_best_companion/total_chars*100).toFixed(2)}%\n`
                                    balasan += `│ *• Rerata Char:* Level ${parseInt(avg_level)}\n`
                                } catch (e) {}
                                // Tampilkan Daftar Friendship Belum Max
                                try {
                                    balasan += `│──────────────────\n`
                                    div_friendship = [{
                                            "lvl": 0,
                                            chars: []
                                        },
                                        {
                                            "lvl": 1,
                                            chars: []
                                        },
                                        {
                                            "lvl": 2,
                                            chars: []
                                        },
                                        {
                                            "lvl": 3,
                                            chars: []
                                        },
                                        {
                                            "lvl": 4,
                                            chars: []
                                        },
                                        {
                                            "lvl": 5,
                                            chars: []
                                        },
                                        {
                                            "lvl": 6,
                                            chars: []
                                        },
                                        {
                                            "lvl": 7,
                                            chars: []
                                        },
                                        {
                                            "lvl": 8,
                                            chars: []
                                        },
                                        {
                                            "lvl": 9,
                                            chars: []
                                        },
                                        {
                                            "lvl": 10,
                                            chars: []
                                        }
                                    ];
                                    for (let char of hyb.characters) {
                                        div_friendship[char.friendship].chars.push(char.name);
                                    }
                                    for (let chars of div_friendship.reverse()) {
                                        if (chars.chars.length > 0) {
                                            if (chars.lvl < 10) {
                                                if (chars.chars.length > 6) {
                                                    char_list = "";
                                                    count = 0;
                                                    for (let tmp_char of chars.chars) {
                                                        count += 1;
                                                        if (count > 5) {
                                                            char_list += ",dll";
                                                            break;
                                                        } else
                                                        if (count == 1)
                                                            char_list += tmp_char;
                                                        else
                                                            char_list += "," + tmp_char;
                                                    }
                                                } else
                                                    char_list = chars.chars.toString();
                                                if(chars.lvl > 0)
                                                    balasan += `│ *• Level ${chars.lvl}:* ${char_list.split(",").join(", ")}\n`
                                            }
                                        }
                                    }
                                    //console.log(div_friendship);
                                } catch (e) {
                                    console.log(e);
                                }
                            } else {
                                balasan += `│──────────────────\n`
                                if (hyb.error.includes("not public"))
                                    balasan += `│ *Oops!* informasi pengembara tidak dipublikasikan!\n`
                                else
                                    balasan += `│ *Oops!* server mihoyo sedang gangguan!\n`
                            }
                        }
                    } catch (e) {
                        console.log(e)
                    }
                    balasan += `│──────────────────\n`
                    balasan += `│ *Be a Friendly Traveler :)*\n`
                    try {
                        if (needSubmitCookie && checkSelf) {
                            balasan += `│──────────────────\n`
                            balasan += `│ *Info*: _Untuk mensubmit cookie silahkan akses *!submit_cookie* secara pribadi ke Paimon_\n`
                        }
                    } catch (e) {}
                    balasan += `╰──────────────────`
                    // Kirim beserta kartu id/tidak
                    //return reply(balasan); // force kirim tanpa kartu ID (buat debug lebih mudah)
                    cetakFoto = false
                    if (!cetakFoto) {
                        return m.reply(balasan)
                    } else {
                        try {
                            pok = await fetchBuffer(global.api(`${lmt["CardID_URL"]}`, ``, {}, ""))
                            resPesan = {
                                image: pok,
                                caption: `${balasan}`
                            }
                            return await paimon.sendMessage(m.from, resPesan, { quoted: m })
                        } catch (e) {
                            console.log(e)
                            return m.reply(balasan)
                        }
                    }
                break
                }
            }
        } catch(e) {
            console.log(e)
            return m.reply(`Server sedang gangguan, coba lagi nanti!`)
        }
        return m.reply('Are... boom boom bakudan?!')
    }
}