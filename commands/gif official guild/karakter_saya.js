const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { cariTraveler, getTravelerCookies } = require("../../lib/gif")
const { formPerkenalkanDiri } = require("../../lib/template")
const genshindb = require("genshin-db")

module.exports = {
    name: "Karakter Saya",
    alias: ["my_char","my_chars","karakterku","daftar_char","daftar_chars"],
    desc: "Lihat karakter genshin yang saya miliki",
    type: "gif",
    example: `!my_char / !my_char diona`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper, isOwner}) => {
        global.mess("check", m)
        // Initial Parameter
        let selfCheck = true
        let target_pencarian = m.sender
        if(m.mentions.length > 0)
            return m.reply(`Gomen... Fitur ini hanya dapat digunakan untuk pengecekan diri sendiri`)
        try {
            let showOneChar = false
            let charInfo = {
                name: "-"
            }
            if(args[0]) {
                charInfo = await genshindb.characters(text);
                if(!charInfo)
                    return m.reply(`Gomen... Karakter dengan katak kunci *${text}* tidak ditemukan!`)
                showOneChar = true
            }
            // Cari anggota
            let cari = await cariTraveler(target_pencarian)
            if (cari == null || !cari)
                return m.reply(`Gomen... UIDnya tidak ditemukan traveler!`)
            // Cetak data
            for (let lmt of cari) {
                // belum registrasi
                if (lmt["WhatsApp_ID"] == target_pencarian && lmt["Status"] == "Blm. Registrasi") {
                    if (selfCheck) {
                        teks = `Oops! kamu belum melakukan registrasi!\n`
                        teks += formPerkenalkanDiri()
                    } else
                        teks = `Oops! Member yang bersangkutan belum melakukan registrasi!`
                    return m.reply(teks)
                } else
                if (lmt["WhatsApp_ID"] == target_pencarian) {
                    // Cek Cookie
                    dataCookie = await getTravelerCookies(lmt["UID"]);
                    if(dataCookie==null)
                        return m.reply(`Data karakter tidak dapat ditampilkan karena *${lmt["Nama"]}* belum pernah *mensubmit cookie*!`)
                    if(showOneChar) {
                        // Tampilkan disini | detail karakter query
                        let imgRenderSingle = await fetchBuffer(global.api("gif_canvas", `/char_show_single`, { ign: lmt["IGN"], ltuid: dataCookie.ltuid, ltoken: dataCookie.ltoken, uid: lmt["UID"], name: charInfo.name}, "apikey"))
                        // console.log(global.api("gif_canvas", `/char_show_single`, { ign: lmt["IGN"], ltuid: dataCookie.ltuid, ltoken: dataCookie.ltoken, uid: lmt["UID"], name: charInfo.name}, "apikey"))
                        let resPesanSingle = {
                            image: imgRenderSingle,
                            caption: `Rincian karakter *${charInfo.name}* milik *${lmt["IGN"]}*`
                        }
                        return await paimon.sendMessage(m.from, resPesanSingle)
                    } else {
                        // Tampilkan disini | daftar koleksi karakter
                        let imgRender = await fetchBuffer(global.api("gif_canvas", `/char_show`, { ign: lmt["IGN"], ltuid: dataCookie.ltuid, ltoken: dataCookie.ltoken, uid: lmt["UID"]}, "apikey"))
                        let resPesan = {
                            image: imgRender,
                            caption: `Daftar koleksi karakter *${lmt["IGN"]}*`
                        }
                        return await paimon.sendMessage(m.from, resPesan)
                    }
                    break
                    // End tampilkan disini
                }
            }
        } catch(e) {
            console.log(e)
            return m.reply(`Server sedang gangguan, coba lagi nanti!`)
        }
    }
}