const { delay, extractMessageContent } = require("@adiwajshing/baileys")
const { isUrl, fetchUrl } = require("../../lib/Function")
const sticker = require('stickerwa-search')

module.exports = {
    name: "Sticker (Crop)",
    alias: ["sv2","stiker_v2","setiker_v2","sticker_v2","s2"],
    desc: "Buat sticker dari gambar yang ada",
    type: "tools",
    exec: async(paimon, m, { command, prefix, text, quoted, mime }) => {
        if (!quoted) return  m.reply(`Silahkan reply gambar dengan perintah !sticker_v2 traveler ^_^`)
        if (/image|video|sticker/.test(mime)) {
            let download = await quoted.download()
            paimon.sendFile(m.from, download, "", m, { asSticker: true, author: global.author, packname: global.packname, categories: ['😄','😊'] })
        } else if (quoted.mentions[0]) {
            let url = await paimon.profilePictureUrl(quoted.mentions[0], "image")
            paimon.sendFile(m.from, url, "", m, { asSticker: true, author: global.author, packname: global.packname, categories: ['😄','😊'] })
        } else if (isUrl(text)) {
            if (isUrl(text)) paimon.sendFile(m.from, isUrl(text)[0], "", m, { asSticker: true, author: global.author, packname: global.packname, categories: ['😄','😊'] })
            else m.reply('No Url Match')
        } else if (text) {
            global.mess("search", m)
            let fetch = await sticker.stickerSearch(text)
            let print = 0
            for (let url of fetch.sticker) {
                if(url == undefined) continue;
                if(print >= 3) break;
                await delay(1000)
                paimon.sendFile(m.from, url, "", m, { asSticker: true, author: global.author, packname: global.packname, categories: ['😄','😊'] })
                print+=1
            }
            if(print==0)
                m.reply(`Oops! Paimon tidak menemukan sticker dengan kata kunci *${text}*`)
        } else if (quoted.type == "templateMessage") {
            let message = quoted.imageMessage || quoted.videoMessage
            let download = await paimon.downloadMediaMessage(message)
            paimon.sendFile(m.from, download, "", m, { asSticker: true, author: global.author, packname: global.packname, categories: ['😄','😊'] })
        } else if (quoted.type == "buttonsMessage") {
            let message = quoted.imageMessage || quoted.videoMessage
            let download = await paimon.downloadMediaMessage(message)
            paimon.sendFile(m.from, download, "", m, { asSticker: true, author: global.author, packname: global.packname, categories: ['😄','😊'] })
        } else {
            return m.reply(`Silahkan reply gambar dengan perintah !sticker_v2 traveler ^_^`, m.from, { quoted: m })
        }
    }
}
