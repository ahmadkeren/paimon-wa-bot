const fs = require("fs")
const { recognize } = require('../../lib/ocr')

module.exports = {
    name: "Image to Text (OCR)",
    alias:["ocr","baca"],
    desc: "Ambil text dari suatu gambar",
    type: "hide",
    exec: async(paimon, m, { mime, quoted }) => {
        return m.reply(`Ehe! fitur ini sedang maintenance :")`)
        if (!quoted) return  m.reply(`Silahkan reply gambar dengan perintah !ocr traveler ^_^`)
        let media = null
        if (/image|sticker/.test(mime))
            media = await paimon.downloadAndSaveMediaMessage(quoted)
        else
            return m.reply(`Silahkan reply gambar dengan perintah !ocr ^_^`)
        // Do OCR
        if(media==null)
            return m.reply(`Ehe! gambarnya gak bisa paimon download buat dilihat!`)
        await recognize(media, {
            lang: 'eng+ind',
            oem: 1,
            psm: 3
        })
        .then(teks => {
            m.reply(teks.trim())
            fs.unlinkSync(media)
        })
        .catch(err => {
            console.log(err.message)
            m.reply("Ehe! paimon gagal membaca gambar tersebut T.T")
            fs.unlinkSync(media)
        })
        return
    }
}