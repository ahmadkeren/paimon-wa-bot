let { exec: execS } = require("child_process")
const fs = require("fs")
const path = require("path")
const { webp2mp4File } = require("../../lib/Converter")
const { getRandom } = require("../../lib/Function")
const dir = path.join(process.cwd(), `./tmp/undefined.jpg`)

module.exports = {
    name: "Sticker to Media",
    alias:["toimg","toimage"],
    desc: "Konversi sticker tidak bergerak menjadi gambar",
    type: "tools",
    exec: async(paimon, m, { mime, quoted }) => {
        if (!quoted) return  m.reply(`Silahkan reply sticker dengan perintah !toimg traveler ^_^`)
        try {
            console.log(quoted)
            if (!/webp/.test(mime)) return m.reply(`Hanya Support mime webp`)
            if(quoted.msg.firstFrameLength || quoted.msg.isAnimated) return m.reply("Sticker bergerak belum didukung! T.T")

            if (!quoted.msg.isAnimated) {
                global.mess("wait", m)
                let media = await paimon.downloadAndSaveMediaMessage(quoted)
                let ran = await getRandom('.png')
                execS(`ffmpeg -i ${media} ${ran}`, (err) => {
                    fs.unlinkSync(media)
                    if (err) throw err
                    let buffer = fs.readFileSync(ran)
                    paimon.sendFile(m.from, buffer, "Sticker to Image", m)
                    fs.unlinkSync(ran)
                })
            }
        } catch(e) {
            console.log(e)
            return m.reply("Gomen.. konversi gagal dilakukan!")
        }
    }
}