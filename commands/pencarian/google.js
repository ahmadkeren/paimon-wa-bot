const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const google = require('google-it')

module.exports = {
    name: "Google",
    alias: ["google"],
    desc: "Lakukan pencarian di google",
    type: "pencarian",
    example: `!google cara membuat kue kering`,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
    	global.mess("search", m)
        let googleQuery = text
        if (googleQuery == undefined || googleQuery == ' ') return reply(`*Hasil Pencarian : ${googleQuery}* tidak ditemukan`)
        google({
            'query': googleQuery
        }).then(results => {
            let vars = `_*HASIL PENCARIAN GOOGLE : ${googleQuery}*_\n\n`
            for (let i = 0; i < results.length; i++) {
                vars += `═════════════════\n\n`
                vars += `*Judul* : ${results[i].title}\n\n`
                vars += `*Deskripsi* : ${results[i].snippet}\n\n`
                vars += `*Link* : ${results[i].link}\n\n`
            }
            return m.reply(vars)
        }).catch(e => {
            console.log(e)
            return m.reply('Pencarian Google gagal dilakukan! T.T')
        })
    }
}