const { fetchUrl, fetchBuffer, pickArray } = require("../../lib/Function")

module.exports = {
    name: "Pencarian Anime",
    alias: ["anime"],
    desc: "Lakukan pencarian anime",
    type: "pencarian",
    example: `!anime naruto`,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("search", m)
        try {
        	let search = await fetchUrl(global.api("arugaz", `/kuso`, {q: text}, ""))
            if(!search.status)
                return m.reply(`Anime dengan judul *${text}* tidak dapat ditemukan!`)
            // Template pesan
            let jawaban = ``;
            jawaban += `╭─「 *PENCARIAN ANIME* 」\n`
            jawaban += `│ *• Judul:* ${search.title}\n`
            jawaban += `│ *• Informasi:* ${search.info}\n`
            jawaban += `│ *• Sinopsis:* ${search.sinopsis}\n`
            jawaban += `│ *• Link Download:* ${search.link_dl}\n`
            jawaban += `╰─────────────────────`
            // End of template pesan
            let imgThumb = await fetchBuffer(`${search.thumb}`)
            let resPesan = {
                image: imgThumb,
                caption: `${jawaban}`
            }
            await paimon.sendMessage(m.from, resPesan, { quoted: m })
        } catch (e) {
            console.log(e)
            return m.reply("Waduh!! sepertinya server anime Paimon lagi gangguan...")
        }
    }
}