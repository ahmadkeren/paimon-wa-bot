const { fetchUrl, fetchBuffer, pickArray, fetchText, numberWithCommas } = require("../../lib/Function")
const axios = require("axios");
const cheerio = require("cheerio");
const pretty = require("pretty");
const fs = require('fs');

const link_api = "https://www.priceza.co.id/s/harga"

module.exports = {
    name: "Pencarian Barang",
    alias: ["harga"],
    desc: "Cari harga barang di berbagai e-commerce",
    type: "pencarian",
    example: `!harga kopi aaa`,
    // isOwner: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("search", m)
        try {
            let items = await cekHargaBarang(text)
            let max_item = 3
            let cnt = 0
            for(let item of items) {
                // console.log(item)
                // Handler
                if(cnt >= max_item) break

                // Cacah data
                let infoBarang = ''
                infoBarang  = `╭─「 *PENCARIAN BARANG* 」\n`
                infoBarang += `│ *• Nama:* ${item.name}\n`
                infoBarang += `│ *• Harga:* ${item.price.region} ${numberWithCommas(item.price.value)},-\n`
                infoBarang += `│ *• Toko:* ${item.source}\n`
                infoBarang += `│ *• Deksripsi:* ${item.desc}\n`
                infoBarang += `│──────────────────\n`
                infoBarang += `│ _Catatan: Akses situs resmi toko untuk melihat detail, hati-hati terhadap penipuan!_\n`
                infoBarang += `╰─────────────────────`
                try {
                    let thumbIMG = await fetchBuffer(`https://${item.image}`)
                    let resPesan = {
                        image: thumbIMG,
                        caption: `${infoBarang}`,
                    }
                    await paimon.sendMessage(m.from, resPesan, { quoted: m })
                } catch(e) {
                    console.log(e)
                    await m.reply(infoBarang)
                }
                // Count
                cnt+=1
            }
        } catch (e) {
            console.log(e)
            return m.reply("Waduh!! Paimon gagal mencari barang, coba lagi nanti ya...")
        }
    }
}

const cekHargaBarang = async(query="samsung galaxy a5") => {
    const markup = await fetchText(link_api+"/"+query)
    const html = cheerio.load(markup)
    const items_tmp = html('div.pz-pdb-card')
    let items = []
    items_tmp.each((idx, el) => {
        const item = html(el).children("div.pz-pdb-item-info")
        let item_source = html(el).children("div.pz-pdb-item-footer").children(".pz-pdb-store").children("img").attr("alt")
        let item_name = item.children("div.pz-pdb_detail").children("div.pz-pdb_detail-a").children("a").children("h3").text()
        let item_desc = item.children("div.pz-pdb_detail").children("div.pz-pdb_detail-a").children("div.pz-pdb-desc").text()
        let item_image = item.children("div.pz-pdb_media").children("a").children("img").attr("src")
        let item_prices = item.children("div.pz-pdb_detail").children("div.pz-pdb_detail-b").children(".pz-pdb-price").children("span")
        let item_price = {
            region: null,
            value: null,
        }
        for(let price of item_prices) {
            let attr = price.attribs
            if(attr.itemprop=="priceCurrency")
                item_price.region = attr.content
            if(attr.itemprop=="price")
                item_price.value = attr.content
            
        }
        // Beauty text
        item_name = item_name.split("\n").join("")
        item_name = item_name.split("\t").join(" ")
        item_name = item_name.replace(/ +(?= )/g,'');

        item_desc = item_desc.split("\n").join("")
        item_desc = item_desc.split("\t").join(" ")
        item_desc = item_desc.replace(/ +(?= )/g,'');

        if(item_image!=undefined)
            item_image = item_image.replace("//","")

        //Trim
        item_name = item_name.trim()
        item_desc = item_desc.trim()

        // Undefined handler
        if(item_source==undefined)
            item_source = "Tevyat"
        
        // Return data
        items.push({
            name: item_name,
            desc: item_desc,
            price: item_price,
            image: item_image,
            source: item_source,
        })
    });
    // console.log(items)
    return items
    // Cari link target file
}