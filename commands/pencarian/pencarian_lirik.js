const { fetchUrl, fetchBuffer, pickArray } = require("../../lib/Function")

module.exports = {
    name: "Pencarian Lirik Lagu",
    alias: ["lirik"],
    desc: "Lakukan pencarian lirik lagu",
    type: "pencarian",
    example: `!lirik menunggu`,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("search", m)
        try {
        	let search = await fetchUrl(global.api("some_random_api", `/lyrics`, {title: text}, ""))
            if(search.response)
                if(search.response.status)
                    if(search.response.status == 404)
                        return m.reply(`Waduh!! Paimon belum punya lirik lagu *${text}* T.T`)
            // Template pesan
            let jawaban = ``;
            jawaban += `*${search.title} by ${search.author}*\n`
            jawaban += `${search.lyrics}`
            // End of template pesan
            let imgThumb = await fetchBuffer(`${search.thumbnail.genius}`)
            let resPesan = {
                image: imgThumb,
                caption: `${jawaban}`
            }
            await paimon.sendMessage(m.from, resPesan, { quoted: m })
        } catch (e) {
            console.log(e)
            return m.reply("Waduh!! lirik lagunya lagi dibawa sama Pika buat dicover...")
        }
    }
}