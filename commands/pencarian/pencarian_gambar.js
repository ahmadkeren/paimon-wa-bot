const { fetchUrl, fetchBuffer, pickArray } = require("../../lib/Function")
let { promisify } = require('util')
let _gis = require('g-i-s')
let gis = promisify(_gis)

module.exports = {
    name: "Pencarian Gambar",
    alias: ["gambar"],
    desc: "Cari dan unduh gambar tertentu",
    type: "pencarian",
    example: `!gambar kucing oyen`,
    // isOwner: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("search", m)
        try {
            let search = await gis(text)
            let img = pickArray(search)
            let imgRes = await fetchBuffer(img.url)
            let resPesan = {
                image: imgRes,
                caption: `Hasil pencarian gambar: *${text}*`
            }
            await paimon.sendMessage(m.from, resPesan, { quoted: m })
        } catch (e) {
            console.log(e)
            return m.reply("Waduh!! Paimon gagal mencari gambar, coba lagi nanti ya...")
        }
    }
}