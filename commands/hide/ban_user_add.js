const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { updateFireData } = require("./../../controller/firebase/fireController")
const id_collection = 'users_banned'

module.exports = {
    name: "Ban Member",
    alias: ["ban"],
    desc: "Banned traveler untuk mengakses Paimon",
    type: "hide",
    example: `!ban @tag`,
    isOwner: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        if(m.mentions.length < 1)
            return m.reply(`Silahkan tag traveler yang ingin diban :")`)
        if(m.mentions.length > 1)
            return m.reply(`Bannya satu-satu ya ^_^`)
        let target_ban = m.mentions[0]
        try {
            for(let user of global.users_banned) {
                if(user == target_ban) {
                    return m.reply(`Ano.. traveler *${user.split('@s.whatsapp.net').join('')}* telah di ban sebelumnya :")`)
                    break
                }
            }
            // Banned user
            global.users_banned.push(target_ban)
            writeDB(global.users_banned)
            return m.reply(`Ehe! traveler *${target_ban.split('@s.whatsapp.net').join('')}* berhasil di ban :")`)
        } catch(e) {
            console.log(e)
            return m.reply(`Gagal membanned *${target_ban.split('@s.whatsapp.net').join('')}*`)
        }
    }
}

const writeDB = async (data_baru) => {
    updateFireData(id_collection, data_baru)
}