const { fetchUrl, fetchBuffer, numberWithCommas,secondsToHms } = require("../../lib/Function")
const { cariTraveler, getTravelerCookies, gifDBEngine } = require("../../lib/gif")
const { formPerkenalkanDiri } = require("../../lib/template")

module.exports = {
    name: "Update UID",
    alias: ["update_uid"],
    desc: "Update UID keanggotaan GIF",
    type: "hide",
    example: `!update_uid <uid baru> (tanpa tanda <>)`,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("wait", m)
        // Initial Parameter
        let selfCheck = true
        let target_pencarian = m.sender
        // if(m.mentions.length > 0) {
        //     selfCheck = false
        //     target_pencarian = m.mentions[0]
        // }
        try {
            // Cari anggota
            let cari = await cariTraveler(target_pencarian)
            if (cari == null || !cari)
                return m.reply(`Gomen... UIDnya tidak ditemukan traveler!`)
            // Cetak data
            for (let lmt of cari) {
                // belum registrasi
                if (lmt["WhatsApp_ID"] == target_pencarian && lmt["Status"] == "Blm. Registrasi") {
                    if (selfCheck) {
                        teks = `Oops! kamu belum melakukan registrasi!\n`
                        teks += formPerkenalkanDiri()
                    } else
                        teks = `Oops! Member yang bersangkutan belum melakukan registrasi!`
                    return m.reply(teks)
                } else
                if (lmt["WhatsApp_ID"] == target_pencarian) {
                    // Datanya masih sama
                    if (lmt["UID"] == text)
                        return m.reply(`Ok.. UIDnya masih sama ya traveler ^_^`);
                    // Inisialisasi new signature
                    let data_baru = {
                        "data": [{
                            "UID": text,
                            "CardID_URL": ""
                        }]
                    };
                    // Update data
                    let anu = await gifDBEngine("put", lmt["guild"], target_pencarian, data_baru)
                    // console.log(anu)
                    try {
                        if (anu.error) {
                            console.log(anu.error)
                            return m.reply(`Gomen.. UID tidak dapat diganti!\nCoba hubungi BOSS untuk mengatasi masalah ini..`)
                        }
                        return m.reply(`Yey! UID berhasil diubah menjadi *${text}* ^_^!`)
                    } catch (e) {
                        console.log(e)
                        return m.reply("Gomen.. UID gagal diupdate!")
                    }
                    console.log(anu)
                    break;
                }
            }
        } catch(e) {
            console.log(e)
            return m.reply(`Server sedang gangguan, coba lagi nanti!`)
        }
    }
}