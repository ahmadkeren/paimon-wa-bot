const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { cariTraveler } = require("../../lib/gif")
const { formPerkenalkanDiri, travelerIdentity} = require("../../lib/template")

module.exports = {
    name: "Submit Abyss",
    alias: ["submit_abyss"],
    desc: "Submit data spiral abyss ke GIF - Leaderboard",
    type: "hide",
    example: `!submit_abyss`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("submit", m)
        try {
            // Submit data
            let target_pencarian = m.sender
            let cari = await cariTraveler(target_pencarian)
            if (cari == null || !cari)
                return m.reply(`Gomen... UIDnya tidak ditemukan traveler!`)
            var found = false
            var my_uid = null
            for (let lmt of anu) {
                // belum registrasi
                if (lmt["WhatsApp_ID"] == target_pencarian && lmt["Status"] == "Blm. Registrasi") {
                    teks = `Oops! kamu belum melakukan registrasi!\n`
                    teks += formPerkenalkanDiri()
                } else
                if (lmt["WhatsApp_ID"] == target_pencarian) {
                    found = true
                    my_uid = lmt["UID"]
                    break
                }
            }
            // Panggil API
            try {
                let put = await fetchUrl(global.api("gif_leaderboard", "/submit", { uid: my_uid }, ""))
                let res = JSON.parse(JSON.stringify(put))
                // err check disni
                if (!res.success) {
                    if (res.pesan.includes("list index out of range"))
                        return m.reply("Oops! server API Mihoyo sedang error, silahkan coba lagi nanti!");
                    else if (res.pesan.includes("Maximum") || res.pesan.includes("HTTPSConnectionPool"))
                        return m.reply("Oops! bandwith server API Mihoyo sedang sibuk, silahkan coba lagi nanti!");
                    else if (res.pesan.includes("herokuapp"))
                        return m.reply("Server API sedang sibuk!")
                    else if (res.pesan.includes("genshinstats"))
                        return m.reply("Server API sedang maintenance!")
                    return m.reply(res.pesan);
                }
                return m.reply(`Yey! data kamu berhasil disubmit!\nUntuk melihat Spyral Abyss Leaderboard silahkan akses:\n1. Laman web: https://gif-official.web.app , atau\n2. Command *!leaderboard*\nTerima Kasih ^_^`)
            } catch (e) {
                console.log(e)
                return m.reply("Waduh!! server leaderboardnya lagi gangguan T.T");
            }
            // End Submit data
        } catch(e) {
            console.log(e)
            return m.reply("Gomen... sedang ada masalah teknis, silahkan coba lagi nanti!")
        }
    }
}