const { fetchUrl, fetchBuffer, arrayRemove } = require("../../lib/Function")
const { updateFireData } = require("./../../controller/firebase/fireController")
const id_collection = 'badwords'

module.exports = {
    name: "Hapus Badword",
    alias: ["delbadword","delbadwords","del_badword","del_badwords"],
    desc: "Hapus kata kasar dari database Paimon",
    type: "hide",
    example: `!delbadword kambing`,
    isOwner: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        try {
            for(let bad of global.badwords) {
                if(bad.toLowerCase() == text.toLowerCase()) {
                    global.badwords = arrayRemove(global.badwords, bad)
                    writeDB(global.badwords)
                    return m.reply(`Siap!! kata *${text}* dihapus dari daftar badword!`)
                    break
                }
            }
            // Tidak ada badword yang akan dihapus
            return m.reply(`Ano... kata *${text}* tidak terdaftar di daftar badwords T.T!`)
        } catch(e) {
            console.log(e)
            return m.reply(`Gagal menghapus kata *${text}* di daftar badwords!`)
        }
    }
}

const writeDB = async (data_baru) => {
    updateFireData(id_collection, data_baru)
}