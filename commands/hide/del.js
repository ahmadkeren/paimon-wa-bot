module.exports = {
    name: "Hapus Pesan",
    alias: ["del","delete","hapus"],
    desc: "Hapus pesan WA",
    type: "hide",
    example: `!del [caption pesan paimon]`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper, isGroup, isAdmin }) => {
        try {
            if(m.quoted) {
                console.log(m)
                let msg = m.quoted
                if(msg.fromMe) {
                    let doDelete = true
                    if(isGroup && !isAdmin) doDelete = false // pesan didalam group tapi bukan admin
                    // hapus pesan
                    if(doDelete) {
                        let hapus = await paimon.sendMessage(msg.from, { delete: msg })
                    }
                    return
                }
            }
        } catch(e) {
            console.log(e)
        }
    }
}