const { fetchUrl, fetchBuffer, pickArray } = require("../../lib/Function")

module.exports = {
    name: "Random Hentai Image",
    alias: ["hentai"],
    desc: "Lihat random gambar hentai",
    type: "hide",
    example: `!hentai`,
    isLimit: true, maxLimit: 10,
    isPrivate: true,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("search", m)
        try {
        	const tag = [
                'ecchi',
                'lewdanimegirls',
                'hentai',
                'hentaifemdom',
                'hentaiparadise',
                'hentai4everyone',
                'animearmpits',
                'animefeets', 
                'animethighss', 
                'animebooty', 
                'biganimetiddies', 
                'animebellybutton', 
                'sideoppai', 
                'ahegao',
            ]
        	const randTag = pickArray(tag)
        	let search = await fetchUrl(global.api("meme_image", `/gimme/${randTag}`, {}, ""))
            let imgChars = null
            try {
                imgChars = await fetchBuffer(`${search.preview[2]}`)
            } catch(e) {
                imgChars = await fetchBuffer(`${search.url}`)
            }
            let resPesan = {
                image: imgChars,
                caption: `${search.title}\n_Jangan tanya linknya ke Paimon ya..._`
            }
            await paimon.sendMessage(m.from, resPesan, { quoted: m })
        } catch (e) {
            console.log(e)
            return m.reply("Waduh!! gambarnya dibawa lari natsu...")
        }
    }
}