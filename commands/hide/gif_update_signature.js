const { fetchUrl, fetchBuffer, numberWithCommas,secondsToHms } = require("../../lib/Function")
const { cariTraveler, getTravelerCookies, gifDBEngine } = require("../../lib/gif")
const { formPerkenalkanDiri } = require("../../lib/template")

module.exports = {
    name: "Update Signature",
    alias: ["update_signature"],
    desc: "Update signature keanggotaan GIF",
    type: "hide",
    example: `!update_signature <signature baru> (tanpa tanda < >)`,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("wait", m)
        // Initial Parameter
        let selfCheck = true
        let target_pencarian = m.sender
        // if(m.mentions.length > 0) {
        //     selfCheck = false
        //     target_pencarian = m.mentions[0]
        // }
        try {
            // Cari anggota
            let cari = await cariTraveler(target_pencarian)
            if (cari == null || !cari)
                return m.reply(`Gomen... UIDnya tidak ditemukan traveler!`)
            // Cetak data
            for (let lmt of cari) {
                // belum registrasi
                if (lmt["WhatsApp_ID"] == target_pencarian && lmt["Status"] == "Blm. Registrasi") {
                    if (selfCheck) {
                        teks = `Oops! kamu belum melakukan registrasi!\n`
                        teks += formPerkenalkanDiri()
                    } else
                        teks = `Oops! Member yang bersangkutan belum melakukan registrasi!`
                    return m.reply(teks)
                } else
                if (lmt["WhatsApp_ID"] == target_pencarian) {
                    // Signature masih sama
                    if (lmt["Signature"] == text)
                        return m.reply(`Ok.. signaturenya masih sama ya traveler ^_^`);
                    // Inisialisasi new signature
                    let data_baru = {
                        "data": [{
                            "Signature": text
                        }]
                    };
                    // Update data
                    let anu = await gifDBEngine("put", lmt["guild"], target_pencarian, data_baru)
                    // console.log(anu)
                    try {
                        if (anu.error) {
                            console.log(anu.error)
                            return m.reply(`Gomen.. signature tidak dapat diganti!\nCoba hubungi BOSS untuk mengatasi masalah ini..`)
                        }
                        return m.reply(`Yey! signature berhasil diubah menjadi *${text}* ^_^!`)
                    } catch (e) {
                        console.log(e)
                        return m.reply("Gomen.. signature gagal diupdate!")
                    }
                    console.log(anu)
                    break;
                }
            }
        } catch(e) {
            console.log(e)
            return m.reply(`Server sedang gangguan, coba lagi nanti!`)
        }
    }
}