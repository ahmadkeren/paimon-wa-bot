const { fetchUrl, fetchBuffer, pickArray } = require("../../lib/Function")

module.exports = {
    name: "Ping",
    alias: ["ping","p"],
    desc: "Cek koneksi paimon",
    type: "hide",
    example: `!ping`,
    // isLimit: true, maxLimit: 3, // dipakai untuk debug fitur limit
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        jawabans = [
            `Pang ping pang ping!!\nSana beliin Paimon Almond Topu...`,
            `Iya,, Paimon ada disini...`,
            `Yaw??`,
            `Pe pe pe tenandayo!! >.<`,
            `Ada apa traveler`,
        ];
        let jawaban = pickArray(jawabans)
        return m.reply(jawaban)
    }
}