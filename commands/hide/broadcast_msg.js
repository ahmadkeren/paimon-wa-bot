const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Broadcast Pesan",
    alias: ["bc","broadcast"],
    desc: "Broadcast pesan keseluruh orang yang pernah chat Paimon",
    type: "hide",
    example: `!bc [pesan/media]`,
    isOwner: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper, quoted, mime }) => {
        try {
            let getGroups = await paimon.groupFetchAllParticipating();
            let groups = Object.entries(getGroups).slice(0).map(entry => entry[1])
            let partisipans = groups.map(v => v.id)
            let download = null
            let isImageMsg = false
            // Deteksi + download jika pesan memiliki gambar
            type = "image"
            if (/image|video/.test(mime)) {
                download = await quoted.download()
                isImageMsg = true

                if (/video/.test(mime)) type = 'video'
            }
            // Formatting Pesan
            let resPesan = `[ *PAIMON BROADCAST* ]\n`
            resPesan += text
            // Kirim broadcast
            for(let partisipan of partisipans) {
                if(isImageMsg)
                    // await paimon.sendFile(partisipan, download, "", m, { caption: resPesan })
                    await paimon.sendMessage(partisipan, {
                        [type]: download,
                        caption: resPesan
                    })
                else
                    await paimon.sendMessage(partisipan, { text: resPesan})
            }
            return m.reply("Yey!! pesan berhasil dibroadcast ^_^");
        } catch(e) {
            // console.log(e)
            return m.reply("Pesan gagal dibroadcast karena: "+e);
        }
    }
}