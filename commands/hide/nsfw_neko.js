const { fetchUrl, fetchBuffer, pickArray } = require("../../lib/Function")

module.exports = {
    name: "Random NSFW Neko Image",
    alias: ["nsfw_neko"],
    desc: "Lihat random gambar neko NSFW",
    type: "hide",
    example: `!nsfw_neko`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("search", m)
        try {
            let search = await fetchUrl(global.api("waifu_nsfw_image", `/neko`, {}, ""))
            console.log(search)
            let imgChars = null
            try {
                imgChars = await fetchBuffer(`${search.preview[2]}`)
            } catch(e) {
                imgChars = await fetchBuffer(`${search.url}`)
            }
            let resPesan = {
                image: imgChars,
                caption: `_Jangan tanya linknya ke Paimon ya..._`
            }
            await paimon.sendMessage(m.from, resPesan, { quoted: m })
        } catch (e) {
            console.log(e)
            return m.reply("Waduh!! gambarnya dibawa lari natsu...")
        }
    }
}