const { fetchUrl, fetchBuffer, arrayRemove } = require("../../lib/Function")
const { updateFireData } = require("./../../controller/firebase/fireController")
const id_collection = 'users_banned'

module.exports = {
    name: "Unban Member",
    alias: ["unban"],
    desc: "Unban traveler agar dapat mengakses Paimon",
    type: "hide",
    example: `!unban @tag`,
    isOwner: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        if(m.mentions.length < 1)
            return m.reply(`Silahkan tag traveler yang ingin diunban :")`)
        if(m.mentions.length > 1)
            return m.reply(`Unbannya satu-satu ya ^_^`)
        let target_unban = m.mentions[0]
        try {
            for(let user of global.users_banned) {
                if(user == target_unban) {
                    // Unban user
                    global.users_banned = arrayRemove(global.users_banned, user)
                    writeDB(global.users_banned)
                    return m.reply(`Yey!! traveler *${user.split('@s.whatsapp.net').join('')}* berhasil di unban ^_^`)
                    break
                }
            }
            // Tidak ada traveler yang akan di unban
            return m.reply(`Ehe! traveler *${target_unban.split('@s.whatsapp.net').join('')}* sedang tidak berada dalam daftar ban :")`)
        } catch(e) {
            console.log(e)
            return m.reply(`Gagal mengunban *${target_unban.split('@s.whatsapp.net').join('')}*`)
        }
    }
}

const writeDB = async (data_baru) => {
    updateFireData(id_collection, data_baru)
}