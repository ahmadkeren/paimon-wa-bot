module.exports = {
    name: "Tag Diri Sendiri",
    alias: ["tagme"],
    desc: "Tag akun WhatsApp kamu sendiri",
    type: "hide",
    example: `!tagme`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper, participants }) => {
        try {
            members_id = []
            let res = `@${m.sender.split('@')[0]}`
            return await paimon.sendMessage(m.from, { text: res, mentions: [m.sender] }, { quoted: m })
        } catch(e) {
            console.log(e)
            return m.reply(`Gagal melakukan tag T.T`)
        }
    }
}