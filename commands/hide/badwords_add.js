const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { updateFireData } = require("./../../controller/firebase/fireController")
const id_collection = 'badwords'

module.exports = {
    name: "Tambah badword",
    alias: ["addbadword","addbadwords","add_badword","add_badwords"],
    desc: "Tambah kata kasar kedalam database Paimon",
    type: "hide",
    example: `!addbadword kambing`,
    isOwner: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        try {
            for(let bad of global.badwords) {
                if(bad.toLowerCase() == text.toLowerCase()) {
                    return m.reply(`Ano.. kata *${text}* telah terdaftar sebelumnya :")`)
                    break
                }
            }
            // Banned user
            global.badwords.push(text)
            writeDB(global.badwords)
            return m.reply(`Yey! kata *${text}* berhasil di daftarkan ke daftar badwords ^_^`)
        } catch(e) {
            console.log(e)
            return m.reply(`Gagal menambahkan kata *${text}* di daftar badwords!`)
        }
    }
}

const writeDB = async (data_baru) => {
    updateFireData(id_collection, data_baru)
}