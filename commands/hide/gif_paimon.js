const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { tanyaKePaimon } = require("../../lib/paimon_chat")

module.exports = {
    name: "Tanya ke Paimon",
    alias: ["paimon","ask"],
    desc: "Tanya apapun ke Paimon",
    type: "hide",
    example: `!paimon <pertanyaan kamu> (tanpa tanda <>)`,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        try {
            return await(tanyaKePaimon(m, text))
        } catch(e) {
            console.log(e)
            return m.reply(`Ssst! Paimon lagi belajar.. nanti lagi ya...`)
        }
    }
}