const fs = require("fs")
const { arrayRemove } = require("../../lib/Function")
const { updateFireData } = require("./../../controller/firebase/fireController")
const id_collection = 'registered_groups'

module.exports = {
    name: "Paimon ANN",
    alias: ["ann","live"],
    desc: "Aktifkan mode ANN",
    type: "hide",
    example: `!ann [on/off]`,
    // isOwner: true,
    // isGroup: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { text, isAnnMode }) => {
    	let toggle = text.toLowerCase()
    	try {
    		switch(toggle) {
    			// Aktifkan ANN system
	        	case "on":
                    if(isAnnMode)
                        return m.reply('Mode ANN sudah diaktifkan disini ^_^')
	        		global.paimon_live_chat.push(m.from)
	        		return m.reply('Yey, Mode ANN berhasil diaktifkan disini ^_^')
	        		break;
	        	// Nonaktif ANN system
	        	case "off":
                    if(!isAnnMode)
                        return m.reply('Mode ANN tidak diaktifkan disini ^_^')
                    global.paimon_live_chat = arrayRemove(global.registered_groups, m.from)
	        		return m.reply('Yey, Mode ANN berhasil dinonaktifkan disini ^_^')
	        		break;
	        	default:
	        		return m.reply('Parameter hanya menerima [on/off]!')
	        		break;
	        }
    	} catch(e) {
    		// console.log(e)
    		return m.reply("Terjadi kesalahan: "+e)
    	}
    }
}

const writeDB = async (data_baru) => {
    updateFireData(id_collection, data_baru)
}