const { fetchUrl, fetchBuffer, kirim_pesan_btn_with_img } = require("../../lib/Function")
const { y2mateA } = require("../../lib/y2mate")

module.exports = {
    name: "Putar MP3 Youtube",
    alias: ["yt_mp3"],
    desc: "Putar music dari video youtube",
    type: "hide",
    example: `!yt_mp3 <link>`,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        try {
            let yt = await y2mateA(text)
            console.log(yt)
            m.reply('Tunggu.. sedang Paimon coba kirimkan ^_^')
            // return await paimon.sendFile(m.from, yt[0].link, "", m)
            return await paimon.sendFileUrl(m.from, yt[0].link, "", m)
        } catch (e) {
            console.log(e)
            m.reply("Gomen.. pemutar musik paimon lagi gangguan, coba lagi nanti ya... T.T")
        }
    }

}