const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Kontak Owner",
    alias: ["owner","admin","actinium"],
    desc: "Kirim kontak owner Paimon",
    type: "lainnya",
    example: `!owner`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        const vcard = 'BEGIN:VCARD\n' // metadata of the contact card
            + 'VERSION:3.0\n' 
            + 'FN:Ahmad S.\n' // full name
            + 'ORG:Traveler paling nup...;\n' // the organization of the contact
            + 'TEL;type=CELL;type=VOICE;waid=6282282170805:+62 822-8217-0805\n' // WhatsApp ID + phone number
            + 'END:VCARD'
        return await paimon.sendMessage(
            m.from,
            { 
                contacts: { 
                    displayName: 'Jeff', 
                    contacts: [{ vcard }] 
                }
            }
        )
    }
}