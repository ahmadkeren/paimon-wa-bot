const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Report / Laporkan Masalah",
    alias: ["report","lapor"],
    desc: "Laporkan berbagai permasalahan ke sistem",
    type: "lainnya",
    example: `!report <masalah>`,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        let laporan = ``
        laporan += `*[ LAPORAN ]*\n`
        laporan += `*Dari*: ${m.sender.split('@')[0]} (${m.pushName})\n`
        laporan += `*Laporan*: ${text}`
        let lapor = await paimon.sendMessage('6282282170805@s.whatsapp.net', {text: laporan}, { quoted: m }) // kirim ke actinium
        return await m.reply("Yey!! masalah berhasil dilaporkan ^_^\nSilahkan tunggu eksekusinya ya Traveler...")
    }
}