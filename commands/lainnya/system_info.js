const { fetchUrl, fetchBuffer, secondsToHms } = require("../../lib/Function")
const os = require('os')
const { sizeFormatter } = require('human-readable')

const format = sizeFormatter({
  std: 'JEDEC', // 'SI' (default) | 'IEC' | 'JEDEC'
  decimalPlaces: 2,
  keepTrailingZeroes: false,
  render: (literal, symbol) => `${literal} ${symbol}B`,
})

module.exports = {
    name: "Server Information",
    alias: ["sys_info","sistem_info","server"],
    desc: "Lihat detail server Paimon",
    type: "lainnya",
    example: `!sys_info`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        let jawaban = ``
        let num = 1;
        let cpus = os.cpus()
        jawaban += `💻 *INFO SERVER PAIMON*\n`
        jawaban += `------------------------\n`
        jawaban += `*Arsitektur*: ${os.arch()}\n`
        jawaban += `*Prosesor*: ${cpus[0].model} (${cpus.length} Cores)\n`
        jawaban += `*Memori (RAM) Bebas*: ${format(os.totalmem() - os.freemem())} / ${format(os.totalmem())}\n`
        jawaban += `*Sistem Operasi*: ${os.type()} ${os.release()}\n`
        jawaban += `*Waktu Berjalan Server*: ${secondsToHms(os.uptime())}\n`
        jawaban += `------------------------\n`
        jawaban += `*Paimon: kenapa tuh ngecek-ngecek? mau upgrade server paimon ya?`
        return m.reply(jawaban)
    }
}