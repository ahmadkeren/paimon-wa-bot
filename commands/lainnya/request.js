const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Request Fitur",
    alias: ["request","req"],
    desc: "Usulkan fitur untuk perkembangan Paimon",
    type: "lainnya",
    example: `!request <keterangan request>`,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        let laporan = ``
        laporan += `*[ REQUEST FITUR ]*\n`
        laporan += `*Dari*: ${m.sender.split('@')[0]} (${m.pushName})\n`
        laporan += `*Request*: ${text}`
        let lapor = await paimon.sendMessage('6282282170805@s.whatsapp.net', {text: laporan}, { quoted: m }) // kirim ke actinium
        return await m.reply("Yey!! request berhasil disubmit ^_^")
    }
}