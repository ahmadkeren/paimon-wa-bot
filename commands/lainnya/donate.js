const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Traktir Jade Parcel",
    alias: ["donate","donasi","traktir"],
    desc: "Traktir Paimon makanan Jade Parcel",
    type: "lainnya",
    example: `!donate`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        /*
        jawaban  = `== *TRAKTIR PAIMON* ==\n`
        jawaban += `Berikut daftar virtual account resmi untuk donasi ke Paimon:\n`
        jawaban += `- Saweria: https://saweria.co/gifofficial\n`
        jawaban += `Catatan: Hati-hati penipuan yang mengatas namakan Paimon GIF\n`
        */
        jawaban  = `Apa? mau traktir paimon? gass beliin jade parcel di liyue ^_^`
        return m.reply(jawaban)
    }
}