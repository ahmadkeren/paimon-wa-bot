const { fetchUrl, fetchBuffer, kirim_pesan_btn_with_img } = require("../../lib/Function")
const yts = require('yt-search')

module.exports = {
    name: "Putar Music / Video",
    alias: ["play","mp3","mp4","yt","youtube"],
    desc: "Cari dan putar musik atau video",
    type: "multimedia",
    example: `!play <judul>`,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("search", m)
        try {
            // Cari data
            let anu = await yts(text)
            // return console.log(anu)
            if(anu.videos.length==0)
                return m.reply("Gomen!! paimon tidak dapat menemukan apa yang ingin kamu cari!")
            cnt = 1;
            for(let yutub of anu.videos) {
                if (yutub.type!="video") continue
                if(cnt>3) break
                // Informasi
                infomp3 = `╭─「 *TIMELINE MEDIA* 」\n`
                infomp3 += `│ *• Judul:* ${yutub.title}\n`
                infomp3 += `│ *• Durasi:* ${yutub.timestamp}\n`
                infomp3 += `│ *• Deksripsi:* ${yutub.description}\n`
                infomp3 += `╰─────────────────────`
                const buttons = [
                  {buttonId: `yt_mp4 ${yutub.url}`, buttonText: {displayText: '▶️ Video'}, type: 1},
                  {buttonId: `yt_mp3 ${yutub.url}`, buttonText: {displayText: '▶️ Audio'}, type: 1}
                ]
                let thumbIMG = await fetchBuffer(`${yutub.thumbnail}`)
                await kirim_pesan_btn_with_img(paimon, m, thumbIMG, buttons, infomp3, `Source: ${yutub.url}`)
                cnt+=1
            }
            return
        } catch (e) {
            console.log(e)
            reply("Gomen.. keknya mp3 player paimon lagi rusak T.T")
        }
    }
}