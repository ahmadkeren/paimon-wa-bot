const { fetchUrl, fetchBuffer, kirim_pesan_btn_with_img, truncateString } = require("../../lib/Function")
const { getKependekan, refineBeautifer } = require("../../lib/genshin")
const { generateListMenu } = require("../../lib/menu_maker")
const genshindb = require("genshin-db")

module.exports = {
    name: "Senjata Genshin",
    alias: ["weapon","wp","weapons"],
    desc: "Lihat detail senjata dan material ascendnya",
    type: "genshin",
    example: `!weapon wgs`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        // console.log(m)
        if(args[0]) {
            // Tampilkan data senjata
            try {
                target_pencarian = text
                target_pencarian = target_pencarian.split("%").join("").toLowerCase()
                data = await genshindb.weapons(target_pencarian,{matchCategories: true})
                if (Array.isArray(data)) {
                    jawaban = `「 *HASIL PENCARIAN WEAPON* 」\n`
                    jawaban += `──────────────────────\n`
                    jawaban += `Kata kunci: *${target_pencarian}*\n`
                    jawaban += `──────────────────────\n`
                    for (let weapon of data) {
                        wp = await genshindb.weapons(weapon)
                        if(wp) {
                            jawaban += `- ${truncateString(weapon,16)} (${wp.rarity}★ / ${wp.weapontype} / ${getKependekan(wp.substat)})\n`
                        }
                    }
                    jawaban += `──────────────────────\n`
                    jawaban += `Untuk melihat detail senjata, silahkan ketik kata kunci dengan lebih spesifik atau pilih dari daftar dibawah!\n`
                    jawaban += (`Contoh: *!weapon ${data[0].toLowerCase()}*`)
                    // Generate list senjata
                    title = `Daftar Pencarian Senjata`
                    let txt_balasan = jawaban
                    cmd = "weapon"
                    desc = "Lihat material ascend dan detail"
                    list = [
                        {
                            title: `Kata Kunci: ${target_pencarian}`,
                            rows: data
                        },
                    ]
                    balasan = await generateListMenu(title,txt_balasan,cmd,desc,list)
                    return await paimon.sendMessage(m.from, balasan, {quoted: m})
                } else
                if (data != null && data.name) {
                    wp = data
                    effect = refineBeautifer(wp)
                    jawaban  = `╭─「 *HASIL PENCARIAN WEAPON* 」\n`
                    jawaban += `│• *Nama Weapon:* ${wp.name}\n`
                    jawaban += `│• *Rarity:* ${wp.rarity} Stars\n`
                    jawaban += `│• *Type:* ${wp.weapontype}\n`
                    jawaban += `│• *Base ATK:* ${wp.baseatk}\n`
                    jawaban += `│• *Sub Stat:* ${wp.subvalue}% ${wp.substat}\n`
                    jawaban += `│• *Deskripsi:* ${wp.description.split("\\n").join(" ")}\n`
                    jawaban += `│• *Effect:* ${effect}\n`
                    jawaban += `│─────────────────────\n`
                    jawaban += `│ *!item* <material>: info material\n`
                    jawaban += `╰─────────────────────`
                    try {
                        await global.mess("send", m)
                        let thumbIMG = await fetchBuffer(global.api("gif_canvas", `/weapon/${wp.name}`, { query: text }, "apikey"))
                        let resPesan = {
                            image: thumbIMG,
                            caption: jawaban
                        }
                        return await paimon.sendMessage(m.from, resPesan, { quoted: m })
                    } catch (e) {
                        console.log(e)
                        m.reply(jawaban)
                    }
                } else {
                    return m.reply(`Oops! senjata dengan kata kunci *${target_pencarian}* tidak ditemukan!`)
                }
            } catch (e) {
                console.log(e)
                return m.reply("Gomen.. buku paimon dicuri swiper T.T")
            }
            // End Tampilkan data senjata
        } else {
            // Tampilkan seluruh senjata genshin impact
            title = "Daftar Senjata"
            let txt_balasan = "Untuk melihat material ascension dan detail senjata, silakan ketik *!weapon <nama_senjata>* atau pilih dari daftar senjata dibawah!"
            cmd = "weapon"
            desc = "Lihat material ascend dan detail"
            list = [
                {
                    title: 'Claymore',
                    rows: genshindb.weapons('Claymore', { matchCategories: true })
                },
                {
                    title: 'Bow',
                    rows: genshindb.weapons('Bow', { matchCategories: true })
                },
                {
                    title: 'Catalyst',
                    rows: genshindb.weapons('Catalyst', { matchCategories: true })
                },
                {
                    title: 'Sword',
                    rows: genshindb.weapons('Sword', { matchCategories: true })
                },
                {
                    title: 'Polearm',
                    rows: genshindb.weapons('Polearm', { matchCategories: true })
                },
            ]
            balasan = await generateListMenu(title,txt_balasan,cmd,desc,list)
            return await paimon.sendMessage(m.from, balasan, {quoted: m})
        }
    }
}