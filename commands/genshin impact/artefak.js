const { fetchUrl, fetchBuffer, capitalize } = require("../../lib/Function")
const { sheetDB } = require("../../lib/sheetDB")
const { generateListMenu } = require("../../lib/menu_maker")
const genshindb = require("genshin-db")

module.exports = {
    name: "Artefak Genshin Impact",
    alias: ["arte","artefak","artifak","artepak"],
    desc: "Lihat detail artefak yang ada di genshin impact",
    type: "genshin",
    example: `!arte crimson`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        // console.log(m)
        if(args[0]) {
            // Tampilkan artefak genshin
            try {
                target_pencarian = text
                target_pencarian = target_pencarian.toLowerCase()
                // Search by name
                let data_master = await genshindb.artifacts(target_pencarian)
                if (data_master == null)
                    return m.reply('Oops! paimon tidak punya data tersebut :")')
                // console.log(data_master)
                try {
                    let artefak = data_master
                    jawaban = `╭─「 *HASIL PENCARIAN ARTEFAK* 」\n`
                    jawaban += `│• *Nama Artefak:* ${artefak.name}\n`
                    jawaban += `│• *Rarity:* ${artefak.rarity.toString()} Stars\n`
                    if (artefak["1pc"] != undefined && artefak["1pc"] != null)
                        jawaban += `│• *Bonus 1 Piece:* ${artefak["1pc"]}\n`
                    if (artefak["2pc"] != undefined && artefak["2pc"] != null)
                        jawaban += `│• *Bonus 2 Piece:* ${artefak["2pc"]}\n`
                    if (artefak["4pc"] != undefined && artefak["4pc"] != null)
                        jawaban += `│• *Bonus 4 Piece:* ${artefak["4pc"]}\n`
                    jawaban += `╰─────────────────────`
                    try {
                        let img = artefak.images
                        try {
                            thumbIMG = await fetchBuffer(img.flower,null, { query: text }, "apikey")
                        } catch (e) {
                            console.log(e)
                            try {
                                thumbIMG = await fetchBuffer(img.plume,null, { query: text }, "apikey")
                            } catch (e) {
                                console.log(e)
                                try {
                                    thumbIMG = await fetchBuffer(img.sands,null, { query: text }, "apikey")
                                } catch (e) {
                                    thumbIMG = await fetchBuffer(img.goblet,null, { query: text }, "apikey")
                                    console.log(e)
                                }
                            }
                        }
                        let resPesan = {
                            image: thumbIMG,
                            caption: `${jawaban}`
                        }
                        return await paimon.sendMessage(m.from, resPesan, { quoted: m })
                    } catch (e) {
                        console.log(e)
                        m.reply(jawaban)
                    }
                } catch (e) {
                    console.log(e)
                }
            } catch (e) {
                console.log(e)
                return m.reply("Gomen.. buku paimon dicuri swiper T.T")
            }
            // End Tampilkan artefak genshin
        } else {
            // Tampilkan list
            let txt_balasan = "Untuk melihat detail artefak silakan ketik *!artefak <nama_artefak>* atau pilih dari daftar dibawah!"
            title = "Daftar Artefak"
            cmd = "artefak"
            desc = "Lihat detail artefak"
            list = [
                {
                    title: title,
                    rows: genshindb.artifacts('names', { matchCategories: true })
                }
            ]
            balasan = await generateListMenu(title,txt_balasan,cmd,desc,list)
            return await paimon.sendMessage(m.from, balasan, {quoted: m})
        }
    }
}