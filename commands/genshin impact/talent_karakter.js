const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { sheetDB } = require("../../lib/sheetDB")
const { generateListMenu } = require("../../lib/menu_maker")
const genshindb = require("genshin-db")

module.exports = {
    name: "Talent Karakter",
    alias: ["talent","talents","skill","skills"],
    desc: "Lihat skill/talent karkater genshin impact",
    type: "genshin",
    example: `!talent amber`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        // console.log(m)
        if(args[0]) {
            // Tampilkan talent karakter
            try {
                target_pencarian = text
                target_pencarian = target_pencarian.toLowerCase()
                // Search by name
                data_master = await genshindb.talents(target_pencarian)
                if (data_master == null)
                    return m.reply("Oops! paimon tidak tau data skill karakter tersebut..")
                global.mess("check", m)
                jawaban = `──「 *TALENT/SKILL KARAKTER* 」──\n`
                jawaban+= `• *Nama Karakter:* ${data_master.name}\n`
                try {
                    for (let key in data_master) {
                        if (!key.includes("passive") && !key.includes("combat") ) continue
                        skill = data_master[key]
                        // console.log(skill)
                        skill_info = skill.info.split("**").join("*")
                        skill_info = skill_info.split("·").join("· ")
                        jawaban += `─────────────────────\n`
                        jawaban += `# *${skill.name}* \n`
                        jawaban += `${skill_info}\n`
                    }
                } catch (e) {
                    console.log(e)
                }
                jawaban += `─────────────────────`
                try {
                    let imgRender = await fetchBuffer(global.api("gif_canvas", `/talent/${text}`, {}, ""))
                    let resPesanTwo = {
                        image: imgRender,
                        caption: `${jawaban}`
                    }
                    return await paimon.sendMessage(m.from, resPesanTwo, { quoted: m })
                } catch(e) {
                    console.log(e)
                    return m.reply(jawaban)
                }
            } catch (e) {
                console.log(e)
                return m.reply("Gomen.. buku paimon dicuri swiper T.T")
            }
            // End Tampilkan talent karakter
        } else {
            // Tampilkan seluruh karakter genshin impact
            let txt_balasan = "Untuk melihat talent/skill karakter silakan ketik *!talent <nama_karakter>* atau pilih dari daftar karakter dibawah!"
            title = "Daftar Karakter"
            cmd = "talent"
            desc = "Lihat detail talent/skill dari karakter"
            list = [
                {
                    title: title,
                    rows: genshindb.characters('names', { matchCategories: true })
                }
            ]
            balasan = await generateListMenu(title,txt_balasan,cmd,desc,list)
            return await paimon.sendMessage(m.from, balasan, {quoted: m})
        }
    }
}