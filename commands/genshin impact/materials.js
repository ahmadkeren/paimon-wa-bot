const { fetchUrl, fetchBuffer, capitalize, numberWithCommas } = require("../../lib/Function")
const { sheetDB } = require("../../lib/sheetDB")
const { generateListMenu } = require("../../lib/menu_maker")
const genshindb = require("genshin-db")

module.exports = {
    name: "Item / Material",
    alias: ["item","items"],
    desc: "Lihat detial item/material yang ada di genshin impact",
    type: "genshin",
    example: `!item iron ore`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        // console.log(m)
        if(args[0]) {
            // Tampilkan artefak genshin
            try {
                target_pencarian = text
                target_pencarian = target_pencarian.toLowerCase()
                // Search by name
                let data_master = await genshindb.material(target_pencarian)
                if (data_master == null)
                    return m.reply('Oops! paimon tidak punya data tersebut :")')
                try {
                    let item_data = data_master
                    // console.log(item_data)
                    jawaban = `╭─「 *HASIL PENCARIAN ITEM* 」\n`
                    jawaban += `│• *Nama Item:* ${item_data.name}\n`
                    if (item_data.rarity != undefined && item_data.rarity != null)
                        jawaban += `│• *Rarity:* ${item_data.rarity} Stars\n`
                    jawaban += `│• *Type:* ${item_data.materialtype}\n`
                    if (item_data.category != undefined && item_data.category != null)
                        jawaban += `│• *Kategori:* ${item_data.category}\n`
                    jawaban += `│• *Deskripsi:* ${item_data.description.split("\\n").join(" ")}\n`
                    if (item_data.source.length > 0)
                        jawaban += `│• *Cara Mendapatkan:*\n`
                    for (let source of item_data.source) {
                        jawaban += `│  - ${source}\n`
                    }
                    jawaban += `╰─────────────────────`
                    let thumbIMG = await fetchBuffer(global.api("gi_image", `/${item_data.images.nameicon}.png`, { query: text }, "apikey"))
                    let resPesan = {
                        image: thumbIMG,
                        caption: `${jawaban}`
                    }
                    return await paimon.sendMessage(m.from, resPesan, { quoted: m })
                } catch (e) {
                    console.log(e)
                }
            } catch (e) {
                console.log(e)
                return m.reply("Gomen.. buku paimon dicuri swiper T.T")
            }
            // End Tampilkan artefak genshin
        } else {
            // Tampilkan list
            let txt_balasan = "Untuk melihat detail material/item silakan ketik *!item <nama_item>* atau pilih dari daftar dibawah!"
            title = "Daftar Makanan"
            cmd = "item"
            desc = "Lihat detail item"
            list = [
                {
                    title: title,
                    rows: genshindb.materials('names', { matchCategories: true })
                }
            ]
            balasan = await generateListMenu(title,txt_balasan,cmd,desc,list)
            return await paimon.sendMessage(m.from, balasan, {quoted: m})
        }
    }
}