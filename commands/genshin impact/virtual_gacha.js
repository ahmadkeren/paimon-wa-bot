const { fetchUrl, fetchBuffer } = require("../../lib/Function")

// Variabel virtual gacha
wisher = []

module.exports = {
    name: "Virtual Gacha",
    alias: ["gacha","virtual_gacha"],
    desc: "Mencoba melakukan gacha secara virtual",
    type: "genshin",
    example: `!gacha char/weapon/standar`,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        // Cek apakah sudah pernah ngewish
        if (wisher[m.from] == undefined || wisher[m.from] == null) {
            wisher[m.from] = {
                "200": {
                    "pity": {
                        "b5": 0,
                        "b4": 0,
                    },
                    "guaranteed": {
                        "b5": 90,
                        "b4": 10,
                    },
                },
                "301": {
                    "pity": {
                        "b5": 0,
                        "b4": 0,
                    },
                    "guaranteed": {
                        "b5": 90,
                        "b4": 10,
                    },
                    "rate_on": {
                        "b5": false,
                        "b4": false,
                    },
                },
                "302": {
                    "pity": {
                        "b5": 0,
                        "b4": 0,
                    },
                    "guaranteed": {
                        "b5": 80,
                        "b4": 10,
                    },
                    "rate_on": {
                        "b5": false,
                        "b4": false,
                    },
                },
            }
            console.log(wisher)
        }
        //console.log(wisher)
        // Filter jenis banner
        var cari = text
        var id_banner = null
        if (cari.includes("char") || cari.includes("karakter")) {
            id_banner = 301
        }
        if (cari.includes("senjata") || cari.includes("weapon")) {
            id_banner = 302
        }
        if (cari.includes("standar") || cari.includes("default")) {
            id_banner = 200
        }
        if (id_banner == null)
            return m.reply(`Ano.. Paimon gak tau ada banner *${cari}* :")`)
        m.reply("Gacha dilakukan... wuzzzz...\n*) _Ceritanya suara bintang jatuh_")
        try {
            // Ambil data
            let data_master = await fetchUrl(global.api("gif_official", "/banner", { query: text }, "apikey"))
            var data = []
            for (let tmp of data_master) {
                if (tmp.banner_type == id_banner) {
                    data = tmp
                    break
                }
            }
            // Ambil persentase
            hasil_gacha = {
                "b5": [],
                "b4": [],
                "b3": []
            }
            star_type = {
                "b5": "🌟",
                "b4": "⭐",
                "b3": "★"
            }
            guaranteed_5_star = false // guaranteed system
            guaranteed_4_star = false // guaranteed system
            for (let i = 1; i <= 10; i++) {
                hoky = Math.random() * 100; // 0.00 - 100.00
                rate_up = Math.random() * 100; // 0.00 - 100.00
                // Tambah pity
                wisher[m.from][id_banner]["pity"]["b5"] += 1
                wisher[m.from][id_banner]["pity"]["b4"] += 1
                // Guaranteed check pity
                if (wisher[m.from][id_banner]["pity"]["b5"] + 1 >= wisher[m.from][id_banner]["guaranteed"]["b5"])
                    guaranteed_5_star = true
                if (wisher[m.from][id_banner]["pity"]["b4"] + 1 >= wisher[m.from][id_banner]["guaranteed"]["b4"])
                    guaranteed_4_star = true
                // Simulasi gacha
                if (hoky <= parseFloat(data.r5_prob) || guaranteed_5_star) {
                    // dapat b5
                    have_rate_up_system = true
                    chance = parseFloat(data.r5_up_prob)
                    if (data.r5_up_prob == null || data.r5_up_prob == NaN) {
                        chance = 0
                        have_rate_up_system = false
                    }
                    if (have_rate_up_system && wisher[m.from][id_banner]["rate_on"]["b5"]) chance = 100
                    // Sistem gacha
                    data_from_rate_up_list = false
                    if (rate_up <= chance) {
                        // Chance rate up system
                        data_usage = data.r5_up_items
                        wisher[m.from][id_banner]["rate_on"]["b5"] = false
                        data_from_rate_up_list = true
                    } else {
                        data_usage = data.r5_items
                    }
                    randIndex = Math.floor(Math.random() * data_usage.length)
                    hasil_gacha["b5"].push(data_usage[randIndex])
                    if (have_rate_up_system && data_usage[randIndex].is_up) {
                        wisher[m.from][id_banner]["rate_on"]["b5"] = false
                    } else
                    if (have_rate_up_system && !data_usage[randIndex].is_up && !data_from_rate_up_list) {
                        wisher[m.from][id_banner]["rate_on"]["b5"] = true
                    }
                    // Reset pity
                    wisher[m.from][id_banner]["pity"]["b5"] = 0
                    guaranteed_5_star = false
                } else
                if (hoky <= parseFloat(data.r4_prob) || guaranteed_4_star || (i >= 10 && hasil_gacha["b4"].length < 1)) {
                    // dapat b4
                    have_rate_up_system = true
                    chance = parseFloat(data.r4_up_prob)
                    if (data.r4_up_prob == null || data.r4_up_prob == NaN) {
                        chance = 0
                        have_rate_up_system = false
                    }
                    if (have_rate_up_system && wisher[m.from][id_banner]["rate_on"]["b4"]) chance = 100
                    // Sistem gacha
                    data_from_rate_up_list = false
                    if (rate_up <= chance && have_rate_up_system) {
                        // Chance rate up system
                        data_usage = data.r4_up_items
                        wisher[m.from][id_banner]["rate_on"]["b4"] = false
                        data_from_rate_up_list = true
                    } else {
                        data_usage = data.r4_items
                    }
                    randIndex = Math.floor(Math.random() * data_usage.length)
                    hasil_gacha["b4"].push(data_usage[randIndex])
                    if (have_rate_up_system && data_usage[randIndex].is_up) {
                        wisher[m.from][id_banner]["rate_on"]["b4"] = false
                    } else
                    if (have_rate_up_system && !data_usage[randIndex].is_up && !data_from_rate_up_list) {
                        wisher[m.from][id_banner]["rate_on"]["b4"] = true
                    }
                    // Reset pity
                    wisher[m.from][id_banner]["pity"]["b4"] = 0
                    guaranteed_4_star = false
                } else {
                    // dapat b3
                    data_usage = data.r3_items
                    randIndex = Math.floor(Math.random() * data_usage.length)
                    hasil_gacha["b3"].push(data_usage[randIndex])
                }
            }
            all_data = [] // dipakai untuk UI gacha
            // Generate hasil
            jawaban = `╭─「 *HASIL VIRTUAL GACHA* 」\n`
            jawaban += `│• *Banner:* ${data.name}\n`
            jawaban += `│• *Jenis:* ${data.banner_type_name}\n`
            jawaban += `│• *Pity:* ${wisher[m.from][id_banner]["pity"]["b5"]}x pull\n`
            jawaban += `│─────────\n`
            for (let i = 5; i >= 3; i--) {
                for (let hasil of hasil_gacha["b" + i]) {
                    all_data.push(hasil.name)
                    if (i > 3)
                        jawaban += `│- *${hasil.name} ${star_type["b"+i]}${i}* (${hasil.type})\n`
                    else
                        jawaban += `│- ${hasil.name} ${star_type["b"+i]}${i} (${hasil.type})\n`
                }
            }
            if (hasil_gacha["b5"].length >= 1) {
                jawaban += `│─────────\n`
                jawaban += `│_Yey! kamu mendapatkan ⭐5_ :)\n`
            }
            jawaban += `╰─────────`
            let imgChars = await fetchBuffer(global.api("gif_canvas", `/gacha`, { q: all_data }, "apikey"))
            let resPesan = {
                image: imgChars,
                caption: `${jawaban}`
            }
            await paimon.sendMessage(m.from, resPesan, { quoted: m })
        } catch (e) {
            console.log(e)
            return m.reply('Gomen.. gachanya gagal karena primo paimon gak cukup :")')
        }
    }
}