const { fetchUrl, fetchBuffer, kirim_pesan_btn_with_img, truncateString } = require("../../lib/Function")
const { getKependekan, refineBeautifer } = require("../../lib/genshin")
const { generateListMenu } = require("../../lib/menu_maker")
const genshindb = require("genshin-db")

module.exports = {
    name: "Karakter Genshin",
    alias: ["char","character","chars"],
    desc: "Lihat detail base stats dan biodata karakter",
    type: "genshin",
    example: `!char amber`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        // console.log(m)
        if(args[0]) {
            // Tampilkan data karakter
            try {
                target_pencarian = text.toLowerCase()
                data = await genshindb.characters(target_pencarian,{matchCategories: true})
                if (Array.isArray(data)) {
                    jawaban = `「 *HASIL PENCARIAN KARAKTER* 」\n`
                    jawaban+= `──────────────────────\n`
                    jawaban+= `Kata kunci: *${target_pencarian}*\n`
                    jawaban+= `──────────────────────\n`
                    for( let char of data) {
                        cr = await genshindb.characters(char)
                        if(cr) {
                            jawaban+=`- ${char} (${cr.rarity}★ / ${cr.weapontype} / ${getKependekan(cr.substat)})\n`
                        }
                    }
                    jawaban+= `──────────────────────\n`
                    jawaban += `Untuk melihat detail karakter, silahkan ketik kata kunci dengan lebih spesifik atau pilih dari daftar dibawah!\n`
                    jawaban+=(`Contoh: !char *${data[0]}*`)
                    // Generate list senjata
                    title = `Daftar Pencarian Karakter`
                    let txt_balasan = jawaban
                    cmd = "char"
                    desc = "Lihat detail karakter"
                    list = [
                        {
                            title: `Kata Kunci: ${target_pencarian}`,
                            rows: data
                        },
                    ]
                    balasan = await generateListMenu(title,txt_balasan,cmd,desc,list)
                    return await paimon.sendMessage(m.from, balasan, {quoted: m})
                } else 
                if (data != null && data.name) {
                    char_info = data
                    char_name = char_info.name.toLowerCase()
                    jawaban = `╭─「 *HASIL PENCARIAN KARAKTER* 」\n`
                    jawaban+= `│• *Nama:* ${char_info.name}\n`
                    jawaban+= `│• *Title:* ${char_info.title}\n`
                    jawaban+= `│• *Rarity:* ${char_info.rarity} Stars\n`
                    jawaban+= `│• *Gender:* ${char_info.gender} (${char_info.body})\n`
                    jawaban+= `│• *Weapon:* ${char_info.weapontype}\n`
                    jawaban+= `│• *Elemen:* ${char_info.element}\n`
                    jawaban+= `│• *Region:* ${char_info.region}\n`
                    jawaban+= `│• *Ulang Tahun:* ${char_info.birthday}\n`
                    jawaban+= `│• *Konstelasi:* ${char_info.constellation}\n`
                    jawaban+= `│• *Afiliasi:* ${char_info.affiliation}\n`
                    jawaban+= `│• *Deskripsi:* ${char_info.description}\n`
                    try {
                        // thumbIMG = await fetchBuffer(`http://gif-canvas.herokuapp.com/char/${char_info.name}`)
                        await global.mess("send", m)
                        let thumbIMG = await fetchBuffer(global.api("gif_canvas", `/char/${char_info.name}`, { query: text }, "apikey"))
                        jawaban+= `╰───────────`
                        if(char_name.includes("aether") || char_name.includes("lumine"))
                            return await paimon.sendMessage(m.from, thumbIMG, {
                                quoted: m,
                                caption: jawaban
                            })
                        const buttons = [
                              {buttonId: `constel ${char_info.name}`, buttonText: {displayText: 'Lihat Konstelasi'}, type: 1},
                              {buttonId: `talent ${char_info.name}`, buttonText: {displayText: 'Lihat Talent/Skills'}, type: 1},
                              {buttonId: `build ${char_info.name}`, buttonText: {displayText: 'Lihat Build Karakter'}, type: 1},
                              {buttonId: `ascend ${char_info.name}`, buttonText: {displayText: 'Lihat Material Ascend'}, type: 1},
                            ]
                        // await waConnection.sendMessage(from, pok, image, { quoted: mek , caption: jawaban})
                        return await kirim_pesan_btn_with_img(paimon, m, thumbIMG, buttons, jawaban,)
                    } catch(e) {
                        console.log(e)
                        return m.reply(jawaban)
                    }
                } else {
                    return m.reply(`Oops! karakter dengan kata kunci *${target_pencarian}* tidak ditemukan!`)
                }
            } catch (e) {
                console.log(e)
                return m.reply("Gomen.. buku paimon dicuri swiper T.T")
            }
            // End Tampilkan data karakter
        } else {
            // Tampilkan seluruh karakter genshin impact
            let txt_balasan = "Untuk melihat detail karakter, silakan ketik *!char <nama_karakter>* atau pilih dari daftar karakter dibawah!"
            title = "Daftar Karakter"
            cmd = "char"
            desc = "Lihat stats dan biodata karakter"
            list = [
                {
                    title: title,
                    rows: genshindb.characters('names', { matchCategories: true })
                }
            ]
            balasan = await generateListMenu(title,txt_balasan,cmd,desc,list)
            return await paimon.sendMessage(m.from, balasan, {quoted: m})
        }
    }
}