const { fetchUrl, fetchBuffer, capitalize, numberWithCommas } = require("../../lib/Function")
const { sheetDB } = require("../../lib/sheetDB")
const { generateListMenu } = require("../../lib/menu_maker")
const genshindb = require("genshin-db")

module.exports = {
    name: "Foods Genshin Impact",
    alias: ["food","foods","makanan","resep"],
    desc: "Lihat detail makanan di genshin impact",
    type: "genshin",
    example: `!food alomnd tofu`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        // console.log(m)
        if(args[0]) {
            // Tampilkan artefak genshin
            try {
                target_pencarian = text
                target_pencarian = target_pencarian.toLowerCase()
                // Search by name
                let data_master = await genshindb.foods(target_pencarian)
                if (data_master == null)
                    return m.reply('Oops! paimon tidak punya data tersebut :")')
                try {
                    let item = data_master
                    // console.log(item)
                    jawaban = `╭─「 *HASIL PENCARIAN FOODS* 」\n`
                    jawaban += `│• *Nama:* ${item.name}\n`
                    jawaban += `│• *Rarity:* ${item.rarity} Stars\n`
                    jawaban += `│• *Type:* ${item.foodtype} / ${item.foodfilter}\n`
                    jawaban += `│• *Deskripsi:* ${item.description.split("\\n").join(" ")}\n`
                    jawaban += `│• *Effect:* ${item.effect}\n`
                    jawaban += `│• *Bahan:*\n`
                    for (let bahan of item.ingredients) {
                        jawaban += `│  - ${bahan.name} *x${numberWithCommas(bahan.count)}*\n`
                    }
                    jawaban += `╰─────────────────────`
                    let thumbIMG = await fetchBuffer(global.api("gi_image", `${item.images.nameicon}.png`, { query: text }, "apikey"))
                    let resPesan = {
                        image: thumbIMG,
                        caption: `${jawaban}`
                    }
                    return await paimon.sendMessage(m.from, resPesan, { quoted: m })
                } catch (e) {
                    console.log(e)
                }
            } catch (e) {
                console.log(e)
                return m.reply("Gomen.. buku paimon dicuri swiper T.T")
            }
            // End Tampilkan artefak genshin
        } else {
            // Tampilkan list
            let txt_balasan = "Untuk melihat detail makanan silakan ketik *!food <nama_makanan>* atau pilih dari daftar dibawah!"
            title = "Daftar Makanan"
            cmd = "food"
            desc = "Lihat detail makanan"
            list = [
                {
                    title: title,
                    rows: genshindb.foods('names', { matchCategories: true })
                }
            ]
            balasan = await generateListMenu(title,txt_balasan,cmd,desc,list)
            return await paimon.sendMessage(m.from, balasan, {quoted: m})
        }
    }
}