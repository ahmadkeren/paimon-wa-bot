const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { sheetDB } = require("../../lib/sheetDB")
const { generateListMenu } = require("../../lib/menu_maker")
const genshindb = require("genshin-db")

module.exports = {
    name: "Tier List / Meta Genshin",
    alias: ["meta","tier_list"],
    desc: "Lihat tier list terbaru genshin impact",
    type: "genshin",
    example: `!meta`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("check", m)
        try {
            let tmpImg = await fetchBuffer(global.api("gif_canvas", `/tier_list`, { query: text }, "apikey"))
            let resPesan = {
                image: tmpImg,
                caption: `Tier List Genshin Impact (diolah dari GIF - Leaderboard)`
            }
            return await paimon.sendMessage(m.from, resPesan, { quoted: m })
        } catch (e) {
            console.log(e)
            return m.reply("Waduh!! Tier listnya lagi dipinjem Noelle buat ujian masuk KOF T.T")
        }
    }
}