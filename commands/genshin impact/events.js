const { fetchUrl, fetchBuffer, capitalize } = require("../../lib/Function")
const { sheetDB } = require("../../lib/sheetDB")
const { generateListMenu } = require("../../lib/menu_maker")
const genshindb = require("genshin-db")

module.exports = {
    name: "Event Genshin",
    alias: ["event","events","timeline"],
    desc: "Lihat event genshin impact saat ini dan yang akan datang",
    type: "genshin",
    example: `!event`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("check", m)
        try {
            let timeline = await fetchUrl(global.api("gif_handler", "", { query: text }, "apikey"))
            if (!timeline.success)
                return reply(timeline.pesan);
            timeline = JSON.parse(JSON.stringify(timeline.data))
            // Filter data
            jawaban = `╭─「 *EVENT GENSHIN IMPACT* 」\n`
            jawaban += `│ 📢 Ohayooo... ini nih event Genshin Impact di versi kali ini.\n`
            jawaban += `│──────────────────\n`
            jawaban += `│ *## SEDANG BERLANGSUNG ##*\n`
            for (let event of timeline.current) {
                // Filter data
                event.name = event.name.replace("Bargain", "Shop");
                // End of Filter data
                jawaban += `│ ──────────────────\n`
                jawaban += `│ 🗒️ *Nama Event*: ${event.name}\n`
                jawaban += `│ 🕖 *Berakhir*: ${event.remaining}\n`
                if (event.description)
                    jawaban += `│ ℹ️ *Deskripsi*: ${event.description}\n`
                if (event.url)
                    jawaban += `│ 🔗 *Laman Web*: ${event.url}\n`
            }
            jawaban += `│──────────────────\n`
            jawaban += `│ *## AKAN DATANG ##*\n`
            for (let event of timeline.coming) {
                // Filter data
                event.name = event.name.replace("Bargain", "Shop");
                // End of Filter data
                jawaban += `│ ──────────────────\n`
                jawaban += `│ 🗂️ *Nama Event*: ${event.name}\n`
                jawaban += `│ 🕓 *Dimulai*: ${event.remaining}\n`
                if (event.description)
                    jawaban += `│ ℹ️ *Deskripsi*: ${event.description}\n`
                if (event.url)
                    jawaban += `│ 🔗 *Laman Web*: ${event.url}\n`
            }
            jawaban += `│──────────────────\n`
            jawaban += `│ Jangan sampai kelewatan ya.. 😊\n`
            jawaban += `╰──────────────────`
            return m.reply(jawaban);
        } catch (e) {
            console.log(e)
            return m.reply("Waduh!! buku event Paimon dibawa main sama Qiqi T.T")
        }
    }
}