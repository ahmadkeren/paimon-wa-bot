const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Today Farming",
    alias: ["farm","farming","jadwal_farming"],
    desc: "Lihat karakter & senjata yang dapat difarming hari ini",
    type: "genshin",
    example: `!farm`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        global.mess("check", m)
        try {
            let imgChars = await fetchBuffer(global.api("gif_canvas", `/farm_characters/sekarang`, { query: text }, "apikey"))
            let resPesan = {
                image: imgChars,
                caption: `Daftar karakter dengan talent yang dapat difarming hari ini`
            }
            await paimon.sendMessage(m.from, resPesan, { quoted: m })
            let imgWeapons = await fetchBuffer(global.api("gif_canvas", `/farm_weapons/sekarang`, { query: text }, "apikey"))
            let resPesanTwo = {
                image: imgWeapons,
                caption: `Daftar senjata dengan material ascend yang dapat difarming hari ini`
            }
            await paimon.sendMessage(m.from, resPesanTwo, { quoted: m })
        } catch (e) {
            console.log(e)
            return m.reply("Waduh!! papan farming Paimon lagi dipinjam Noelle buat ujian masuk KOF T.T")
        }
    }
}