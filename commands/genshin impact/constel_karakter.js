const { fetchUrl, fetchBuffer, capitalize } = require("../../lib/Function")
const { sheetDB } = require("../../lib/sheetDB")
const { generateListMenu } = require("../../lib/menu_maker")
const genshindb = require("genshin-db")

module.exports = {
    name: "Konsetalasi Karakter",
    alias: ["constel","constels","konsetel","konstelasi"],
    desc: "Lihat constelasi karkater genshin impact",
    type: "genshin",
    example: `!constal amber`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        // console.log(m)
        if(args[0]) {
            // Tampilkan constel karakter
            try {
                target_pencarian = text
                target_pencarian = target_pencarian.toLowerCase()
                // Search by name
                data_master = await genshindb.constellations(target_pencarian)
                if (data_master == null)
                    return m.reply('Oops! paimon tidak punya data tersebut :")')
                // console.log(data_master)
                try {
                    jawaban = `╭─「 *KONSTELASI KARAKTER* 」\n`
                    jawaban += `│• *Nama Karakter:* ${data_master.name}\n`
                    try {
                        for (let key in data_master) {
                            data_constel = data_master[key]
                            if (key == "name" || key=="images" || key=="version") continue
                            jawaban += `│• *${capitalize(key)}:*\n`
                            jawaban += `│  - Nama: ${data_constel.name}\n`
                            jawaban += `│  - Effect: ${data_constel.effect.split("**").join("*")}\n`
                        }
                    } catch (e) {
                        console.log(e)
                    }
                    jawaban += `╰─────────────────────`
                    m.reply(jawaban) // lom ada preview gambar
                } catch (e) {
                    console.log(e)
                }
            } catch (e) {
                console.log(e)
                return m.reply("Gomen.. buku paimon dicuri swiper T.T")
            }
            // End Tampilkan constel karakter
        } else {
            // Tampilkan seluruh karakter genshin impact
            let txt_balasan = "Untuk melihat Konstelasi karakter silakan ketik *!constel <nama_karakter>* atau pilih dari daftar karakter dibawah!"
            title = "Daftar Karakter"
            cmd = "constel"
            desc = "Lihat detail konsetalasi dari karakter"
            list = [
                {
                    title: title,
                    rows: genshindb.characters('names', { matchCategories: true })
                }
            ]
            balasan = await generateListMenu(title,txt_balasan,cmd,desc,list)
            return await paimon.sendMessage(m.from, balasan, {quoted: m})
        }
    }
}