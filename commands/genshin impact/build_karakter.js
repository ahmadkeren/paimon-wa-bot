const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { sheetDB } = require("../../lib/sheetDB")
const { generateListMenu } = require("../../lib/menu_maker")
const genshindb = require("genshin-db")

module.exports = {
    name: "Build Karakter",
    alias: ["build","builds"],
    desc: "Lihat build karakter genshin impact",
    type: "genshin",
    example: `!build amber`,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        // console.log(m)
        if(args[0]) {
            // Tampilkan build karakter
            target_pencarian = text
            global.mess("check", m)
            n = await sheetDB("1kXRacThMnJ6-ohZP3HnzrhOlyDBPfl5yBrYqhRMQVeA", 3, target_pencarian)
            // console.log(n)
            // Optimalisasi hasil gambar
            if (n.length < 1)
                return m.reply("Gomen.. paimon gak punya build karakter tersebut T.T")
            if (n.length > 1) {
                let jawaban = `Paimon menemukan ${n.length} data build karakter dengan kata kunci *${target_pencarian}*:\n`
                let tmpRes = []
                for (let char of n) {
                    tmpRes.push(char.name)
                    let detail = await genshindb.characters(char.name)
                    if(detail==null) {
                        jawaban += `- ${char.name}\n`
                    } else {
                        jawaban += `- ${detail.name} (${detail.rarity}★ / ${detail.weapontype} / ${detail.substat})\n`
                    }
                }
                jawaban+= `Silahkan pilih salah satu dari daftar karakter tersebut melalui menu dibawah.`
                // Generate list
                title = "Hasil Pencarian"
                cmd = "build"
                desc = "Lihat build karakter"
                list = [
                    {
                        title: `${title}: ${target_pencarian}`,
                        rows: tmpRes,
                    }
                ]
                balasan = await generateListMenu(title,jawaban,cmd,desc,list)
                return await paimon.sendMessage(m.from, balasan, {quoted: m})
            } else {
                karakter = n[0]
                if (karakter.canvas_name == 0 || karakter.canvas_name == "" || karakter.canvas_name == undefined)
                    return m.reply('Ehe! paimon masih belajar build karakter itu :")')
                try {
                    let thumbIMG = await fetchBuffer(global.api("gif_canvas", `/build/${karakter.name}`, { query: text }, "apikey"))
                    let resPesan = {
                        image: thumbIMG,
                        caption: `Build karakter *${karakter.name}*\n*) _Lets make your own build :)_ `
                    }
                    return await paimon.sendMessage(m.from, resPesan, { quoted: m })
                } catch (e) {
                    console.log(e)
                    return m.reply('Ehe! build karakter tersebut dicuri swiper :")')
                    try {
                        jawaban = `Gomen, paimon lagi sibuk..\nUntuk melihat build karakter ${karakter.name}, traveler dapat mengakses laman: ${karakter["link_build"]}\nMakacih 😇`
                        return m.reply(jawaban)
                    } catch (e) {
                        console.log(e)
                        return m.reply('Ehe! build karakter tersebut dicuri swiper :")')
                    }
                }

            }
            // End Tampilkan build karakter
        } else {
            // Tampilkan seluruh karakter genshin impact
            let txt_balasan = "Untuk melihat build karakter silakan ketik *!build <nama_karakter>* atau pilih dari daftar karakter dibawah!"
            title = "Daftar Karakter"
            cmd = "build"
            desc = "Lihat build untuk karakter"
            list = [
                {
                    title: title,
                    rows: genshindb.characters('names', { matchCategories: true })
                }
            ]
            balasan = await generateListMenu(title,txt_balasan,cmd,desc,list)
            return await paimon.sendMessage(m.from, balasan, {quoted: m})
        }
    }
}