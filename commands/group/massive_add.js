const { fetchUrl, fetchBuffer } = require("../../lib/Function")

const membersGroup = [] // tinggal layover

module.exports = {
    name: "Tambah Member secara Masal",
    alias: ["massive_add"],
    desc: "Tambah banyak nomor hp ke group.",
    type: "group",
    example: `!massive_add`,
    isBotAdmin: true,
    isGroup: true,
    isAdmin: true,
    isOwner: true,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        try {
            for(let member of membersGroup) {
                let tambah_member = await paimon.groupParticipantsUpdate(
                    m.from,
                    [member],
                    "add"
                )
            }
        } catch(e) {
            console.log(e)
            return m.reply(`Gagal menambahkan ${text}`)
        }
    }
}