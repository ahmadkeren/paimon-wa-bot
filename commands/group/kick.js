const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Kick Member",
    alias: ["kick","remove","wisuda"],
    desc: "Kick nomor hp dari group.",
    type: "group",
    example: `!kick @tag`,
    isBotAdmin: true,
    isGroup: true,
    isAdmin: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        if(m.mentions.length < 1)
            return m.reply(`Silahkan tag traveler yang ingin diwisuda :")`)
        if(m.mentions.length > 1)
            return m.reply(`Wisudanya satu-satu ya ^_^`)
        let target_wisuda = m.mentions[0]
        try {
            await m.reply(`Siap!! wisuda *${target_wisuda}* segera dilaksanakan`)
            setTimeout(() => {
                paimon.groupParticipantsUpdate(
                    m.from,
                    [target_wisuda],
                    "remove"
                )
            }, 1100)
            return
        } catch(e) {
            console.log(e)
            return m.reply(`Gagal mengeluarkan ${text}`)
        }
    }
}