const { fetchUrl, fetchBuffer } = require("../../lib/Function")
const { addAfkUser, checkAfkUser, delAfkUser } = require("../../lib/afk")

module.exports = {
    name: "Sistem AFK",
    alias: ["afk"],
    desc: "Tambah nomor hp ke daftar AFK",
    type: "group",
    example: `!afk pergi ke toilet`,
    isQuery: true,
    exec: async(paimon, m, { commands, args, prefix, text, toUpper, sender }) => {
        let alreadyAFK = await checkAfkUser(m.sender);
        if(alreadyAFK.status) {
            // sedang afk
            let hapus = await delAfkUser(m.sender)
            let tambah = await addAfkUser(m.sender, text)
            return m.reply(`Berhasil menetapkan AFK dari yang sebelumnya *${alreadyAFK.reason}* menjadi *${text}*`)
        } else {
            // akan afk (tambah ke database afk)
            let tambah_afk = await addAfkUser(m.sender, text)
            let jawaban = ``
            jawaban += `Siap!! Kamu telah menetapkan AFK dengan keterangan *${text}* ^_^`
            return m.reply(jawaban)
        }
    }
}