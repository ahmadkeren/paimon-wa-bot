const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Demote Admin",
    alias: ["demote"],
    desc: "Turunkan admin tertentu menjadi member",
    type: "group",
    example: `!demote @tag`,
    isBotAdmin: true,
    isGroup: true,
    isAdmin: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        if(m.mentions.length < 0)
            return m.reply(`Silahkan tag traveler yang ingin didemote :")`)
        if(m.mentions.length > 1)
            return m.reply(`Demotenya satu-satu ya ^_^`)
        let target_tag = m.mentions[0]
        try {
            await m.reply(`Siap!! ${target_tag.split("@")[0]} segera didemote dari Admin`)
            setTimeout(() => {
                paimon.groupParticipantsUpdate(
                    m.from,
                    [target_tag],
                    "demote"
                )
            }, 1100)
            return
        } catch(e) {
            console.log(e)
            return m.reply(`Gagal menjadikan admin *${text}* sebagai member`)
        }
    }
}