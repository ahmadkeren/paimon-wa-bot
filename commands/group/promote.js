const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Promote Admin",
    alias: ["promote"],
    desc: "Jadikan nomor tertentu menjadi admin",
    type: "group",
    example: `!promote @tag`,
    isBotAdmin: true,
    isGroup: true,
    isAdmin: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        if(m.mentions.length < 0)
            return m.reply(`Silahkan tag traveler yang ingin dipromote :")`)
        if(m.mentions.length > 1)
            return m.reply(`Promotenya satu-satu ya ^_^`)
        let target_tag = m.mentions[0]
        try {
            await m.reply(`Siap!! ${target_tag.split("@")[0]} segera dipromote menjadi Admin`)
            setTimeout(() => {
                paimon.groupParticipantsUpdate(
                    m.from,
                    [target_tag],
                    "promote"
                )
            }, 1100)
            return
        } catch(e) {
            console.log(e)
            return m.reply(`Gagal menjadikan *${text}* sebagai admin`)
        }
    }
}