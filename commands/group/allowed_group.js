const fs = require("fs")
const { arrayRemove } = require("../../lib/Function")
const { updateFireData } = require("./../../controller/firebase/fireController")
const id_collection = 'registered_groups'

module.exports = {
    name: "Allowed Group",
    alias: ["system","sistem"],
    desc: "Aktifkan paimon pada group tertentu",
    type: "hide",
    example: `!system [on/off]`,
    isOwner: true,
    isGroup: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { text, isPaimonActive }) => {
    	let toggle = text.toLowerCase()
    	try {
    		switch(toggle) {
    			// Aktifkan gif system
	        	case "on":
                    if(isPaimonActive)
                        return m.reply('Paimon sudah diaktifkan di group ini ^_^')
	        		global.registered_groups.push(m.from)
	        		// fs.writeFileSync('./database/registered_groups.json', JSON.stringify(registered_groups))
                    writeDB(global.registered_groups)
	        		return m.reply('Yey, Paimon berhasil diaktifkan di group ini ^_^')
	        		break;
	        	// Nonaktif gif system
	        	case "off":
                    if(!isPaimonActive)
                        return m.reply('Paimon belum diaktifkan di group ini ^_^')
                    global.registered_groups = arrayRemove(global.registered_groups, m.from)
	        		// fs.writeFileSync('./database/registered_groups.json', JSON.stringify(registered_groups))
                    writeDB(global.registered_groups)
	        		return m.reply('Yey, Paimon berhasil dinonaktifkan di group ini ^_^')
	        		break;
	        	default:
	        		return m.reply('Parameter hanya menerima [on/off]!')
	        		break;
	        }
    	} catch(e) {
    		// console.log(e)
    		return m.reply("Terjadi kesalahan: "+e)
    	}
    }
}

const writeDB = async (data_baru) => {
    updateFireData(id_collection, data_baru)
}