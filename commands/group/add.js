const { fetchUrl, fetchBuffer } = require("../../lib/Function")

module.exports = {
    name: "Tambah Member",
    alias: ["add","invit","invite"],
    desc: "Tambah nomor hp ke group.",
    type: "group",
    example: `!add <hp> (tanpa tanda <> dan diawali dengan 62)`,
    isBotAdmin: true,
    isGroup: true,
    isAdmin: true,
    isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper }) => {
        try {
            if(text.length > 15)
                return m.reply(`Nambahinnya satu-satu ya traveler ^_^`)
            let calon_member = text+"@s.whatsapp.net";
            console.log(calon_member)
            let tambah_member = await paimon.groupParticipantsUpdate(
                m.from,
                [calon_member],
                "add"
            )
            console.log(tambah_member)
        } catch(e) {
            console.log(e)
            return m.reply(`Gagal menambahkan ${text}`)
        }
    }
}