module.exports = {
    name: "Tag Seluruh Member Group",
    alias: ["tagall","tag_all","mention_all"],
    desc: "Tag seluruh member group yang ada",
    type: "group",
    example: `!tagall`,
    isGroup: true,
    isAdmin: true,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper, participants }) => {
        try {
            members_id = []
            let res = ``
            res += `╔═══ Ditag oleh *${m.pushName}* ══\n`
            for (let mem of participants) {
                res += `╠➥ @${mem.id.split('@')[0]}\n`
                members_id.push(mem.id)
            }
            res += `╚═══〘 PAIMON 〙═══`
            await paimon.sendMessage(m.from, { text: res, mentions: members_id }, { quoted: m })
        } catch(e) {
            console.log(e)
            return m.reply(`Gagal melakukan tag ke seluruh member group`)
        }
    }
}