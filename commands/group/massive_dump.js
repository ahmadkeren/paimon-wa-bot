module.exports = {
    name: "Dump Seluruh Member Group",
    alias: ["massive_dump"],
    desc: "Dump nomor hp seluruh member group yang ada",
    type: "group",
    example: `!massive_dump`,
    isGroup: true,
    isAdmin: true,
    isOwner: true,
    // isQuery: true, // pakai jika ingin auto pake fitur example
    exec: async(paimon, m, { commands, args, prefix, text, toUpper, participants }) => {
        try {
            members_id = []
            let res = ``
            for (let mem of participants) {
                members_id.push(mem.id)
            }
            console.log(members_id)
            return m.reply(`Berhasil melakukan dump seluruh member group`)
        } catch(e) {
            console.log(e)
            return m.reply(`Gagal melakukan dump seluruh member group`)
        }
    }
}