const { updateFireData } = require("./../../controller/firebase/fireController")
const id_collection = 'db_custom'

module.exports = {
    name: "delcmd",
    alias: ["deletecmd","deletecommand"],
    desc: "Hapus sticker command dari database",
    type: "database",
    example: "Quote atau reply sticker dengan format: %prefix%command <nama_fungsi>",
    isOwner: true,
    exec: async(paimon, m, { quoted, prefix }) => {
        let hash = quoted.msg.fileSha256.toString("hex")
        if (!hash) return m.reply("Hash tidak ditemukan")
        if (!(hash in global.db.sticker))
            return m.reply(`Hash tidak ditemukan didalam database, untuk melihat daftar media command silahkan akses ${prefix}listcmd`)
        if (global.db.sticker[hash] && global.db.sticker[hash].locked)
            return m.reply('Kamu tidak memilik akses untuk mengubah sticker command ini')
        delete global.db.sticker[hash]
        m.reply("Berhasil menghapus command")
        return await writeDB()
    }
}

const writeDB = async (data_baru = global.db) => {
    updateFireData(id_collection, data_baru)
}