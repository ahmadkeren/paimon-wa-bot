const { updateFireData } = require("./../../controller/firebase/fireController")
const id_collection = 'db_custom'

module.exports = {
    name: "delmsg",
    alias: ['vn', 'msg', 'video', 'gif', 'audio', 'img', 'sticker'].map(v => 'del' + v),
    desc: "Hapus template pesan dari database",
    type: "database",
    example: "Kirim pesan dengan format: %prefix%command <nama_pesan>",
    isOwner: true,
    exec: async(paimon, m, { prefix, command, text }) => {
        let which = command.replace(/get/i, '')
        let database = global.db.database
        if (!(text.toLowerCase() in database))
            return m.reply(`"${text}" tidak tersimpan didalam database`)
        delete database[text.toLowerCase()]
        m.reply(`Berhasil menghapus "${text}" dari database`)
        return await writeDB()
    },
    isQuery: true
}

const writeDB = async (data_baru = global.db) => {
    updateFireData(id_collection, data_baru)
}