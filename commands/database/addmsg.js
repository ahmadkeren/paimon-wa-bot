const { proto } = require("@adiwajshing/baileys")
const { updateFireData } = require("./../../controller/firebase/fireController")
const id_collection = 'db_custom'

module.exports = {
    name: "addmsg",
    alias: ['vn', 'msg', 'video', 'gif', 'audio', 'img', 'sticker'].map(v => 'add' + v),
    desc: "Tambah template pesan ke database",
    type: "database",
    example: "Quote atau reply pesan dengan format: %prefix%command <nama_pesan>",
    isOwner: true,
    exec: async(paimon, m, { prefix, command, text, quoted }) => {
        if(!m.quoted || m.quoted == null)
            return m.reply(`Silahkan quote pesannya traveler ^_^`)
        let M = proto.WebMessageInfo
        let which = command.replace(/\+|add/i, '')
        let database = global.db.database
        if (text.toLowerCase() in database)
            return m.reply(`"${text}" telah tersimpan didalam database`)
        database[text.toLowerCase()] = M.fromObject(m.quoted ? quoted.fakeObj : m).toJSON()
        let jawaban = ``
        jawaban += `Berhasil menambahkan template pesan sebagai "${text}"\n`
        jawaban += `Akses pesan dengan perintah ${prefix}get${which} ${text}\n`
        jawaban += `Untuk melihat daftar pesan silahkan akses ${prefix}list${which}`
        m.reply(jawaban)
        return await writeDB()
    },
    isQuery: true
}

const writeDB = async (data_baru = global.db) => {
    updateFireData(id_collection, data_baru)
}