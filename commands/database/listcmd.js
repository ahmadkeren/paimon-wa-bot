module.exports = {
    name: "listcmd",
    alias: ["listcommand"],
    desc: "Lihat daftar sticker command dari database",
    type: "database",
    exec: async(paimon, m, { quoted }) => {
        let text = ``
        text += `=== *DAFTAR STICKER COMMAND* ===\n`
        Object.entries(global.db.sticker).map(([key, value], index) => {
        	text += `\n`
	        text += `*Perintah* : ${value.locked ? `*${value.text}*` : value.text}\n`
	        text += `*Pembuat*: @${value.creator.split("@")[0]}\n`
	        text += `*Dibuat pada* : ${new Date(value.createAt)}\n`
        })
        text += `\n`
        text += `Catatan: *bold* pada nama perintah berarti perintah telah dikunci\n`
        /*
        text += `${Object.entries(global.db.sticker).map(([key, value], index) => `${index + 1}.\n`
        
        */
        paimon.sendMessage(m.from, { text, mentions: paimon.parseMention(text) }, { quoted: m })
    }
}