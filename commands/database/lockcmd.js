const { updateFireData } = require("./../../controller/firebase/fireController")
const id_collection = 'db_custom'

module.exports = {
    name: "lockcmd",
    alias: ["lockedcmd"],
    desc: "Kunci sticker command didalam database",
    type: "database",
    example: "Reply atau quote sticker dengan perintah %prefix%command",
    isOwner: true,
    exec: async(paimon, m, { quoted, command }) => {
        if (!quoted.msg.fileSha256)
            return m.reply("Sha256 tidak ditemukan!")
        let hash = quoted.msg.fileSha256.toString("hex")
        if (!(hash in global.db.sticker))
            return m.reply(`Hash tidak ditemukan didalam database, untuk melihat daftar sticker command silahkan akses ${prefix}listcmd`)
        global.db.sticker[hash].locked = !/^un/i.test(command)
        m.reply("Berhasil mengunci sticker command didalam database")
        return await writeDB()
    },
    isMedia: true
}

const writeDB = async (data_baru = global.db) => {
    updateFireData(id_collection, data_baru)
}