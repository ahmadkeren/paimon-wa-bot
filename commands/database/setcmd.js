const { updateFireData } = require("./../../controller/firebase/fireController")
const id_collection = 'db_custom'

module.exports = {
    name: "setcmd",
    alias: ["addcmd","addcommand"],
    desc: "Tambah sticker command kedalam database",
    type: "database",
    example: "Reply sticker dengan perintah %prefix%command <nama_fungsi>",
    isOwner: true,
    exec: async(paimon, m, { quoted, text }) => {
        if (!quoted.msg.fileSha256)
            return m.reply('Sha256 tidak ditemukan!')
        let hash = quoted.msg.fileSha256.toString("hex")
        if (global.db.sticker[hash] && global.db.sticker[hash].locked)
            return m.reply('Kamu tidak memiliki izin untuk mengubah perintah pada command pada sticker ini')
        global.db.sticker[hash] = {
            text,
            mentions: quoted.mentions,
            creator: m.sender,
            createAt: + new Date,
            locked: false
        }
        m.reply("Berhasil menambahkan command kedalam sticker")
        return await writeDB()
    },
    isMedia: true,
    isQuery: true
}

const writeDB = async (data_baru = global.db) => {
    updateFireData(id_collection, data_baru)
}