module.exports = {
    name: "getmsg",
    alias: ['vn', 'msg', 'video', 'gif', 'audio', 'img', 'sticker'].map(v => 'get' + v),
    desc: "Ambil template pesan dari database",
    type: "database",
    example: "Kirim pesan dengan format: %prefix%command <nama_pesan>",
    exec: async(paimon, m, { prefix, command, quoted, text }) => {
        let which = command.replace(/get/i, '')
        let database = global.db.database
        if(!(text.toLowerCase() in database))
            return m.reply(`"${text}" tidak tersimpan didalam database`)
        let _m = database[text.toLowerCase()]
        paimon.relayMessage(m.from, _m.message, { messageId: _m.id })
    },
    isQuery: true
}