const nameCollection = "database_wa"
const firebase_db = require("../../lib/firebase_db.js")

const db = firebase_db.fireDB
const fireFetch = firebase_db.fetchData

// Main Core
exports.getAllFireData = async (collectionName = nameCollection) => {
    let data = []
    let query = await db.collection(collectionName).get()
    if (query.docs.length > 0) {
        for (const item of query.docs) {
            let id = item.id
            let dt = item.data()
            data.push({
                id: id,
                data: dt.data
            })
        }
    }
    return data
}

exports.getFireData = async (id_collection) => {
    let query = await db.collection(nameCollection).doc(id_collection).get()
    let data =  query.data()
    if(data == undefined)
    	return []
    else
    	return data.data
}


exports.updateFireData = async (id_collection, data) => {
	let update = await db.collection(nameCollection).doc(id_collection).update({
		"data": data
	});
	return true
}

// Local System

exports.getLocalFireData = async (fireData, id) => {
    let data = []
    for(let dt of fireData) {
        if(dt.id == id) {
            data = dt.data
            break;
        }
    }
    return data
}


// Realtime Listener
exports.getFireDataRealtime = async (id_collection) => {
    const listener = await db.collection(nameCollection).doc(id_collection)
    return listener
}

// Realtime Listener
exports.getRealtimeDB = async (id_collection) => {
    const listener = await db.collection(nameCollection)
    return listener
}