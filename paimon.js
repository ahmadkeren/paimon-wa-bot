require("./config")
const { generateWAMessage, areJidsSameUser, proto } = require("@adiwajshing/baileys")
const { Simple, Collection, Function } = require("./lib")
const { isUrl, isNumber } = Function
const Func = require("./lib")
const fs = require("fs")
const moment = require("moment-timezone")
const chalk = require("chalk")
const util = require("util")
const { correct } = require("./lib/Correct")
const { checkBadword, handleToxicer } = require("./lib/badwords")
const { checkNewGifMember, handleNewGifMember } = require("./lib/gif_auto_register")
const { checkPaimonAR, handlePaimonAR } = require("./lib/paimon_autoreply")
const { paimonAiEngine, executeCMD } = require("./lib/paimon_ai")
const { tanyaKePaimon } = require("./lib/paimon_chat")
const { pickArray, secondsToHms } = require("./lib/Function")
const { isLimitAccess, handleLimitAccess } = require("./lib/handle_limit")
const { updateFireData } = require("./controller/firebase/fireController")
const { delAfkUser, checkAfkUser } = require("./lib/afk")
const { sedangAFK } = require("./lib/template")
const { checkContact, addContact, updateContact } = require("./lib/realtime_contacts")

// global.db = JSON.parse(fs.readFileSync("./tmp/database.json"))
if (global.db) global.db = {
    sticker: {},
    database: {},
    chats: {},
    ...(global.db || {})
}

// Database get data

module.exports = async (paimon, m, commands, chatUpdate) => {
    try {
        let { type, isGroup, sender, from } = m
        let body = (type == "buttonsResponseMessage") ? m.message[type].selectedButtonId : (type == "listResponseMessage") ? m.message[type].singleSelectReply.selectedRowId : (type == "templateButtonReplyMessage") ? m.message[type].selectedId : m.text 
        let metadata = isGroup ? await paimon.groupMetadata(from) : {}
        let pushname = isGroup ? metadata.subject : m.pushName
        let participants = isGroup ? metadata.participants : [sender]
        let groupAdmin = isGroup ? participants.filter(v => v.admin !== null).map(v => v.id) : []
        let isBotAdmin = isGroup ? groupAdmin.includes(paimon.user?.jid) : false
        let isAdmin = isGroup ? groupAdmin.includes(sender) : false
        let isOwner = [paimon.user?.jid, ...global.owner].map(v => v.replace(/[^0-9]/g, '') + '@s.whatsapp.net').includes(sender)

        // let gif_guilds = await getFireData('gif_guilds')
        let isGIFGuild = isGroup ? global.gif_guilds.includes(from) : false
        // console.log("Is GIF Official Guilds? "+isGIFGuild)

        let isPaimonActive = isGroup ? global.registered_groups.includes(from) : false
        // console.log(isPaimonActive)

        let isAnnMode = global.paimon_live_chat.includes(from)
        // console.log(isPaimonActive)

        let isBanned = global.users_banned.includes(sender)
        // console.log(isBanned)

        let isAfk = (await checkAfkUser(m.sender)).status // Cek apakah saya afk sebelumnya
        // console.log(isAfk)

        // if (options.autoRead) (paimon.type == "legacy") ? await paimon.chatRead(m.key, 1) : await paimon.sendReadReceipt(from, sender, [m.id])
        if (options.mute && !isOwner) return
        if (options.self && !isOwner && !m.fromMe) return

        // var prefix = /^[°•π÷×¶∆£¢€¥®™+✓_=|~!?#%^&.©^]/gi.test(body) ? body.match(/^[°•π÷×¶∆£¢€¥®™+✓_=|~!?#%^&.©^]/gi)[0] : Function.checkPrefix(prefa, body).prefix ?? "#"
        var prefix = /^[!.]/gi.test(body) ? body.match(/^[!.]/gi)[0] : Function.checkPrefix(prefa, body).prefix ?? ""

        let quoted = m.quoted ? m.quoted : m

        let mime = null
        try {
            mime = (quoted.msg || m.msg).mimetype
        } catch(e) {
            console.log("Mime type tidak terdeteksi")
            return m.reply(pickArray([
                "Hayo.. apa itu???",
                "Paimon nyimak...",
                "....",
                "Hoooooooo",
                "Apa tu yang sekali lihat???",
            ]))
        }

        let isMedia = /image|video|sticker|audio/.test(mime)
        let budy = (typeof m.text == "string" ? m.text : "")
        let args = body.trim().split(/ +/).slice(1)
        let text = q = args.join(" ")
        let cmdName = body.replace(prefix, '').trim().split(/ +/).shift().toLowerCase()
        const cmd = commands.get(cmdName) || Array.from(commands.values()).find((v) => v.alias.find((x) => x.toLowerCase() == cmdName)) || ""
        
        if (m.message) {
            console.log(chalk.black(chalk.bgWhite('[ PESAN ]')), chalk.black(chalk.bgGreen(new Date)), chalk.black(chalk.bgGreen(body || type)) + "\n" + chalk.black(chalk.bgWhite("=> Dari")), chalk.black(chalk.bgGreen(m.pushName)), chalk.black(chalk.yellow(sender)) + "\n" + chalk.black(chalk.bgWhite("=> Di")), chalk.bgGreen(isGroup ? metadata.subject : m.pushName, from))  
        }

        // Sticker Command
        if (isMedia && m.msg.fileSha256 && (m.msg.fileSha256.toString("hex") in global.db.sticker)) {
            let hash = global.db.sticker[m.msg.fileSha256.toString("hex")]
            let { text, mentions } = hash
            let messages = await generateWAMessage(m.from, { text, mentions }, {
                userJid: paimon.user.jid,
                quoted: m.quoted && m.quoted.fakeObj
            })
            messages.key.fromMe = await areJidsSameUser(m.sender, paimon.user.jid)
            messages.key.id = m.id
            messages.pushName = m.pushName
            if (isGroup) {
                messages.participant = m.sender
                messages.customSender = m.sender
            }
            let msg = {
                ...chatUpdate,
                messages: [proto.WebMessageInfo.fromObject(messages)],
                type: "append"
            }
            return await paimon.ev.emit("messages.upsert", msg)
        }

        // Handle patch sticker command
        if(m.customSender) m.sender = m.customSender

        // Database
        try {
            let chat = global.db.chats[m.from]
            if (typeof chat !== "object") global.db.chats = {}
            if (chat) {
                if (!('antidelete' in chat)) chat.antidelete = true
            } else global.db.chats[m.from] = {
                antidelete: true
            }
        } catch(e) {
            console.error(e)
        }

        // Realtime contact updater
        try {
            let contact = {
                id: m.from,
                name: null,
                type: null,
                photo: await paimon.profilePictureUrl(m.key.remoteJid),
                custom_name: null, // parameter desktop
                tags: [], // parameter desktop
            }
            if(isGroup) {
                contact.type = "group"
                contact.name = metadata.subject
            } else {
                contact.type = "personal"
                contact.name = pushname
            }
            let cekKontak = await checkContact(contact)
            if(!cekKontak.exist) {
                let getGroups = await paimon.groupFetchAllParticipating();
                let groups = Object.entries(getGroups).slice(0).map(entry => entry[1])
                let tags = []
                for(let group of groups) {
                    let participants = group.participants
                    for(let participant of participants) {
                        if(participant.id == contact.id) {
                            tags.push(group.subject)
                            break;
                        }
                    }
                }
                contact.tags = tags
                addContact(contact)
            } else {
                if(cekKontak.diff) {
                    updateContact(contact)
                }
            }
        } catch(e) {
            console.log(e)
        }


        // - Write
        let intervalUpdate = false // FALSE: sekarang update otomatis jika ada perubahan
        if(intervalUpdate) {
            let update_db_time_minute = 30 // menit
            let update_db_time_sec = update_db_time_minute*60 // detik
            setInterval(() => {
                // Update ke database dilakukan setiap 30 menit sekali
                // fs.writeFileSync('./tmp/database.json', JSON.stringify(global.db, null, 2))
                updateFireData('db_custom', global.db)
            }, update_db_time_sec * 1000)
        }
        

        // Anti Delete
        if (m.message && m.message.protocolMessage && m.message.protocolMessage.type == 0) {
            if (!db.chats[m.from].antidelete) return
            let key = m.message.protocolMessage.key
            let msg = await paimon.serializeM(await Store.loadMessage(key.remoteJid, key.id))
            let teks = `「 Mendeteksi Pesan Terhapus 」\n`
            teks += `⬡ Nama : ${msg.pushName}\n`
            teks += `⬡ User : @${msg.sender.split("@")[0]}\n`
            teks += `⬡ Waktu : ${moment(msg.messageTimestamp * 1000).tz("Asia/Jakarta")}\n`
            teks += `⬡ Jenis Pesan : ${msg.type}`
            paimon.sendText(m.from, teks, msg, { mentions: [msg.sender] })
            paimon.relayMessage(m.from, msg.message, { messageId: msg.id })
        }

        // Custom smart filter text
        if(m.message) {
            // console.log(m)
            if(m.message.conversation) {
                let chat_user = m.message.conversation.toLowerCase()
                chat_user = chat_user.split("-").join("")
                // console.log(chat_user)

                // Start of Anti Badword Function | Anti kata-kata kasar / dsb
                if(isGroup && isBotAdmin) {
                    if(checkBadword(chat_user)) {
                        return await handleToxicer(paimon, m, isAdmin)
                    }
                }
                // End of Anti Badword Function

                // Smart Auto Register GIF
                if(isGroup && isGIFGuild) {
                    if(checkNewGifMember(chat_user)) {
                        console.log("Mendeteksi perkenalan diri...")
                        return await handleNewGifMember(paimon, m, chat_user)
                    }
                }

                // Auto Reply Paimon
                let checkAutoReply = await checkPaimonAR(paimon, m, isGroup, chat_user)
                if(checkAutoReply.cekal)
                    return await handlePaimonAR(paimon, m, checkAutoReply)
            }
        }

        // Antisipasi single dot
        if(prefix.toString().trim() == body.trim()) return

        // Sistem deteksi typoo
        if (!cmd && prefix!= null && prefix!="") {
            if(isAnnMode)
                return await(tanyaKePaimon(m, body))
            // console.log("masuk nih bro.. prefixnya:>>"+prefix+"<<")
            let cmdName = body.replace(prefix, '').trim().split(/ +/).shift().toLowerCase()
            var array = Array.from(commands.keys());
            Array.from(commands.values()).map((v) => v.alias).join(" ").replace(/ +/gi, ",").split(",").map((v) => array.push(v))
            var anu = await correct(cmdName, array)
            var alias = commands.get(anu.result) || Array.from(commands.values()).find((v) => v.alias.find((x) => x.toLowerCase() == anu.result)) || ""
            let akurasi = anu.rating * 100
            let teks = ``
            // Bulatkan akurasi
            akurasi = Math.round(akurasi)
            // Auto correct
            if(akurasi >= 70) {
                await m.reply(`Perintah yang Anda maksud mungkin adalah ~${cmdName}~ *${anu.result}* (akurasi ${akurasi}%). Berikut merupakan hasil eksekusi perintah *${prefix}${anu.result}* (${alias.name}).`)
                return await executeCMD(paimon, m, anu.result, text, commands, body)
            } else
            // Prediksi command
            if(akurasi > 50) {
                teks += `Ehe! perintah *${body}* tidak ditemukan! T.T\n`
                teks += `Mungkin perintah yang dimaksud adalah:\n`
                teks += `- *_Perintah:_* ${prefix + anu.result}\n`
                teks += `- *_Alias :_* ${alias.alias.join(", ")}\n`
                teks += `- *_Akurasi :_* ${akurasi}%\n`
                teks += `_Jangan sungkan-sungkan kirim lagi ya Traveler_ ^_^`
            } else {
                return await paimonAiEngine(paimon, m, isOwner, text, commands, body)
                // teks += `Ehe! perintah *${body}* tidak terdaftar di dalam database *!menu*`
            }
            if(body.length > 1)
                return m.reply(teks)
        }

        if (isGroup) {
            // Sistem AFK
            if(isAfk) {
                let afkStatus = await checkAfkUser(m.sender)
                if(!afkStatus.status) return; // counter siapa tau ada masalah
                let afk_duration = Date.now() - afkStatus.time
                let resAFKDone = ``
                resAFKDone += `Yey!! ${m.sender.split('@s.whatsapp.net').join('')} kembali dari AFK *${afkStatus.reason}* selama *${secondsToHms(afk_duration/1000)}* ^_^`
                m.reply(resAFKDone)
                delAfkUser(m.sender) // hapus data afk dari db
            }
            if(m.mentions.length > 0) {
                for(let mention of m.mentions) {
                    let checkAfk = await checkAfkUser(mention)
                    if(checkAfk.status) {
                        // Sedang AFK
                        m.reply(sedangAFK(mention, checkAfk))
                    }
                }
            }
            if(m.quoted != null) {
                try {
                    let isQuotedAfk = await checkAfkUser(m.quoted.sender)
                    if(isQuotedAfk.status)
                        m.reply(sedangAFK(m.quoted.sender, isQuotedAfk))
                } catch(e) {
                    console.log(e)
                }
            }
            // End of Sistem AFK
        }

        if (!cmd) {
            // console.log("tidak mengandung command")
            return;
        }

        if (cmd) {
            skipPrefix = false
            if(m.msg) {
                // Respon pengganti prefix lewat select menu
                if(m.msg.singleSelectReply) {
                    if(m.msg.singleSelectReply.selectedRowId)
                        skipPrefix = true
                }
                // Respon pengganti prefix lewat button
                if(m.msg.selectedButtonId) skipPrefix = true
            }
            if((prefix == "" || prefix == undefined || prefix == null) && !skipPrefix) return;
        }

        if (cmd && isBanned && !isOwner) {
            return global.mess("banned",m)
        }

        if (prefix && !isOwner && !isPaimonActive && isGroup) return global.mess("terculik", m)

        if (prefix && options.maintenance && !isOwner) return global.mess("maintenance", m)

        if (cmd.isGifMenu && !isGIFGuild) {
            return global.mess("gif_only", m)
        }

        if (cmd.isMedia && !isMedia) {
            return global.mess("media", m)
        }

        if (cmd.isOwner && !isOwner) {
            return global.mess("owner", m)
        }

        if (cmd.isGroup && !isGroup) {
            return global.mess("group", m)
        }

        if (cmd.isPrivate && isGroup) {
            return global.mess("private", m)
        }

        if (cmd.isBotAdmin && !isBotAdmin) {
            return global.mess("botAdmin", m)
        }

        if (cmd.isAdmin && !isAdmin) {
            return global.mess("admin", m)
        }

        if (cmd.isBot && m.fromMe) {
            return global.mess("bot", m)
        }

        if (cmd.disable == true && cmd.disable == false) {
            return global.mess("dead", m)
        }

        if (cmd.desc && text.endsWith("--desc")) return m.reply(cmd.desc)
        if (cmd.example && text.endsWith("--use")) {
            return m.reply(`${cmd.example.replace(/%prefix/gi, prefix).replace(/%command/gi, cmd.name).replace(/%text/gi, text)}`)
        }

        if (cmd.isQuery && !text) {
            return m.reply(`${cmd.example.replace(/%prefix/gi, prefix).replace(/%command/gi, cmd.name).replace(/%text/gi, text)}`)
        }

        if (cmd.isLimit) {
            // Sistem limitasi akses menu
            try {
                let maxLimit = global.defMaxLimit
                if(cmd.maxLimit) maxLimit = cmd.maxLimit

                if(await isLimitAccess(paimon, m, cmd.name, cmd.maxLimit))
                    return global.mess("max_limit", m)
                else {
                    await handleLimitAccess(paimon, m, cmd.name, maxLimit)
                }
            } catch(e) {
                console.log("Sistem limit bermasalah, reason:")
                console.log(e)
            }
        }

        try {
            cmd.exec(paimon, m, {
                metadata,
                pushName: pushname,
                participants,
                body,
                args,
                text,
                quoted,
                mime,
                prefix,
                command: cmd.name,
                commands,
                Function: Func,
                isPaimonActive, // group terdaftar
                isAnnMode, // mode bicara langsung dengan paimon
                isGIFGuild, // gif official guilds
                toUpper: function toUpper(query) {
                    return query.replace(/^\w/, c => c.toUpperCase())
                }
            })
        } catch (e) {
            console.log(e)
            // paimon.sendMessage(from, { text: String(e) }, { quoted: m })
        }   
    } catch (e) {
        console.log(e)
        // paimon.sendMessage(m.from, { text: String(e) }, { quoted: m })
    }
}

global.reloadFile(__filename)
