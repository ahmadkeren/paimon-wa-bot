// Firebase Config
const adminFirebase = require("firebase-admin");

var serviceAccount = require("./../firebase_auth.json");
adminFirebase.initializeApp({
  credential: adminFirebase.credential.cert(serviceAccount),
  databaseURL: "https://gif-official-default-rtdb.firebaseio.com"
});

exports.adminDB = adminFirebase
exports.fireDB = adminFirebase.firestore()