const { updateFireData } = require("./../controller/firebase/fireController")
const id_collection = 'realtime_contact'

exports.checkContact = async (contact) => { // Cek apakah kontak sudah ada di db
    let res = {
        exist: false,
        diff: false,
    }
    for(let db of global.contacts) {
        if(db.id == contact.id) {
            res.exist = true
            if(
                db.name!=contact.name ||
                db.photo!=contact.photo
            ) {
                res.diff = true
            }
            break
        }
    }
    return res
}

exports.addContact = async (contact) => { // Tambahkan kontak ke db
    global.contacts.push(contact)
    writeDB(global.contacts)
}

exports.updateContact = async (contact) => { // Update kontak ke db
    let res = []
    for(let db of global.contacts) {
        if(db.id == contact.id) {
            let tmp = db
            tmp.name = contact.name
            tmp.photo = contact.photo
            res.push(tmp)
        } else {
            res.push(db)
        }
    }
    global.contacts = res
    writeDB(global.contacts)
}

const writeDB = async (data_baru) => {
    updateFireData(id_collection, data_baru)
}