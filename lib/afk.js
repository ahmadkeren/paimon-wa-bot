const { updateFireData } = require("./../controller/firebase/fireController")
const id_collection = 'afk_list'

const writeDB = async (data_baru = global.afk_list) => {
    updateFireData(id_collection, data_baru)
}

exports.addAfkUser = async (id_user, text) => {
    // cek doble afk (kadang kalo lagi lag bisa double)
    let alreadyAFK = await checkAfkUser(id_user)
    if(alreadyAFK.status) return
    // Tambahkan ke database
    const obj = {
        id: id_user,
        reason: text,
        time: Date.now(),
    }
    global.afk_list.push(obj)
    return await writeDB()
}

exports.delAfkUser = async (id_user) => {
    let filter = []
    for(let afk_user of global.afk_list) {
        if(afk_user.id != id_user) {
            filter.push(afk_user)
        }
    }
    global.afk_list = filter
    return await writeDB()
}

exports.checkAfkUser = checkAfkUser = async (id_user) => {
    let status = {
        reason: null,
        time: null,
        status: false,
    }
    for(let afk_user of global.afk_list) {
        if(afk_user.id == id_user) {
            status.reason = afk_user.reason
            status.time = afk_user.time
            status.status = true
            break;
        }
    }
    return status
}