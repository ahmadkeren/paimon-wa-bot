const { sheetDB } = require("./sheetDB")
const axios = require("axios")

const guild_list = [
    {
        name: "flourine", // gif utama
        id: 1,
        sdbKey: "yk91qk65b743u",
        sheet: null,
    },
    {
        name: "layover",
        id: 3,
        sdbKey: "m7gbla3lyeqpp",
        sheet: "GIF - Layover Village",
    },
    {
        name: "kakureta",
        id: 4,
        sdbKey: "m7gbla3lyeqpp",
        sheet: "GIF - Kakureta Village",
    },
    {
        name: "tanoshi",
        id: 5,
        sdbKey: "m7gbla3lyeqpp",
        sheet: "GIF - Tanoshi Village",
    }
];

exports.getGuildID = async (search) => {
    let result = guild_list[1] // set default: layover village
    let nama_group = search.toLowerCase()
    for(let guild of guild_list) {
        if(nama_group.includes(guild.name.toLowerCase())) {
            result = guild
            break
        }
    }
    return result;
}

exports.gifDBEngine = gifDBEngine = async (method, location, parameter, data_update) => {
    docID = "1kzQI5yD0cT1R2YyOIGjkEG6NHAz3ld3bBZ_xNwOszuQ"
    //eg: gifDBEngine(get,layover,uid_wa,false);
    switch (method.toLowerCase()) {
        case "get":
        case "search":
            location = location.toLowerCase();
            for(let guild of guild_list) {
                if(location == guild.name.toLowerCase()) {
                    anu = await sheetDB(docID, guild.id, parameter)
                    //tambah parameter
                    if(anu.length>0)
                        anu[0]["guild"] = guild.name
                    break   
                }
            }
            return anu;
            break

        case "patch":
        case "put":
        case "update":

        case "post":
        case "tambah":
            location = location.toLowerCase();
            for(let guild of guild_list) {
                if(location == guild.name.toLowerCase()) {
                    // Handle Query
                    let query = {
                        sheet: guild.sheet
                    }
                    if(guild.sheet == null)
                        query = {}
                    // Handle method
                    let methodVal = "PUT";
                    if(method == "post" || method=="tambah")
                        methodVal = "POST"
                    let endpoint = guild.sdbKey
                    if(methodVal=="PUT")
                        endpoint = endpoint+"/WhatsApp_ID/"+parameter
                    // Request to server
                    let res = await axios({
                        method: `${methodVal}`,
                        url: global.api("sheetdb", `/${endpoint}`, query, null),
                        headers: {
                            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36',
                            'Content-Type': 'application/json'
                        },
                        data : data_update
                    })
                    return res
                }
            }
            break
        default:
            return []
            break
    }
}

exports.cariTraveler = async (target_pencarian) => {
    let result = null
    for(let guild of guild_list) {
        let res = await gifDBEngine("get", guild.name, target_pencarian, false)
        if(res.length > 0) {
            result = res;
            break;
        }
    }
    return result;
}

exports.cookieDBEngine = cookieDBEngine = async function(method, parameter, data_update = null) {
    docID = "1zhOjQ77hOadD49meB28iQ3Dp6AKtCBYc9tBPanw0lFo"
    sdbKey = "um7dzmnqvq7p4"
    //eg: gifDBEngine(get,layover,uid_wa,false);
    switch (method.toLowerCase()) {
        case "get":
        case "search":
            anu = await sheetDB(docID, 1, parameter)
            return anu
            break

        case "patch":
        case "put":
        case "update":

        case "post":
        case "tambah":
            // Handle method
            let methodVal = "PUT";
            if(method == "post" || method=="tambah")
                methodVal = "POST"
            let endpoint = sdbKey
            if(methodVal=="PUT")
                endpoint = endpoint+"/uid/"+parameter
            // Request to server
            let res = await axios({
                method: `${methodVal}`,
                url: global.api("sheetdb", `/${endpoint}`, null, null),
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36',
                    'Content-Type': 'application/json'
                },
                data : data_update
            })
            return res
            break
        default:
            return []
            break
    }
}

exports.getDataFromCookie = getDataFromCookie = async function(myCookie) {
    cookieArray = myCookie.split(";")
    let myLtuid = ""
    let myLtoken = ""
    for (let fzz_str of cookieArray) {
        if (fzz_str.toLowerCase().includes("ltuid")) {
            new_fzz = fzz_str.split(" ").join("")
            data_fzz = new_fzz.split("=")
            data_taken = data_fzz[1]
            myLtuid = data_taken
        }
        if (fzz_str.toLowerCase().includes("ltoken")) {
            new_fzz = fzz_str.split(" ").join("")
            data_fzz = new_fzz.split("=")
            data_taken = data_fzz[1]
            myLtoken = data_taken
        }
    }
    res_data = [];
    res_data["ltuid"] = myLtuid
    res_data["ltoken"] = myLtoken
    return res_data
}

exports.getTravelerCookies = async(uid) => {
    let res = {
            ltuid : null,
            ltoken: null,
            reminder: "off",
        };
    try {
        let data = await cookieDBEngine("get", uid);
        if(data.length > 0) {
            let myCookie = data[0]["data_cookie"];
            let dataFromCookie = await getDataFromCookie(myCookie);
            let myLtuid = dataFromCookie["ltuid"];
            let myLtoken = dataFromCookie["ltoken"];
            res = {
                ltuid : myLtuid,
                ltoken: myLtoken,
                reminder: data[0]["reminder"],
            };
        }
    } catch(e) {
        console.log(e)
    }
    return res;
}