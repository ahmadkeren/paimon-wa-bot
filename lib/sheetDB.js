const { GoogleSpreadsheet } = require('google-spreadsheet');
const credentials = require('../client_secret.json');

exports.sheetDB = async (docID, sheetIndex = 1, parameter = "") => {
    doc = new GoogleSpreadsheet(docID);
    await doc.useServiceAccountAuth(credentials);
    await doc.loadInfo();
    const sheet = doc.sheetsByIndex[sheetIndex - 1];
    const rows = await sheet.getRows();
    // Filter pencarian
    res_data = [];
    try {
        parameter = parameter.toLowerCase()
    } catch (e) {
        try {
            parameter = parameter[0].toLowerCase()
        } catch (e) {
            console.log(e)
        }
    }
    for (let item of rows) {
        for (let raw_data of item._rawData) {
            if (
                raw_data.toLowerCase().includes(parameter) ||
                raw_data.toLowerCase() == parameter
            ) {
                res_data.push(item)
                break
            }
        }
    }
    return res_data;
}