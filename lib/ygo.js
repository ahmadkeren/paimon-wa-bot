const { numberWithCommas, arrToString } = require("./Function")

exports.templateMD = (kartu_terpilih) => {
    // console.log(kartu_terpilih)
    jawaban = `╭─「 *HASIL PENCARIAN KARTU* 」\n`
    try {
        jawaban += `│• *Nama:* ${kartu_terpilih.name}\n`
    } catch (e) {}
    // try {
    //     if (kartu_terpilih.archetype)
    //         jawaban += `│• *Archetype:* ${kartu_terpilih.archetype}\n`
    // } catch (e) {}
    try {
        if (kartu_terpilih.type)
            jawaban += `│• *Tipe:* ${kartu_terpilih.type}\n`
    } catch (e) {}
    try {
        if (kartu_terpilih.level) {
            jawaban += `│• *Level:* `
            for (let bintang = 0; bintang < kartu_terpilih.level; bintang++) {
                jawaban += `✪`
            }
            jawaban += ` (${kartu_terpilih.level})`
            jawaban += `\n`
        }
    } catch (e) {}
    try {
        if (kartu_terpilih.linkRating)
            jawaban += `│• *Link:* LINK-${kartu_terpilih.linkRating}\n`
    } catch (e) {}
    try {
        if (kartu_terpilih.atk && kartu_terpilih.def)
            jawaban += `│• *ATK / DEF:* ${numberWithCommas(kartu_terpilih.atk)} / ${numberWithCommas(kartu_terpilih.def)}\n`
        else if (kartu_terpilih.atk && !kartu_terpilih.def)
            jawaban += `│• *ATK / DEF:* ${numberWithCommas(kartu_terpilih.atk)} / -\n`
        else if (!kartu_terpilih.atk && kartu_terpilih.def)
            jawaban += `│• *ATK / DEF:* - / ${numberWithCommas(kartu_terpilih.def)}\n`

    } catch (e) {}
    try {
        if (kartu_terpilih.attribute && kartu_terpilih.race)
            jawaban += `│• *Atribut / Tipe:* ${kartu_terpilih.attribute} / ${kartu_terpilih.race}\n`
        else if (kartu_terpilih.attribute && !kartu_terpilih.race)
            jawaban += `│• *Atribut / Tipe:* ${kartu_terpilih.attribute} / -\n`
        else if (!kartu_terpilih.attribute && kartu_terpilih.race)
            jawaban += `│• *Atribut / Tipe:* - / ${kartu_terpilih.race}\n`
    } catch (e) {}
    try {
        if (kartu_terpilih.linkArrows) {
            if(kartu_terpilih.linkArrows.length>0) {
                jawaban += `│• *Link Makers:* `
                for (let link of kartu_terpilih.linkArrows) {
                    // Urutan: ⬆️ ↗️ ➡️ ↘️ ⬇️ ↙️ ⬅️ ↖️
                    if (link == "Top")
                        jawaban += `⬆️ `
                    else if (link == "Top-Right")
                        jawaban += `↗️ `
                    else if (link == "Right")
                        jawaban += `➡️ `
                    else if (link == "Bottom-Right")
                        jawaban += `↘️ `
                    else if (link == "Bottom")
                        jawaban += `⬇️ `
                    else if (link == "Bottom-Left")
                        jawaban += `↙️ `
                    else if (link == "Left")
                        jawaban += `⬅️ `
                    else if (link == "Top-Left")
                        jawaban += ` ↖️`
                }
                jawaban += `\n`
            }
        }
    } catch (e) {}
    try {
        if (kartu_terpilih.rarity) {
            let rarity = ''
            switch(kartu_terpilih.rarity.toLowerCase()) {
                case 'ur':
                    rarity = "Ultra Rare";
                    break;
                case 'sr':
                    rarity = "Super Rare";
                    break;
                case 'r':
                    rarity = "Rare";
                    break;
                case 'n':
                    rarity = "Normal";
                    break;
                default:
                    rarity = "Unknown";
                    break
            }
            jawaban += `│• *Rarity (MD):* ${kartu_terpilih.rarity} (${rarity})\n`
        }
    } catch (e) {}
    try {
        if (kartu_terpilih.banStatus)
            jawaban += `│• *Ban Status (MD):* ${kartu_terpilih.banStatus}\n`
    } catch (e) {}
    try {
        if (kartu_terpilih.obtain.length>0) {
            let tmpSourceKartu = []
            for(let pk of kartu_terpilih.obtain) {
                tmpSourceKartu.push(pk.source.name)
            }
            // Remove duplikat
            tmpSourceKartu = Array.from(new Set(tmpSourceKartu))
            jawaban += `│• *Obtain (MD):* ${arrToString(tmpSourceKartu)}\n`
        }
    } catch (e) {console.log(e)}
    try {
        if (kartu_terpilih.description)
            jawaban += `│• *Deskripsi:* ${kartu_terpilih.description}\n`
    } catch (e) {}
    jawaban += `╰──────────`
    return jawaban
}