const { fetchUrl, fetchBuffer } = require("./Function")

exports.tanyaKePaimon = tanyaKePaimon = async(m, pertanyaan) => {
    let api = global.api("gif_paimon", `/chat/${m.sender}/${pertanyaan}`, "", "")
    let anu = await fetchUrl(api)
    if(!anu.success)
        return m.reply("Gomen... Paimon lagi belajar.. sebentar ya...")
    else
        return m.reply(anu.pesan)
}