const fs = require("fs")
const { pickArray } = require("./Function")
const _party_helper = JSON.parse(fs.readFileSync('./database/party_helper.json'))

exports.checkPaimonAR = async (paimon, m, isGroup, input) => {
    // Inisialisasi response
    let res = {
        cekal: true,
        pesan: "",
        linkPreview: false,
        needPartyHelper: false,
        metadata: null,
    }
    let metadata = null
    // Handle Group
    if(isGroup)
        metadata = await paimon.groupMetadata(m.key.remoteJid)
    // Handle logic
    if (isGroup && (
        input.includes("!bantu") || input.includes("!co-op") || input.includes("!coop") ||
        ((
            input.includes("bantu") || input.includes("bantuin") || input.includes("help") || input.includes("mabar") || input.includes("co-op") || input.includes("bntu") || input.includes("bnt")
        ) && ( // auto detect tanpa tag, jika ada yang chat: bantu lawan, dll
            input.includes("domain") || input.includes("boss") || input.includes("lawan") ||
            input.includes("event") || input.includes("bos") || input.includes("lwn") ||
            input.includes("artefak") || input.includes("farm") || input.includes("farming")
        ))
    )) {
        res.needPartyHelper = true
        res.metadata = metadata
    } else
    if (isGroup && (
        !input.includes("link discord") &&
        !input.includes("Layanan Discord") &&
        input.includes("link") && (
            input.includes("grup") ||
            input.includes("group") ||
            input.includes("invit") ||
            input.includes("inv") ||
            input.includes("gif")
        )
    )) {
        try {
            let link = await paimon.groupInviteCode(m.from)
            let jawaban = ``
            jawaban += `╭─「 *RINCIAN GROUP* 」\n`
            jawaban += `│ *Nama Group*: ${metadata.subject}\n`
            jawaban += `│ *Owner/Pendiri*: https://wa.me/${metadata.owner.split("@s.whatsapp.net").join("")}\n`
            jawaban += `│ *Link Group*: https://chat.whatsapp.com/${link}\n`
            if(metadata.desc == undefined)
                jawaban += `│ *Deskripsi*: -\n`
            else
                jawaban += `│ *Deskripsi*: ${metadata.desc}\n`
            jawaban += `╰──────────────`
            res.pesan = jawaban
            res.linkPreview = true
        } catch(e) {
            console.log(e)
            res.cekal = true
        }
    } else
    if (
        input.includes("makanan darurat") ||
        input.includes("emergency food")
    ) {
        res.pesan = "Oy! Paimon bukan makanan darurat!"
    } else
    if (
        input.includes("bokep") && 
        input.includes("link")
    ) {
        res.pesan = pickArray(["bokep teros 😑","Lank link teross..."])
    } else
    if (
        !input.includes("!paimon") &&
        input.includes("paimon") && (
            input.includes("makasih") ||
            input.includes("terimakasih") ||
            input.includes("trimakasih") ||
            input.includes("terima kasih") ||
            input.includes("thanks") ||
            input.includes("thx") ||
            input.includes("thank you")
        )
    ) {
        res.pesan = pickArray(["Sama-sama traveler! ^_^",'Sama-sama :")'])
    } else
    if (
        !input.includes("!paimon") &&
        input.includes("paimon") && (
            input.includes("bot") ||
            input.includes("robot")
        )
    ) {
        res.pesan = "Oy! Paimon bukan BOT T.T"
    } else
    if (
        !input.includes("!paimon") &&
        (
            input.includes("paimon") ||
            input.includes("bot") ||
            input.includes("botnya")
        ) && (
            input.includes("curi") ||
            input.includes("culik")
        )
    ) {
        res.pesan = "Oh tidak!! Paimon diculik!!"
    } else
    if (
        !input.includes("!paimon") &&
        input.includes("paimon") && (
            input.includes("bodoh") ||
            input.includes("buduh") ||
            input.includes("bodong") ||
            input.includes("rusak") ||
            input.includes("error")
        )
    ) {
        res.pesan = "Hikss.. Jahat T.T"
    } else {
        res.cekal = false
    }
    // console.log(res)
    return res
}

exports.handlePaimonAR = async (paimon, m, res) => {
    // Fitur untuk menghandle para traveler toxic
    try {
        if(res.needPartyHelper) {
            return await showPartyHelper(paimon, m, res.metadata)
        } else
        if(res.linkPreview) {
            return await paimon.sendMessage(m.from, { text: res.pesan }, { linkPreview: true })
        } else
            return m.reply(res.pesan)
    } catch(e) {
        return console.log(e)
    }
}

const showPartyHelper = async function(paimon, m, metadata) {
    let teks = `╭─「 *PARTY HELPER* 」\n`
    let no = 0
    let groupMembers = metadata.participants
    for (let prem of global._party_helper) {
        for (let mem of groupMembers) {
            if (mem.id == prem) {
                no += 1
                teks += `[${no.toString()}] @${prem.split('@')[0]}\n`
                break
            }
        }
    }
    teks += `│+ Total : ${no}\n`
    teks += `│──────────\n`
    teks += `│ Catatan:\n`
    teks += `│ 1. Untuk melihat UID member lain silahkan gunakan perintah !uid <tag>\n`
    teks += `│ 2. Silahkan akses _*!discord*_ jika memerlukan layanan voice line\n`
    teks += `╰─⎿ *${m.pushName}* ⏋─`
    return await paimon.sendMessage(m.from, { text: teks, mentions: global._party_helper }, { quoted: m })
}