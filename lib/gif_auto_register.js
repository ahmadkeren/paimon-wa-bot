const { cariTraveler, getTravelerCookies, gifDBEngine } = require("./gif")
const { formPerkenalkanDiri } = require("./template")

exports.checkNewGifMember = (input) => {
    if (
        (input.includes("nama")) &&
        (
            (
                input.includes("uid") ||
                input.includes("id") ||
                input.includes("server")
            ) ||
            (
                input.includes("asal") ||
                input.includes("kota") ||
                input.includes("asl")
            )
        )
    )
        return true;
    else
        return false;
}

exports.handleNewGifMember = async (paimon, m, text) => {
    // Smart auto register traveler baru
    try {
        let cari = await cariTraveler(m.sender)
        let member_baru = false
        let allocated_member = true
        let villageLocation = "layover"

        if(cari == null || !cari) {
            member_baru = true
            allocated_member = false
        }

        if(cari.length > 0) {
            try {
                if(cari[0]["Status"] == "Blm. Registrasi") {
                    member_baru = true
                }
            } catch(e) {
                console.log(e)
            }
            try {
                villageLocation = cari[0]["guild"]
            } catch(e) {
                console.log(e)
            }
        }

        if (member_baru) {
            // terdeteksi member baru (belum registrasi)
            // fetch data
            data_string = text.split(`\n`)
            data_member = {
                nama: null,
                asal: null,
                ign: null,
                uid: null,
                char_favorit: null,
            }
            for (let fzz_str of data_string) {
                if (fzz_str.toLowerCase().includes("nama")) {
                    new_fzz = fzz_str.split(": ").join(":")
                    data_fzz = new_fzz.split(":")
                    data_taken = data_fzz[1]
                    if (data_taken != null && data_taken != "" && data_taken != " ")
                        data_member.nama = capitalizeEach(data_taken)
                } else
                if (fzz_str.toLowerCase().includes("asal") || fzz_str.toLowerCase().includes("kota") || fzz_str.toLowerCase().includes("asl")) {
                    new_fzz = fzz_str.split(": ").join(":")
                    data_fzz = new_fzz.split(":")
                    data_taken = data_fzz[1]
                    if (data_taken != null && data_taken != "" && data_taken != " ")
                        data_member.asal = capitalizeEach(data_taken)
                    else
                        data_member.asal = "Namek (Unknown)"
                } else
                if (fzz_str.toLowerCase().includes("ign")) {
                    new_fzz = fzz_str.split(": ").join(":")
                    data_fzz = new_fzz.split(":")
                    data_taken = data_fzz[1]
                    if (data_taken != null && data_taken != "" && data_taken != " ")
                        data_member.ign = data_taken
                } else
                if (fzz_str.toLowerCase().includes("uid")) {
                    new_fzz = fzz_str.split(": ").join(":")
                    data_fzz = new_fzz.split(":")
                    data_taken = data_fzz[1]
                    if (data_taken != null && data_taken != "" && data_taken != " ")
                        data_member.uid = data_taken
                } else
                if (fzz_str.toLowerCase().includes("char") || fzz_str.toLowerCase().includes("favorit") || fzz_str.toLowerCase().includes("favo")) {
                    new_fzz = fzz_str.split(": ").join(":")
                    data_fzz = new_fzz.split(":")
                    data_taken = data_fzz[1]
                    if (data_taken != null && data_taken != "" && data_taken != " ")
                        data_member.char_favorit = data_taken
                }
            }
            if (data_member.nama == null || data_member.uid == null || data_member.ign == null) {
                let teks = `Oops.. data yang kamu submit tidak lengkap!\n`
                teks += formPerkenalkanDiri()
                return m.reply(teks)
            } else {
                console.log(data_member)
                if (allocated_member) {
                    data_baru = {
                        "data": [{
                            "Nama": data_member.nama,
                            "IGN": data_member.ign,
                            "UID": data_member.uid,
                            "Asal": data_member.asal,
                            "Profile_Picture": data_member.char_favorit,
                            "Status": "Aktif"
                        }]
                    };
                    try {
                        anu = await gifDBEngine("put", villageLocation, m.sender, data_baru)
                    } catch (e) {
                        console.log("Data member baru tidak dapat disubmit ke DB")
                        console.log(e)
                    }
                }
                // return await sendGIFMemberRequest(paimon, m, text)
                let jawaban = `Yey! data kamu berhasil Paimon rekap!\nKartu anggotanya nanti Paimon buatin ya..\nKalau ada kesalahan data, jangan lupa melapor traveler 😁`
                await paimon.sendMessage(m.from, {text: jawaban}, { quoted: m }) // kirim ke member baru
                let chat_ke_owner = `*[MEMBER BARU]*\n*Nomor*: @${m.sender.split("@s.whatsapp.net")}\n*Biodata*:\n${text}`
                await paimon.sendMessage('6282282170805@s.whatsapp.net', {text: chat_ke_owner}, { quoted: m }) // kirim ke actinium
            }
        }
    } catch(e) {
        console.log(e)
    }
}