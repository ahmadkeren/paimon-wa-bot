var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod }
}
Object.defineProperty(exports, "__esModule", { value: true })

const axios = require("axios")
const cheerio = require("cheerio")
const { resolve } = require("path")
const util = require("util")
const { MessageType } = require('@adiwajshing/baileys')
const fetch = require('node-fetch')


exports.getRandom = (ext, length = "10") => {
    var result = ""
    var character = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"
    var characterLength = character.length
    for (var i = 0; i < length; i++) {
        result += character.charAt(Math.floor(Math.random() * characterLength))
    }

    return `${result}.${ext}`
}

exports.pickArray = (arr) => {
    let res = arr[Math.floor(Math.random()*arr.length)];
    return res;
}

exports.arrayRemove = (arr, value) => { 
    return arr.filter(function(ele){ 
        return ele != value; 
    });
}

exports.getDate = () => {
    let ts = Date.now();
    let date_ob = new Date(ts);
    let date = date_ob.getDate();
    let month = date_ob.getMonth() + 1;
    let year = date_ob.getFullYear();
    let res = (year + "-" + month + "-" + date);
    return res;
}

exports.htmlToWaText = (text) => {
    // Enter
    text = text.split("<p>").join("")
    text = text.split("</p>").join("\n")
    // Bold
    text = text.split("<strong>").join("*")
    text = text.split("</strong>").join("*")
    // Italic
    text = text.split("<em>").join("_")
    text = text.split("</em>").join("_")
    // Del
    text = text.split("<s>").join("~")
    text = text.split("</s>").join("~")
    // Return
    return removeLastString(text,"\n","");
}

exports.removeLastString = removeLastString = (str, search, replace) => {
    const lastIndex = str.lastIndexOf(search);
    const replacement = replace;
    const replaced = str.substring(0, lastIndex) + replacement + str.substring(lastIndex + 1);
    return replaced
}

exports.fetchBuffer = async (url, options) => {
	try {
		options ? options : {}
		const res = await axios({
			method: "GET",
			url,
			headers: {
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.70 Safari/537.36",
				'DNT': 1,
				'Upgrade-Insecure-Request': 1
			},
			...options,
			responseType: 'arraybuffer'
		})
		return res.data
	} catch (err) {
		return err
	}
}

exports.fetchUrl = async (url, options) => {
    try {
        options ? options : {}
        const res = await axios({
            method: 'GET',
            url: url,
            headers: {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36'
            },
            ...options
        })
        return res.data
    } catch (err) {
        return err
    }
}

exports.fetchText = fetchText = (url, options) => new Promise(async (resolve, reject) => {
    fetch(url, options)
        .then(response => response.text())
        .then(text => {
            // console.log(text)
            resolve(text)
        })
        .catch((err) => {
            reject(err)
        })
})

exports.WAVersion = async () => {
    let get = await exports.fetchUrl("https://web.whatsapp.com/check-update?version=1&platform=web")
    let version = [get.currentVersion.replace(/[.]/g, ", ")]
    return version
}

exports.runtime = (seconds) => {
    seconds = Number(seconds)
	var d = Math.floor(seconds / (3600 * 24))
	var h = Math.floor(seconds % (3600 * 24) / 3600)
	var m = Math.floor(seconds % 3600 / 60)
	var s = Math.floor(seconds % 60)
	var dDisplay = d > 0 ? d + (d == 1 ? " day, " : " days, ") : ""
	var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : ""
	var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : ""
	var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : ""
	return dDisplay + hDisplay + mDisplay + sDisplay
}

exports.clockString = (ms) => {
    let h = isNaN(ms) ? '--' : Math.floor(ms / 3600000)
    let m = isNaN(ms) ? '--' : Math.floor(ms / 60000) % 60
    let s = isNaN(ms) ? '--' : Math.floor(ms / 1000) % 60
    return [h, m, s].map(v => v.toString().padStart(2, 0)).join(':')
}

exports.isUrl = (url) => {
    return url.match(new RegExp(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/, 'gi'))
}

exports.checkPrefix = (prefix, body) => {
    if (!body) return false
    if (typeof prefix == "string") {
        return {
            match: body.startsWith(prefix),
            prefix: prefix,
            body: body.replace(prefix, "")
        }
    } else if (typeof prefix == "object") {
        if (Array.isArray(prefix)) {
            for (const value of prefix) {
                if (typeof value == "string") {
                    if (body.startsWith(value)) return {
                        match: true,
                        prefix: value,
                        body: body.replace(value, "")
                    }
                } else if (typeof value == "object") {
                    if (value instanceof RegExp) {
                        if (body.match(value)) return {
                            match: true,
                            prefix: (value.exec(body))?.[0],
                            body: body.replace(value, "")
                        }
                    }
                }
            }
        } else if (prefix instanceof RegExp) {
            if (body.match(prefix)) return {
                match: true,
                prefix: (prefix.exec(body))?.[0],
                body: body.replace(prefix, "")
            }
        }
    }
    return false
}

exports.logic = (check, inp, out) => {
	if (inp.length !== out.length) throw new Error('Input and Output must have same length')
	for (let i in inp)
		if (util.isDeepStrictEqual(check, inp[i])) return out[i]
	return null
}

exports.isNumber = (number) => {
    const int = parseInt(number)
    return typeof int === 'number' && !isNaN(int)
}

exports.numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}


exports.truncateString = (str,n=10) => {
  return (str.length > n) ? str.substr(0, n-1) + '..' : str;
}

exports.capitalize = (s) => {
  if (typeof s !== 'string') return ''
    return s.charAt(0).toUpperCase() + s.slice(1)
}

exports.sebutAngka = (s) => {
  switch(s) {
    case "0": return "Nol"; break;
    case "1": return "Satu"; break;
    case "2": return "Dua"; break;
    case "3": return "Tiga"; break;
    case "4": return "Empat"; break;
    case "5": return "Lima"; break;
    case "6": return "Enam"; break;
    case "7": return "Tujuh"; break;
    case "8": return "Delapan"; break;
    case "9": return "Sembilan"; break;
    case "10": return "Sepuluh"; break;
    default: return "-"; break;
  }
}

exports.capitalizeEach = capitalizeEach = (str) => {
    return str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
    })
}

exports.arrToString = (arr) => {
    $string = ""
    for (let item of arr) {
        if ($string.length == "")
            $string = capitalizeEach(item)
        else
            $string += ", " + capitalizeEach(item)
    }
    return $string
}

exports.getNamaBulan = (bln=1) => {
    bln = bln.toString()
    switch(bln) {
        case "1": return "Januari"; break;
        case "2": return "Februari"; break;
        case "3": return "Maret"; break;
        case "4": return "April"; break;
        case "5": return "Mei"; break;
        case "6": return "Juni"; break;
        case "7": return "Juli"; break;
        case "8": return "Agustus"; break;
        case "9": return "September"; break;
        case "10": return "Oktober"; break;
        case "11": return "November"; break;
        case "12": return "Desember"; break;
        default: return "April Mob"; break;
    }
}

exports.secondsToHms = (d) => {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);
    // hour / hours
    var hDisplay = h > 0 ? h + (h == 1 ? " jam, " : " jam, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " menit, " : " menit, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " detik" : " detik") : "";
    return hDisplay + mDisplay + sDisplay;
}

exports.kirim_pesan_btn_with_img = async (paimon, m, imgBuffer, buttons, msg, footer=null) => {
    let buttonMessage = {
        image: imgBuffer,
        caption: msg,
        footer: footer,
        buttons: buttons,
        headerType: 4
    }
    try {
        return await paimon.sendMessage(m.from, buttonMessage, { quoted: m })
    } catch(e) {
        console.log(e)
        return null;
    }
}