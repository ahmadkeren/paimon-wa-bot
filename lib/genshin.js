exports.refineBeautifer = (weapon) => {
    effect = weapon.effect
    if (effect.includes("{0}")) effect = effect.split("{0}").join("*" + weapon.r1[0] + " / " + weapon.r2[0] + " / " + weapon.r3[0] + " / " + weapon.r4[0] + " / " + weapon.r5[0] + "*")
    if (effect.includes("{1}")) effect = effect.split("{1}").join("*" + weapon.r1[1] + " / " + weapon.r2[1] + " / " + weapon.r3[1] + " / " + weapon.r4[1] + " / " + weapon.r5[1] + "*")
    if (effect.includes("{2}")) effect = effect.split("{2}").join("*" + weapon.r1[2] + " / " + weapon.r2[2] + " / " + weapon.r3[2] + " / " + weapon.r4[2] + " / " + weapon.r5[2] + "*")
    if (effect.includes("{3}")) effect = effect.split("{3}").join("*" + weapon.r1[3] + " / " + weapon.r2[3] + " / " + weapon.r3[3] + " / " + weapon.r4[3] + " / " + weapon.r5[3] + "*")
    if (effect.includes("{4}")) effect = effect.split("{4}").join("*" + weapon.r1[4] + " / " + weapon.r2[4] + " / " + weapon.r3[4] + " / " + weapon.r4[4] + " / " + weapon.r5[4] + "*")
    if (effect.includes("{5}")) effect = effect.split("{5}").join("*" + weapon.r1[5] + " / " + weapon.r2[5] + " / " + weapon.r3[5] + " / " + weapon.r4[5] + " / " + weapon.r5[5] + "*")
    // Filter
    effect = effect.split("/ undefined").join("")
    return effect
}

exports.getKependekan = ($str,pemisah="",spasi="") => {
    arrStr = $str.split(" ")
    if(arrStr.length<=1) return $str
    resStr = ""
    for(let i=0; i<arrStr.length;i++) {
        resStr+=arrStr[i].charAt(0)
        if(i+1<arrStr.length)
            resStr+=(pemisah+spasi)
    }
    return resStr
}