const {Wit, log} = require('node-wit');

const serverToken = "NOS5UOL47UEUABA5FODL7YOCUZO3LIHN" // server
const clientToken = "NA6JR7KQOIXXM5EE7MNCMVVUS4MJMGWG" // client

const paimonServer = new Wit({accessToken: serverToken});
const paimonClient = new Wit({accessToken: clientToken});

exports.executeCMD = executeCMD = async(paimon, m, cmdName, text, commands, body) => {
    try {
        let cmdAlt = commands.get(cmdName) || Array.from(commands.values()).find((v) => v.alias.find((x) => x.toLowerCase() == cmdName)) || ""
        let args = body.trim().split(/ +/).slice(1)
        return await cmdAlt.exec(paimon, m, {text, args})
    } catch(e) {
        console.log(e)
        return m.reply("Perintah *"+cmdName+"* tidak ditemukan!")
    }
}

exports.paimonAiEngine = async (paimon, m, isOwner=false, text, commands, body) => {
    let unhandled_msg = "Paimon tidak mengerti apa yang kamu maksud..\nUntuk melihat apa yang paimon bisa, silahkan ketik *!menu*"
    try {
        let client = paimonClient;
        if(isOwner) client = paimonServer;
        client
            .message(text, {})
            .then(data => {
                try {
                    // Logic handle || Hanya ola
                    let ambang_intent = 0.60
                    let ambang_entities = 0.50
                    // Unhandled
                    if(data.intents.length == 0)
                        return m.reply(unhandled_msg)
                    // Intent handled
                    let intent = {
                        name: null,
                        confidence: 0,
                        value: null,
                    }
                    try {
                        intent = data.intents[0]
                        intent.name = intent.name.split('get_').join('')
                    } catch(e) {}
                    // Entity handled
                    let entitas = {
                        name: null,
                        confidence: 0,
                        value: "",
                    }
                    let str_entitas = ""
                    let cnt = 0
                    try {
                        for(let entity_name in data.entities) {
                            for(let entitiy of data.entities[entity_name]) {
                                str_entitas += entitiy.value
                                // Handle spasi
                                if(cnt+1 < data.entities[entity_name].length)
                                    str_entitas += " "
                                // Count
                                cnt+=1
                            }
                            break;
                        }
                    } catch(e) {
                        console.log(e)
                    }
                    
                    // console.log("== Intent ==")
                    // console.log(intent)
                    // console.log("== Entitas ==")
                    // console.log(entitas)
                    // console.log(intent.confidence +" vs "+ ambang_intent)
                    // console.log("entitas: "+str_entitas)
                    
                    if(intent.confidence >= ambang_intent) {
                        return executeCMD(paimon, m, intent.name, str_entitas, commands, body)
                    }
                    return m.reply(unhandled_msg)
                    // End of Logic handle
                } catch(e) {
                    console.log(e)
                    return m.reply(unhandled_msg)
                }
            })
            .catch(e => {
                console.log(e)
                return m.reply(unhandled_msg)
            });
    } catch(e) {
        console.log(e)
        return m.reply(unhandled_msg)
    }
}