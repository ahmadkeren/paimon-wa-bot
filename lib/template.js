const { fetchUrl, fetchBuffer, secondsToHms } = require("./Function")

exports.formPerkenalkanDiri = formPerkenalkanDiri => {
    teks = ""
    teks += `Silahkan memperkenalkan diri dengan mengirimkan:\n`
    teks += `- Nama Asli: \n`
    teks += `- Kota Asal: \n`
    teks += `- IGN GI: \n`
    teks += `- UID GI: \n`
    teks += `- Char Favorit: \n`
    teks += `Jangan lupa disertai dengan screenshoot profil akun GInya ya.. 😊🙏\n`
    return teks
}

exports.catatanGIF = formPerkenalkanDiri => {
    teks = ""
    teks += `Catatan: \n`
    teks += `1) Aturan GIF dapat dibaca pada laman: https://bit.ly/gif_rules\n`
    teks += `2) Untuk melihat UID member lain silahkan ketik *!uid* <tag orangnya>\n`
    teks += `3) Tertarik berkompetisi dalam Spiral Abyss, silahkan akses laman: https://gif-official.web.app\n`
    teks += `4) Untuk melihat apa yang paimon bisa silahkan ketik *!menu*\n`
    teks += `5) Untuk mendaftar ke sistem paimon, akses *!gif_reg* <uid> (tanpa tanda <>)\n`
    return teks
}

exports.travelerIdentity = async (balasan, lmt) => {
    let IGD = await fetchUrl(global.api("enka_genshin", `/u/${lmt["UID"]}/__data.json`, {}, ""))
    // Templating
    balasan += `│ *• Nama:* ${lmt["Nama"]}\n`
    balasan += `│ *• Asal:* ${lmt["Asal"]}\n`
    balasan += `│ *• IGN:* ${lmt["IGN"]}\n`
    balasan += `│ *• UID:* ${lmt["UID"]}\n`
    balasan += `│ *• Keanggotaan:* ${lmt["Status"]}\n`
    if (lmt["Badge"] != "" && lmt["Badge"] != null && lmt["Badge"] != " ")
        balasan += `│ *• Lencana:* ${lmt["Badge"]}\n`
    balasan += `│ *• Signature:* ${lmt["Signature"]}\n`;

    try {
        if(IGD.playerInfo) {
            balasan += `│──────────────────\n`
            balasan += `│ *• Nickname:* ${IGD.playerInfo.nickname}\n`
            balasan += `│ *• Level:* AR-${IGD.playerInfo.level} / WL-${IGD.playerInfo.worldLevel}\n`
            balasan += `│ *• Game Signature:* ${IGD.playerInfo.signature}\n`
        }
    } catch(e) {
        console.log(e)
    }
    return balasan
}

exports.benefitCookies = (balasan) => {
    balasan += `Contoh: *!submit_cookie mi18nLang=en-us; _MHYUUID=95ca55d....*\n`
    balasan += `Benefit/keuntungan mensubmit cookie:\n`
    balasan += ` - Auto reedem code\n`
    balasan += ` - Auto claim daily hoyolab\n`
    balasan += ` - Cek resin melalui Paimon\n`
    balasan += ` - Cek pendapatan primogem/mora\n`
    balasan += ` - Cek saran upgrade karakter\n`
    balasan += ` - Berpartisipasi dalam GIF - Leaderboard\n`
    balasan += `Untuk mendapatkan cookie silahkan akses laman: https://paimon.page.link/cookie`
    return balasan
}

exports.sedangAFK = (sender, afkData) => {
    // Get time different
    let currentTime = Date.now()
    let diff_time = currentTime - afkData.time
    // Sedang AFK
    let resAFK = ``
    resAFK += `Oops!! Traveler ${sender.split('@s.whatsapp.net').join('')} sedang AFK\n`
    resAFK += `*Alasan AFK*: ${afkData.reason}\n`
    resAFK += `*Lama AFK*: berlangsung ${secondsToHms(diff_time/1000)}\n`
    resAFK += `Tunggu dia kembali ya Traveler ^_^`
    return resAFK
}