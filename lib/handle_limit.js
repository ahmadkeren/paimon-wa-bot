const fs = require("fs")
const { getDate } = require("./Function")
// let db_limits = JSON.parse(fs.readFileSync('./database/limits.json'))
const { updateFireData } = require("./../controller/firebase/fireController")
const id_collection = 'limits'

exports.handleLimitAccess = async (paimon, m, fitur, maxLimit=3) => {
    // Fitur untuk menghandle limit user tertentu
    return await addLimitData(m.from, fitur, maxLimit)
}

exports.isLimitAccess = async (paimon, m, fitur, maxLimit) => {
    for(let limit of global.db_limits) {
        if(limit.user == m.from) {
            for(let menu of limit.menus) {
                if(menu.name == fitur) {
                    if(menu.prev_access == getDate()) {
                        if(menu.limit >= maxLimit)
                            return true;
                        else
                            return false
                    } else {
                        await resetLimitData(m.from, fitur)
                        return false
                    }
                }
            }
        }
    }
    return false
}

const addLimitData = async (user, menuName) => {
    let template = {
        name: menuName,
        prev_access: getDate(),
        limit: 1,
    }
    let indexLimit = 0;
    let userKetemu = false
    for(let limit of global.db_limits) {
        if(limit.user == user) {
            userKetemu = true
            let indexMenu = 0;
            for(let menu of limit.menus) {
                let menuKetemu = false
                if(menu.name == menuName) {
                    menuKetemu = true
                    // Tambahkan limit data
                    // console.log("Menambah data limit terhadap menu...")
                    global.db_limits[indexLimit].menus[indexMenu].limit+=1
                    global.db_limits[indexLimit].menus[indexMenu].prev_access = getDate()
                    return await writeDB(global.db_limits)
                }
                if(!menuKetemu) {
                    // Tambahkan template limit data
                    // console.log("Menambah template data menu...")
                    global.db_limits[indexLimit].menus.push(template)
                    return await writeDB(global.db_limits)
                }
                indexMenu+=1
            }
        }
        indexLimit+=1
    }
    if(!userKetemu)
        return await storeLimitData(user, template)
}

const storeLimitData = async (user, menu=null) => {
    let template = {
        user: user,
        menus: []
    }
    if(menu!=null) {
        template.menus.push(menu)
    }
    global.db_limits.push(template)
    return await writeDB(global.db_limits)
}

const resetLimitData = async (user, menuName) => {
    let indexLimit = 0;
    for(let limit of global.db_limits) {
        if(limit.user == user) {
            let indexMenu = 0;
            for(let menu of limit.menus) {
                if(menu.name == menuName) {
                    // Reset limit data
                    global.db_limits[indexLimit].menus[indexMenu].limit=0
                    global.db_limits[indexLimit].menus[indexMenu].prev_access=getDate()
                    return await writeDB(global.db_limits)
                }
                indexMenu+=1
            }
        }
        indexLimit+=1
    }
}

const writeDB = async (data_baru) => {
    updateFireData(id_collection, data_baru)
    // return fs.writeFileSync('./database/limits.json', JSON.stringify(data_baru))
}