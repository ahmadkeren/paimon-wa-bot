const fs = require("fs")

const { updateFireData } = require("./../controller/firebase/fireController")
const id_collection = 'pinalty'

exports.checkBadword = (input) => {
    pesan = input
    // Antisipasi number replcement, contoh: 4su
    pesan = pesan.split("12").join("r")
    pesan = pesan.split("1").join("i")
    pesan = pesan.split("3").join("e")
    pesan = pesan.split("4").join("a")
    pesan = pesan.split("5").join("s")
    pesan = pesan.split("6").join("g")
    pesan = pesan.split("9").join("g")
    pesan = pesan.split("0").join("o")
    // Hapus karakter duplicate, contoh: fuuck == fuck
    pesan = pesan.replace(/(.)\1+/g, '$1')
    // Split ke array
    pesan = pesan.split(" ")
    for (kata of pesan) {
        if (global.badwords.includes(kata.toLowerCase())) return true
    }
    return false
}

exports.handleToxicer = async (paimon, m, isAdmin=false) => {
    // Fitur untuk menghandle para traveler toxic
    if (!isAdmin) {
        try {
            // Sistem wisuda
            total_pelanggaran = addPinaltyData(m.sender, 1)
            if (total_pelanggaran >= 3) {
                // Auto wisuda
                var Kick = `${m.from}`
                let res = ""
                res += `*「 ANTI BADWORD 」*\n`
                res +=`Gomeneh.. Kamu Paimon wisuda karena berkata kasar!\n`
                res +=`Paimon udah ingetin kamu berkali-kali lo... :)`
                m.reply(res)
                setTimeout(() => {
                    // return await paimon.groupRemove(m.from, [Kick])
                    paimon.groupParticipantsUpdate(
                        m.from,
                        [m.sender],
                        "remove"
                    )
                }, 1100)
                return hapusPinaltyData(m.sender)
            } else
            if (total_pelanggaran < 2) {
                // Tegura awal
                return m.reply("Ucapannya tolong dijaga ya traveler!!")
            } else {
                // Teguran akhir
                return m.reply("Ucapannya tolong dijaga ya traveler!!\nJika diulangi lagi kamu akan paimon wisuda!")
            }
        } catch (e) {
            console.log(e)
            return m.reply("Untung paimon bukan admin, kalo admin udah paimon kick!");
        }
    } else {
        return m.reply("Tolong jaga ucapan min👍")
    }
}

const putPinaltyData = (sender) => {
    let found = false
    resData = null
    for (let pinalty of global._pelanggar) {
        if (pinalty.id === sender) {
            found = true
            resData = pinalty
            break
        }
    }
    if (found === false) {
        let obj = {
            id: sender,
            pinalty: 0
        }
        global._pelanggar.push(obj)
        // fs.writeFileSync('./database/pinalty.json', JSON.stringify(_pelanggar))
        updateFireData(id_collection, global._pelanggar)
        resData = obj
    }
    return resData
}

const hapusPinaltyData = (sender) => {
    let newPinaltyStorage = []
    for (let pinalty of global._pelanggar) {
        if (pinalty.id != sender) {
            newPinaltyStorage.push(pinalty)
        }
    }
    // Langsung apply
    global._pelanggar = newPinaltyStorage
    // fs.writeFileSync('./database/pinalty.json', JSON.stringify(_pelanggar))
    updateFireData(id_collection, global._pelanggar)
    return true
}

const addPinaltyData = (target, num) => {
    smartData = putPinaltyData(target) // smart check
    let index_pinalty = 0
    let found = false
    for (let pinalty of global._pelanggar) {
        if (pinalty.id == target) {
            found = true
            break
        }
        index_pinalty += 1
    }
    if (found !== false) {
        tmp_count = global._pelanggar[index_pinalty].pinalty += num
        // fs.writeFileSync('./database/pinalty.json', JSON.stringify(_pelanggar))
        updateFireData(id_collection, global._pelanggar)
    }
    return tmp_count;
}