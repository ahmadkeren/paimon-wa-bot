const generateListMenu = async (btn_text="Lihat Daftar", text="Teks Pesan", command="ascend", label_deskripsi="Lihat detail", menus=[]) => { 
	// Format $menus
	/*
		$menus = [
			title: "Nama Header",
			rows: [
				{
					title: "Judul sub header",
					description: "Judul sub header",
					rowId: "prefix title",
				}
			]
		]
		// contoh:
		$menus = [
			title: "Build Karakter",
			rows: ["amber","diona","diluc"]
		]
	*/

	var generated_list = [];

	for(let menu of menus) {
		var tmp_menu = {
			title: menu.title,
			rows: [],
		}
		for(let data of menu.rows) {
			tmp_menu.rows.push({
				title: `${data}`,
				description: `${label_deskripsi} ${data}`,
				rowId: `${command} ${data}`,
			})
		}
		generated_list.push(tmp_menu)
		// console.log(tmp_menu)
	}


	button = {
		buttonText: `${btn_text}`,
		text: `${text}`,
		sections: generated_list,
		listType: 1
	}

	return button;
}

exports.generateListMenu = generateListMenu