const { getFireData } = require("../controller/firebase/fireController")

// Main method
const main = async() => {
	const test = await getFireData("badwords")
	return console.log(test)
}

// Run
main()