const { getFireDataRealtime, updateFireData } = require("../controller/firebase/fireController")
const collection_name = "realtime_notif"
// Main method
const main = async() => {
	const notifikasi = await getFireDataRealtime(collection_name)
	const observer = notifikasi.onSnapshot(docSnapshot => {
        console.log(`Melakukan sinkronisasi ke database`);
        const dataMaster = docSnapshot.data()
        // Filter data
        let dataFilter = filterUnsendedMessage(dataMaster)
        
        // Kirim pesan
        for(let dt of dataFilter) {
        	for(let target of dt.target) {
        		console.log(`Mengirim pesan dengan ID: ${dt.id} ke nomor ${target}`)
        	}
        }

        // Toggle status
        let dataStatus = changeMessageStatus(dataMaster)
        let updateStatus = updateFireData(collection_name, dataStatus)
    }, err => {
        console.log(`Encountered error: ${err}`);
    });
}

const filterUnsendedMessage = function(dataMaster) {
	let dataFilter = []
    for(let dt of dataMaster.data) {
    	if(dt.isSended) continue
    	dataFilter.push(dt)
    }
    return dataFilter
}

const changeMessageStatus = function(dataMaster) {
	let dataFilter = []
    for(let dt of dataMaster.data) {
    	if(dt.isSended)
    		dataFilter.push(dt)
    	else {
    		let tmp = dt
    		tmp.isSended = true
    		dataFilter.push(tmp)
    	}
    }
    return dataFilter
}

// Run
main()