require("./config")
const { default: makeWASocket, DisconnectReason, useMultiFileAuthState, fetchLatestBaileysVersion, delay, jidNormalizedUser, makeWALegacySocket, useSingleFileLegacyAuthState, DEFAULT_CONNECTION_CONFIG, DEFAULT_LEGACY_CONNECTION_CONFIG } = require("@adiwajshing/baileys")
const fs = require("fs")
const util = require("util")
const chalk = require("chalk")
const pino = require("pino")
const yargs = require("yargs")
const path = require("path")
const { Boom } = require("@hapi/boom")
const { Collection, Simple, Store } = require("./lib")
const { serialize, WAConnection } = Simple
const Commands = new Collection()
const genshindb = require("genshin-db")
const genshindbOptions = {
    dumpResult: false, // The query result will return an object with the properties: query, folder, match, options, filename, result.
    matchAltNames: true, // Allows the matching of alternate or custom names.
    matchAliases: false, // Allows the matching of aliases. These are searchable fields that returns the data object the query matched in.
    matchCategories: false, // Allows the matching of categories. If true, then returns an array if it matches.
    verboseCategories: false, // Used if a category is matched. If true, then replaces each string name in the array with the data object instead.
    queryLanguages: ["Indonesia"], // Array of languages that your query will be searched in.
    resultLanguage: "Indonesia" // Output language that you want your results to be in.
}
genshindb.setOptions(genshindbOptions)

const {
    getListenerRealtimeDB
} = require("./listener/database")

Commands.prefix = prefa

global.api = (name, path = '/', query = {}, apikeyqueryname) => (name in global.APIs ? global.APIs[name] : name) + path + (query || apikeyqueryname ? '?' + new URLSearchParams(Object.entries({ ...query, ...(apikeyqueryname ? { [apikeyqueryname]: global.APIKeys[name in global.APIs ? global.APIs[name] : name] } : {}) })) : '')

global.opts = new Object(yargs(process.argv.slice(2)).exitProcess(false).parse())

// const { state, saveState } = global.opts["legacy"] ? useSingleFileLegacyAuthState(`./${sessionName.legacy}`) : useSingleFileAuthState(`./${sessionName.multi}`)

async function mainWAEngine() {
    let { state, saveCreds } = await useMultiFileAuthState(path.resolve('./session'))
    let { version, isLatest } = await fetchLatestBaileysVersion()
    console.log(`using WA v${version.join('.')}, isLatest: ${isLatest}`)

    const readCommands = () => {
        let dir = path.join(__dirname, "./commands")
        let dirs = fs.readdirSync(dir)
        let cleanDirs = []
        for(let tmpDir of dirs) {
            if(tmpDir!=".DS_Store")
                cleanDirs.push(tmpDir);
        }
        dirs = cleanDirs
        let listCommand = {}
        try {
            dirs.forEach(async (res) => {
                let groups = res.toLowerCase()
                Commands.type = dirs.filter(v => v !== "_").map(v => v)
                listCommand[groups] = []
                let files = fs.readdirSync(`${dir}/${res}`).filter((file) => file.endsWith(".js"))
                console.log(files)
                for (const file of files) {
                    const command = require(`${dir}/${res}/${file}`)
                    listCommand[groups].push(command)
                    Commands.set(command.name, command)
                    delay(100)
                    global.reloadFile(`${dir}/${res}/${file}`)
                }
            })
            Commands.list = listCommand
        } catch (e) {
            console.error(e)
        }
    }

    const connect = async () => {
        await readCommands()
        let { version, isLatest } = await fetchLatestBaileysVersion()
        let connOptions = {
            printQRInTerminal: true,
            logger: pino({ level: "silent" }),
            auth: state,
            version: [2, 2323, 4]
        }
        const paimon = new WAConnection(global.opts["legacy"] ? makeWALegacySocket(connOptions) : makeWASocket(connOptions))
        global.Store = Store.bind(paimon)

        // PUSHER NOTIFICATION FROM WEB/API
        // Inisalisasi variabel
        global.gif_guilds = []
        global.registered_groups = []
        global.badwords = []
        global._pelanggar = []
        global.db_limits = []
        global._party_helper = []
        global.users_banned = []
        global.db = []
        global.afk_list = []
        global.contacts = []
        global.paimon_live_chat = []

        paimon.ev.on("creds.update", saveCreds)

        paimon.ev.on("connection.update", async(update) => {
            if (update.connection == "open" && paimon.type == "legacy") {
                paimon.user = {
                    id: paimon.state.legacy.user.id,
                    jid: paimon.state.legacy.user.id,
                    name: paimon.state.legacy.user.name
                }
            }
            const { lastDisconnect, connection } = update
            if (connection) {
                console.info(`Connection Status : ${connection}`)
            }

            if (connection == "open") {
                // Akses database di firestore firebase secara realtime
                const accessDB = await getListenerRealtimeDB(paimon)
            }

            if (connection == "close") {
                try {
                    let reason = new Boom(lastDisconnect?.error)?.output.statusCode
                    if (reason === DisconnectReason.badSession) { console.log(`Bad Session File, Please Delete Session and Scan Again`); paimon.logout(); }
                    else if (reason === DisconnectReason.connectionClosed) { console.log("Connection closed, reconnecting...."); connect(); }
                    else if (reason === DisconnectReason.connectionLost) { console.log("Connection Lost from Server, reconnecting..."); connect(); }
                    else if (reason === DisconnectReason.connectionReplaced) { console.log("Connection Replaced, Another New Session Opened, Please Close Current Session First"); paimon.logout(); }
                    else if (reason === DisconnectReason.loggedOut) { console.log(`Device Logged Out, Please Scan Again And Run.`); process.exit(); }
                    else if (reason === DisconnectReason.restartRequired) { console.log("Restart Required, Restarting..."); connect(); }
                    else if (reason === DisconnectReason.timedOut) { console.log("Connection TimedOut, Reconnecting..."); connect(); }
                    else paimon.end(`Unknown DisconnectReason: ${reason}|${connection}`)
                } catch(e) {
                    console.log(e)
                }
            }
        })

        paimon.ev.on('group-participants.update', async (groupUpdate) => {
            require("./listener/group_participants")(paimon, groupUpdate)
        })

        paimon.ev.on("messages.upsert", async (chatUpdate) => {
            m = serialize(paimon, chatUpdate.messages[0])

            if (!m.message) return
            if (m.key && m.key.remoteJid == "status@broadcast") return
            if (m.key.id.startsWith("BAE5") && m.key.id.length == 16) return
            require("./paimon")(paimon, m, Commands, chatUpdate)
        })

        if (paimon.user && paimon.user?.id) paimon.user.jid = jidNormalizedUser(paimon.user?.id)
        paimon.logger = (paimon.type == "legacy") ? DEFAULT_LEGACY_CONNECTION_CONFIG.logger.child({ }) : DEFAULT_CONNECTION_CONFIG.logger.child({ })
    }

    connect()
}

mainWAEngine()