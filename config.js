const fs = require("fs")
const chalk = require("chalk")

global.reloadFile = (file, options = {}) => {
    nocache(file, module => console.log(`File "${file}" has updated`))
}

// Rest Api
global.APIs = {
    sheetdb: 'https://sheetdb.io/api/v1',
    md_meta: 'https://www.masterduelmeta.com',
    storage_google: 'https://storage.googleapis.com',
    ygo_pro_deck: 'https://db.ygoprodeck.com',
    gi_image: 'https://res.cloudinary.com/genshin/image/upload/sprites',
    ygo_canvas: 'https://ygo-canvas.herokuapp.com',
    gif_handler: 'http://47.254.235.183/gif_api_php', // php
    gif_official: 'http://47.254.235.183:2222',
    gif_canvas: 'http://47.254.235.183:3000',
    // gif_canvas: 'https://gif-canvas.herokuapp.com',
    gif_api_stats: 'http://47.254.235.183:2222',
    gif_paimon: `http://47.254.193.32:7777`,
    // gif_paimon: `http://localhost:7777`,
    gif_leaderboard: `http://47.254.193.32:9000`,
    zenz: 'https://zenzapis.xyz',
    meme_image: 'https://meme-api.herokuapp.com',
    waifu_nsfw_image: 'https://waifu.pics/api/nsfw',
    waifu_sfw_image: 'https://waifu.pics/api/sfw',
    arugaz: 'https://arugaz.herokuapp.com/api',
    some_random_api: 'https://some-random-api.ml',
    enka_genshin: 'https://enka.shinshin.moe',
}

// Apikey
global.APIKeys = {
	'https://zenzapis.xyz': '2d0b04c7b930',
}

// Other
global.options = {
    autoRead: true,
    maintenance: false,
    self: false,
    mute: false
}

global.prefa = /^[zZ#$+.?_&<>!/\\]/
global.owner = ["6282282170805"]
global.sessionName = {
    legacy: "/tmp/paimon-legacy",
    multi: "/tmp/paimon-multi"
}

global.packname = "Paimon"
global.author = "Actinium"

global.defMaxLimit = 3

global.mess = (type, m) => {
    let msg = {
        wait: '⏳ Sebentar.. sedang Paimon proses..',
        send: '⏳ Sebentar.. sedang Paimon kirim..',
        submit: '⏳ Sebentar.. sedang Paimon submit..',
        check: '⏳ Sebentar.. sedang Paimon periksa..',
        search: '⏳ Sebentar.. sedang Paimon cari..',
        generate: '⏳ Sebentar.. sedang Paimon buat..',
        owner: 'Perintah ini hanya dapat digunakan oleh Actinium!',
        badge: 'Perintah ini hanya dapat digunakan oleh traveler berbadge!',
        group: 'Perintah ini hanya dapat digunakan di dalam group!',
        private: 'Perintah ini hanya dapat digunakan di private chat!',
        admin: 'Perintah ini hanya dapat digunakan oleh admin group!',
        gif_only: 'Perintah ini hanya dapat digunakan didalam GIF - Official Guild!',
        botAdmin: 'Paimon bukan admin, tidak dapat mengakses fitur tersebut',
        bot: 'Fitur ini hanya dapat diakses oleh Paimon',
        dead: 'Fitur ini sedang dimatikan!',
        media: 'Reply media',
        error: "Ehe! hasil tidak ditemukan!",
        maintenance: "Ehe! Paimon lagi di maintenance nih traveler! ^_^",
        terculik: "Oh tidak!! Paimon diculik... T.T \nAwas nanti Paimon laporin ke boss!",
        max_limit: "Oops!! Kamu telah melewati batas mengakses menu ini! \nCoba lagi besok ya... ^_^",
        banned: "*Ehe! Kamu telah Paimon BAN! hubungi BOS jika ada komplain ya..*",
    }[type]
    if (msg) return m.reply(msg, m.from, { quoted: m })
}

function nocache(module, cb = () => {}) {
    fs.watchFile(require.resolve(module), async () => {
        await uncache(require.resolve(module))
        cb(module)
    })
}

function uncache(module = '.') {
    return new Promise((resolve, reject) => {
        try {
            delete require.cache[require.resolve(module)]
            resolve()
        } catch (e) {
            reject(e)
        }
    })
}

let file = require.resolve(__filename)
fs.watchFile(file, () => {
    fs.unwatchFile(file)
    console.log(chalk.redBright(`Update File "${file}"`))
})
